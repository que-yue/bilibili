package cn.xiaoke.damowang.order;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@MapperScan("cn.xiaoke.damowang.order.dao")
@EnableEurekaClient
@EnableFeignClients
public class DamowangOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DamowangOrderApplication.class, args);
    }

}
