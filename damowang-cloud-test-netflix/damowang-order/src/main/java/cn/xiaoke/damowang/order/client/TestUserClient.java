package cn.xiaoke.damowang.order.client;

import cn.xiaoke.damowang.order.vo.BaseVoInfo;
import cn.xiaoke.damowang.order.vo.TestOrderVo;
import feign.RequestInterceptor;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@FeignClient(value = "damowang-user",path = "damowang-user")
public interface TestUserClient {

    @PostMapping("/user/listByIds")
    Map<Integer, String> listByIds(@RequestBody List<Integer> ids);

    @PostMapping("/asd/dsfsd")
    void setUserName(List<? extends BaseVoInfo> vos);

//    @PostMapping("/user/listSetName")
//    Object listSetName(@RequestBody List vos,@RequestParam(value = "clazz",required = false) Class clazz);

}
