package cn.xiaoke.damowang.order.config;


import cn.xiaoke.damowang.order.client.TestUserClient;
import cn.xiaoke.damowang.order.vo.BaseVoInfo;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//@Aspect
//@Component
public class AopConfig {



    @Around("execution(void cn.xiaoke.damowang.order.client.TestUserClient.setUserName(*)) &&args(list)")
    public void asdasd(ProceedingJoinPoint joinPoint, List<? extends BaseVoInfo> list){
        final Object target = joinPoint.getTarget();
        final TestUserClient client = (TestUserClient) target;
        final List<Integer> ids = list.stream()
                .flatMap(item -> Stream.of(item.getCreateUser(), item.getUpdateUser()))
                .collect(Collectors.toList());
        final Map<Integer, String> map = client.listByIds(ids);
        list.forEach(item ->{
            item.setCreateUserName(map.get(item.getCreateUser()));
            item.setUpdateUserName(map.get(item.getUpdateUser()));
        });
    }

}
