package cn.xiaoke.damowang.order.config;

import cn.xiaoke.damowang.order.UserNameUtil;
import cn.xiaoke.damowang.order.client.TestUserClient;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter(value = TestUserClient.class)
public class PublicConfig {

    @Bean
    public UserNameUtil userNameUtil(){
        return UserNameUtil.userNameUtil;
    }

}
