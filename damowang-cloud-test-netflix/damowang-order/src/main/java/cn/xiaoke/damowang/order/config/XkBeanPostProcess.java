package cn.xiaoke.damowang.order.config;


import cn.xiaoke.damowang.order.client.TestUserClient;
import cn.xiaoke.damowang.order.vo.BaseVoInfo;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//@Component
public class XkBeanPostProcess implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof TestUserClient) {
            final Object proxyInstance = Proxy.newProxyInstance(XkBeanPostProcess.class.getClassLoader(), new Class[]{TestUserClient.class}
                    , (proxy, method, args) -> {
                        if (method.getName().equals("setUserName")){
                            final TestUserClient client = (TestUserClient) proxy;
                            final List<? extends BaseVoInfo> list = (List)args[0];

                            final List<Integer> ids = list.stream()
                                    .flatMap(item -> Stream.of(item.getCreateUser(), item.getUpdateUser()))
                                    .collect(Collectors.toList());
                            final Map<Integer, String> map = client.listByIds(ids);
                            list.forEach(item ->{
                                item.setCreateUserName(map.get(item.getCreateUser()));
                                item.setUpdateUserName(map.get(item.getUpdateUser()));
                            });
                            return null;
                        }else{
                            return method.invoke(bean, args);
                        }
                    });
            return proxyInstance;
        }
        return bean;
    }


//        @Override
//    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        if (bean instanceof TestUserClient) {
//            Enhancer enhancer = new Enhancer();
//            enhancer.setSuperclass(bean.getClass());
//            enhancer.setCallback(new InvocationHandler() {
//                @Override
//                public Object invoke(Object obj, Method method, Object[] args) throws Throwable {
//                    System.out.println("代理对象运行");
//                    if (method.getName().equals("setUserName")) {
//                        final TestUserClient client = (TestUserClient) obj;
//                        final Map<Integer, String> map = client.listByIds(Arrays.asList(1, 2, 3));
//                        final List<? extends BaseVoInfo> list = (List) args[0];
//                        list.forEach(item -> {
//                            item.setCreateUserName(map.get(item.getCreateUser()));
//                            item.setUpdateUserName(map.get(item.getUpdateUser()));
//                        });
//                        System.out.println(map);
//                    } else {
//                        return method.invoke(bean, args);
//                    }
//                    return null;
//                }
//            });
//            return enhancer.create();
//        } else {
//            System.out.println("....");
//        }
//        return bean;
//    }
}
