package cn.xiaoke.damowang.order.controller;


import cn.xiaoke.damowang.order.UserNameUtil;
import cn.xiaoke.damowang.order.client.TestUserClient;
import cn.xiaoke.damowang.order.entity.TestOrder;
import cn.xiaoke.damowang.order.service.OrderService;
import cn.xiaoke.damowang.order.vo.BaseVoInfo;
import cn.xiaoke.damowang.order.vo.TestOrderVo;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * (Order)表控制层
 *
 * @author makejava
 * @since 2022-01-08 19:39:36
 */
@RestController
@RequestMapping("order")
@Api(tags = "测试订单控制器")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final TestUserClient testUserClient;


    @GetMapping("/list")
    public List<TestOrderVo> getOrder(){
        final List<TestOrder> list = orderService.list();

        final List<TestOrderVo> vos = list.stream().map(l -> {
            final TestOrderVo testOrder = new TestOrderVo();
            BeanUtils.copyProperties(l, testOrder);
            return testOrder;
        }).collect(Collectors.toList());

//        testUserClient.setUserName(vos);

        return vos;
    }

    @GetMapping("/list2")
    public List<TestOrderVo> getOrder2(){
        final List<TestOrder> list = orderService.list();

        final List<TestOrderVo> vos = list.stream().map(l -> {
            final TestOrderVo testOrder = new TestOrderVo();
            BeanUtils.copyProperties(l, testOrder);
            return testOrder;
        }).collect(Collectors.toList());

        final List<Integer> ids = vos.stream()
                .flatMap(item -> Stream.of(item.getCreateUser(), item.getUpdateUser()))
                .collect(Collectors.toList());
        final Map<Integer, String> map = testUserClient.listByIds(ids);
        vos.forEach(item ->{
            item.setCreateUserName(map.get(item.getCreateUser()));
            item.setUpdateUserName(map.get(item.getUpdateUser()));
        });

//        final Object objects = testUserClient.listSetName(vos,TestOrderVo.class);
        return vos;
    }

    @PostMapping("/list3")
    public List<TestOrderVo> getOrder3(){

        final List<TestOrder> list = orderService.list();

        final List<TestOrderVo> vos = list.stream().map(l -> {
            final TestOrderVo testOrder = new TestOrderVo();
            BeanUtils.copyProperties(l, testOrder);
            return testOrder;
        }).collect(Collectors.toList());
//        UserNameUtil.setUserName(vos);
//        UserNameUtil.setUserName(vos,TestOrderVo::getCreateUser,TestOrderVo::setCreateUserName);
//        UserNameUtil.setUserName(vos
//                ,new UserNameUtil.Info<>(TestOrderVo::getCreateUser,TestOrderVo::setCreateUserName)
//                ,new UserNameUtil.Info<>(TestOrderVo::getUpdateUser,TestOrderVo::setUpdateUserName)
//        );

        return vos;
    }
}

