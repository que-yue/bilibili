package cn.xiaoke.damowang.order.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xiaoke.damowang.order.entity.TestOrder;

/**
 * (Order)表数据库访问层
 *
 * @author makejava
 * @since 2022-01-08 19:39:36
 */
public interface OrderMapper extends BaseMapper<TestOrder> {


}

