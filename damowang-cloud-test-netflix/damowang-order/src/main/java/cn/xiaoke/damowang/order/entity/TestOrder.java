package cn.xiaoke.damowang.order.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * (Order)表实体类
 *
 * @author makejava
 * @since 2022-01-08 19:39:36
 */
@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class TestOrder implements Serializable{

    private Integer id;
    //订单金额
    private Long money;
    //创建人ID
    private Integer createUser;
    //创建时间
    private LocalDateTime createDate;
    //修改人ID
    private Integer updateUser;
    //修改时间
    private LocalDateTime updateDate;



}

