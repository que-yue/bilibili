package cn.xiaoke.damowang.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xiaoke.damowang.order.entity.TestOrder;

/**
 * (Order)表服务接口
 *
 * @author makejava
 * @since 2022-01-08 19:39:37
 */
public interface OrderService extends IService<TestOrder> {

}

