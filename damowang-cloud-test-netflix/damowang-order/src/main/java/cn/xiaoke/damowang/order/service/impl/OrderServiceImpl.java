package cn.xiaoke.damowang.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xiaoke.damowang.order.dao.OrderMapper;
import cn.xiaoke.damowang.order.entity.TestOrder;
import cn.xiaoke.damowang.order.service.OrderService;
import org.springframework.stereotype.Service;

/**
 * (Order)表服务实现类
 *
 * @author makejava
 * @since 2022-01-08 19:39:37
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, TestOrder> implements OrderService {

}

