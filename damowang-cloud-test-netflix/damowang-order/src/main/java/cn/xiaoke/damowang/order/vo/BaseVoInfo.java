package cn.xiaoke.damowang.order.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
@Data
public class BaseVoInfo {
    //创建人ID
    private Integer createUser;
    //创建人姓名
    private String createUserName;
    //修改人ID
    private Integer updateUser;
    //修改人姓名
    private String updateUserName;
}
