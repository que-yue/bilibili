package cn.xiaoke.damowang.order.vo;


import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * (Order)表实体类
 *
 * @author makejava
 * @since 2022-01-08 19:39:36
 */
@SuppressWarnings("serial")
@Data
@Accessors(chain = true)
public class TestOrderVo extends BaseVoInfo implements Serializable{

    private Integer id;
    //订单金额
    private Long money;
    //创建人ID
//    private Integer createUser;
    //创建人姓名
//    private String createUserName;
    //创建时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private LocalDateTime createDate;
    //修改人ID
//    private Integer updateUser;
    //修改人姓名
//    private String updateUserName;
    //修改时间
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private LocalDateTime updateDate;



}

