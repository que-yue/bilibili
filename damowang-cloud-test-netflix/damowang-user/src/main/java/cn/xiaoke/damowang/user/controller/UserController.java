package cn.xiaoke.damowang.user.controller;

import cn.xiaoke.damowang.user.entity.BaseVoInfo;
import cn.xiaoke.damowang.user.entity.TestUser;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("user")
@Api(tags = "测试用户控制器")
public class UserController {

    private static final Map<Integer, TestUser> map = new HashMap<Integer,TestUser>(){{
        put(1,new TestUser().setId(1).setUserName("admin"));
        put(2,new TestUser().setId(2).setUserName("zhangsan"));
        put(3,new TestUser().setId(3).setUserName("lisi"));
        put(4,new TestUser().setId(4).setUserName("wangwu"));
        put(5,new TestUser().setId(5).setUserName("zhaoliu"));
        put(6,new TestUser().setId(6).setUserName("shunqi"));
    }};

    @PostMapping("/listByIds")
    @ApiOperation("通过ID集合查询用户集合")
    public Map<Integer, String> listByIds(@RequestBody List<Integer> ids){
        System.out.println("==============");
        final Map<Integer, String> collect = ids.stream().map(map::get).filter(Objects::nonNull).collect(Collectors.toMap(TestUser::getId, TestUser::getUserName));
        System.out.println(collect);
        return collect;
    }

    @PostMapping("/listSetName")
    @ApiOperation("通过ID集合查询用户集合")
    public List<MyJSONObject> listSetName(@RequestBody List<MyJSONObject> list,@RequestParam(required = false) Object clazz){
        System.out.println("==============");
        final List<Integer> ids = list.stream()
                .flatMap(item -> Stream.of(item.getCreateUser(), item.getUpdateUser()))
                .collect(Collectors.toList());
        final Map<Integer, String> map = listByIds(ids);
        list.forEach(item ->{
            item.put("@type",clazz.toString().replace("class ",""));
            item.setCreateUserName(map.get(item.getCreateUser()));
            item.setUpdateUserName(map.get(item.getUpdateUser()));
        });
        System.out.println(JSON.toJSONString(list));
        return list;
    }
    @Data
    public static class MyJSONObject extends LinkedHashMap<String,Object> {
        //创建人ID
        private Integer createUser;
        //创建人姓名
        private String createUserName;
        //修改人ID
        private Integer updateUser;
        //修改人姓名
        private String updateUserName;
    }
}
