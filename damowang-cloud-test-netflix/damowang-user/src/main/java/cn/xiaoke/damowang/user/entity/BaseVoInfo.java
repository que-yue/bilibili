package cn.xiaoke.damowang.user.entity;

import lombok.Data;

@Data
public class BaseVoInfo {
    //创建人ID
    private Integer createUser;
    //创建人姓名
    private String createUserName;
    //修改人ID
    private Integer updateUser;
    //修改人姓名
    private String updateUserName;
}
