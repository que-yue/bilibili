package cn.xiaoke.damowang.user.entity;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TestUser {

    private Integer id;

    private String userName;
}
