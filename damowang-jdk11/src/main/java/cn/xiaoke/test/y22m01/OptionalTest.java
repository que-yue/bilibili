package cn.xiaoke.test.y22m01;

import lombok.Data;

import java.util.Optional;
import java.util.Random;

public class OptionalTest {


    public static void main(String[] args) {

//        final Optional<Long> opt1 = Optional.ofNullable(randmon());
//
//        System.out.println(opt1.orElse(add100000000()));
//
//        final Long aLong = opt1.orElseGet(() -> {
//            return add100000000();
//        });
//        System.out.println(aLong);
//
//        opt1.ifPresent((val)->{
//            System.out.println("我是ifPresent");
//            System.out.println(val);
//        });
//
//        final Optional<Long> newOpt = opt1.filter((val) -> val > -100);
//
//        System.out.println(newOpt.isPresent());

        final String s = Optional.ofNullable(getA()).map(A::getB).map(B::getC).map(C::getValue).orElse(null);
        System.out.println(s);

        final String s2 = Optional.ofNullable(getA()).flatMap(A::opGetB).flatMap(B::opGetC).flatMap(C::opGetV).orElse(null);
        System.out.println(s2);

    }

    public static Long randmon(){
        return new Random().nextInt(2) == 0 ? 0L : null;
    }

    public static long add100000000(){
        System.out.println("开始计算100000000");
        long n = 0;
        for (int i = 0; i < 100000000; i++) {
            n += i;
        }
        return n;
    }
    public static A getA(){
        final Random random = new Random();
        A a = null;
        if (random.nextInt(10) < 6) {
            a = new A();
            if (random.nextInt(10) < 7) {
                final B b = new B();
                a.setB(b);
                if (random.nextInt(10) < 8) {
                    final C c = new C();
                    b.setC(c);
                    if (random.nextInt(10) < 9) {
                        c.setValue("我是C的value");
                    }
                }
            }
        }
        return a;
    }
    @Data
    static class A{
        B b;

        public Optional<B> opGetB(){
            return Optional.ofNullable(b);
        }
    }
    @Data
    static class B{
        C c;
        public Optional<C> opGetC(){
            return Optional.ofNullable(c);
        }
    }
    @Data
    static class C{
        String value;

        public Optional<String> opGetV(){
            return Optional.ofNullable(value);
        }
    }




















}
