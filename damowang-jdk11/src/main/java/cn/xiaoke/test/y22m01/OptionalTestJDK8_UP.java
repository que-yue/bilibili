package cn.xiaoke.test.y22m01;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public class OptionalTestJDK8_UP {

    public static final Map<String,String> map = new HashMap<>(){
        {
//            put("1","1");
//            put("2","2");
            put("3","3");
            put("4","4");
        }
    };



    public static void main(String[] args) {
        final Optional<Long> opt = Optional.ofNullable(random());

//        opt.ifPresentOrElse((val)->{
//            System.out.println(val);
//        },()->{
//            System.out.println("val不存在");
//        });

//        opt.map(val ->{
//            System.out.println(val);
//            return val;
//        }).orElseGet(()->{
//            System.out.println("val不存在");
//            return null;
//        });

        final String s = Optional.ofNullable(map.get("1"))
                .or(() -> Optional.ofNullable(map.get("2")))
                .or(() -> Optional.ofNullable(map.get("3")))
                .or(() -> Optional.ofNullable(map.get("4")))
                .orElse("我是保底");
        System.out.println(s);
    }


    public static Long random(){
        return new Random().nextInt(2) == 0 ? 0L : null;
    }
}
