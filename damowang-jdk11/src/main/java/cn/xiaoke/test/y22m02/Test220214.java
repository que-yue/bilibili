package cn.xiaoke.test.y22m02;

import cn.hutool.core.bean.BeanUtil;

import java.lang.reflect.Constructor;
import java.util.LinkedList;

public class Test220214 {

    public static void main(String[] args) {

    }
}

class ObjectPool<T> {

    private final LinkedList<T> list;
    private final Constructor<T> constructor;
    private final T copyObj;
    private final int size;

    // 定义对象池存储可供使用的对象
    public ObjectPool(Class<T> clazz, int size) throws Exception {
        // 初始化对象池，填入对象
        list = new LinkedList<T>();
        constructor = clazz.getConstructor();
        copyObj = constructor.newInstance();
        this.size = size;
        for (int i = 0; i < size; i++) {
            list.add(constructor.newInstance());
        }
    }

    public T get() throws Exception {
        // 从对象池里取出一个对象，注意:不允许返回null
        if (list.size() > 0) {
            return list.remove();
        }
        return constructor.newInstance();
    }

    public void recycle(T obj) {
        // 当对象可以回收时，通过调用该方法把对象放回对象池
        if (list.size() < size){
            BeanUtil.copyProperties(copyObj,obj);
            list.add(obj);
        }
    }
}