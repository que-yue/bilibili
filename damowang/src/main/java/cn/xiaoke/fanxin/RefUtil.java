package cn.xiaoke.fanxin;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class RefUtil {

    public static void 打印属性信息(Object obj) {
        final Class<?> aClass = obj.getClass();
        System.out.printf("开始分析: %s \n\r", aClass.getSimpleName());
        for (Field field : aClass.getDeclaredFields()) {
            System.out.printf("属性名: %s\t, 属性类型: %s  \n\r", field.getName(), field.getType());
        }
    }

    public static void 打印方法信息(Object obj) {
        final Set<String> set = Arrays.stream(Object.class.getDeclaredMethods()).map(Method::getName).collect(Collectors.toSet());
        set.addAll(Arrays.stream(Object.class.getMethods()).map(Method::getName).collect(Collectors.toSet()));
        final Class<?> aClass = obj.getClass();
        System.out.printf("开始分析: %s \n\r", aClass.getSimpleName());
        for (Method method : aClass.getDeclaredMethods()) {
            if (!set.contains(method.getName())) {
                System.out.printf("方法名: %s\t, 参数类型: %s\t, 返回值类型: %s\t  \n\r"
                        , method.getName()
                        , Arrays.toString(method.getParameterTypes())
                        , method.getReturnType());
//                System.out.printf("方法名: %s\t, 泛型参数类型: %s\t, 泛型返回值类型: %s\t  \n\r"
//                        , method.getName()
//                        , Arrays.toString(method.getGenericParameterTypes())
//                        , method.getGenericReturnType());
            }
        }
    }
    public static void 打印全部方法信息(Object obj) {
        final Set<String> set = Arrays.stream(Object.class.getDeclaredMethods()).map(Method::getName).collect(Collectors.toSet());
        set.addAll(Arrays.stream(Object.class.getMethods()).map(Method::getName).collect(Collectors.toSet()));
        final Class<?> aClass = obj.getClass();
        System.out.printf("开始分析: %s \n\r", aClass.getSimpleName());
        final Set<Method> methodSet = Arrays.stream(aClass.getMethods()).collect(Collectors.toSet());
        methodSet.addAll(Arrays.stream(aClass.getDeclaredMethods()).collect(Collectors.toSet()));
        for (Method method : methodSet) {
            if (!set.contains(method.getName())) {
                System.out.printf("方法名: %s\t, 参数类型: %s\t, 返回值类型: %s\t  \n\r"
                        , method.getName()
                        , Arrays.toString(method.getParameterTypes())
                        , method.getReturnType());
//                System.out.printf("方法名: %s\t, 泛型参数类型: %s\t, 泛型返回值类型: %s\t  \n\r"
//                        , method.getName()
//                        , Arrays.toString(method.getGenericParameterTypes())
//                        , method.getGenericReturnType());
            }
        }
    }
}
