package cn.xiaoke.fanxin.demo;

import java.lang.reflect.Array;
import java.util.function.IntFunction;
import java.util.function.Supplier;

public class 创建泛型对象和数组 {

    public static void main(String[] args) {
        final Pack<String> pack = new Pack<>(String::new);

        final PackArr<String> arr = new PackArr<>(String[]::new);

    }



    static class Pack<T>{
        T data;

//        public Pack() {
//            this.data = new T();
//        }


        public Pack(T t) throws IllegalAccessException, InstantiationException{
            data = (T) t.getClass().newInstance();
        }

        public Pack(Class<T> clazz) throws IllegalAccessException, InstantiationException {
            data = (T) clazz.newInstance();
        }

        public Pack(Supplier<T> supplier){
            final T t = supplier.get();
            data = t;
        }
    }

    static class PackArr<T>{
        T[] datas;

//        public Pack() {
//            this.datas = new T[10];
//        }

        public PackArr(Class<T> clazz) {
            datas = (T[]) Array.newInstance(clazz,10);
        }

        public PackArr(T... t) {
            datas = (T[]) Array.newInstance(t.getClass().getComponentType(),t.length << 1);
        }
        public PackArr(IntFunction<T[]> function){
            final T[] ts = function.apply(10);
            datas = ts;
        }
    }
}
