package cn.xiaoke.fanxin.demo;

import java.util.Random;

public class 泛型异常 {


    public static void main(String[] args) {
        final Pack pack = new Pack<>();
        try {
            pack.bbb();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }


    static class Pack<T extends Throwable> {

//        public void aaa() throws T{
//            try {
//                System.out.println(1);
//            } catch (T e) {
//                e.printStackTrace();
//            }
//        }


        public void bbb() throws T {
            try {
                System.out.println(1);
                if (new Random().nextInt(2) == 1) {
                    throw new RuntimeException();
                } else {
                    throw new MyException();
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            } catch (MyException e) {
                System.out.println();
                e.printStackTrace();
            }
//            catch (MyException<String> e) {
//                System.out.println();
//                e.printStackTrace();
//            }
//            catch (MyException<Integer> e) {
//                System.out.println();
//                e.printStackTrace();
//            }
        }
    }

    static class MyException extends Exception {
//    static class MyException<T> extends Exception{

    }


}
