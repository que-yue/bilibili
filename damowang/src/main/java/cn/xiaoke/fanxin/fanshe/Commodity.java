package cn.xiaoke.fanxin.fanshe;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
public class Commodity {
    String name;
    List<Specification> specifications;
    public Commodity() {
    }
    public Commodity(String name) {
        this.name = name;
        this.specifications = new ArrayList<>();
    }
}
