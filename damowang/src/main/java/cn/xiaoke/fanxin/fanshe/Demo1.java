package cn.xiaoke.fanxin.fanshe;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

public class Demo1 {

    public static void main(String[] args) {
        final Class<MyList> aClass = MyList.class;
        soutClassGenericInfo(aClass);
        final Class<A> aClassA = A.class;
        soutClassGenericInfo(aClassA);
        final Class<B> aClassB = B.class;
        soutClassGenericInfo(aClassB);
        final Class<C> aClassC = C.class;
        soutClassGenericInfo(aClassC);
    }

    static class MyList extends ArrayList<String>{

    }
    static class A{

    }
    static class B<T>{

    }
    static class C<T> extends ArrayList<T>{

    }


    private static void soutClassGenericInfo(Class<?> aClass) {
        final Type genericSuperclass = aClass.getGenericSuperclass();
        System.out.println(genericSuperclass);
        System.out.println("genericSuperclass.getTypeName() = " + genericSuperclass.getTypeName());
        System.out.println("genericSuperclass.getClass() = " + genericSuperclass.getClass());
        if (genericSuperclass instanceof ParameterizedType) {
            final ParameterizedType type = (ParameterizedType) genericSuperclass;
            final Type[] actualTypeArguments = type.getActualTypeArguments();
            System.out.println("---------------");
            System.out.println(Arrays.toString(actualTypeArguments));
            System.out.println("type.getOwnerType() = " + type.getOwnerType());
            System.out.println("type.getRawType() = " + type.getRawType());
        }
        System.out.println("==============");
    }
}
