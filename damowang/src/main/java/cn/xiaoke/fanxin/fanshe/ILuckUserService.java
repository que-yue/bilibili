package cn.xiaoke.fanxin.fanshe;


import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
public interface ILuckUserService extends MyService<LuckUser> {

    Map<Integer, Boolean> action(List<Integer> id);
}
