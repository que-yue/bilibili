/*
 * 版权信息 : @ 聚均科技
 */
package cn.xiaoke.fanxin.fanshe;


import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/2/16 10:39
 */
@RequiredArgsConstructor
public class LuckUserController {

    private final ILuckUserService luckUserService;

    public List<LuckUserVO> list(LuckUserWhereDTO whereDTO){
        final LambdaQueryWrapper<LuckUser> wrapper = Wrappers.lambdaQuery(LuckUser.class)
                .eq(whereDTO.getAge() != null, LuckUser::getAge, whereDTO.getAge());
        final List<LuckUser> list = luckUserService.list(wrapper);

        final List<LuckUserVO> luckUserVOS = new ArrayList<>();
        for (LuckUser luckUser : list) {
            final LuckUserVO userVO = new LuckUserVO();
            BeanUtil.copyProperties(luckUser,userVO);
            luckUserVOS.add(userVO);
        }
        return luckUserVOS;
    }
    public List<LuckUserVO> list2(LuckUserWhereDTO whereDTO){
       return  luckUserService.listVO(whereDTO,LuckUserVO.class);
    }
    public static void main(String[] args) {
        final ILuckUserService userService = new LuckUserServiceImpl();
        final LuckUserController controller = new LuckUserController(userService);
        final LuckUserSaveDTO saveDTO = new LuckUserSaveDTO().setAddress("asdsfggdgfd").setAge(11);
        controller.save2(saveDTO);
//        final List<LuckUserVO> luckUserVOS = controller.list2(new LuckUserWhereDTO());

        final Map<Integer, Boolean> action = controller.action(Arrays.asList(1, 2, 3));


    }


    public void save(LuckUserSaveDTO dto){
        final LuckUser luckUser = new LuckUser();
        BeanUtil.copyProperties(dto,luckUser);
        luckUserService.save(luckUser);
    }
    public void save2(LuckUserSaveDTO dto){
        luckUserService.saveByDto(dto);
    }

    public void updateById(LuckUserUpdateDTO dto){
        final LuckUser luckUser = new LuckUser();
        BeanUtil.copyProperties(dto,luckUser);
        luckUserService.updateById(luckUser);
    }

    public Map<Integer,Boolean> action(List<Integer> ids){
        Map<Integer,Boolean> result = luckUserService.action(ids);
        return result;
    }

}
