package cn.xiaoke.fanxin.fanshe;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
public interface LuckUserMapper extends BaseMapper<LuckUser> {

}
