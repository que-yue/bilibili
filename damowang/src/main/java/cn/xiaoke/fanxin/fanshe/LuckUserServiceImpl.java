package cn.xiaoke.fanxin.fanshe;


import com.alibaba.fastjson.JSON;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
public class LuckUserServiceImpl extends MyServiceImpl<LuckUserMapper,LuckUser> implements ILuckUserService{

    private List<List<Commodity>> getCommodityByStoreId(Integer storeId){
        final Commodity commodity1 = new Commodity().setName("商品A" + storeId).setSpecifications(new ArrayList<>());
        commodity1.getSpecifications().add(new Specification().setName("商品A大"+ storeId).setPrice( 11));
        commodity1.getSpecifications().add(new Specification().setName("商品A大"+ storeId).setPrice( 11));
        final Commodity commodity2 = new Commodity().setName("商品B" + storeId).setSpecifications(new ArrayList<>());
        commodity2.getSpecifications().add(new Specification().setName("商品B大"+ storeId).setPrice( 22));
        commodity2.getSpecifications().add(new Specification().setName("商品B小"+ storeId).setPrice( 12));
        return Collections.singletonList(Arrays.asList(commodity1, commodity2));
    }

    public Boolean push(List<List<Commodity>> commodities){
        for (List<Commodity> commodityList : commodities) {
            for (Commodity commodity : commodityList) {
                for (Specification specification : commodity.getSpecifications()) {
                    System.out.println(specification);
                }
            }
        }
        return true;
    }

    @Override
    public Map<Integer, Boolean> action(List<Integer> ids) {
        final Map<Integer, Boolean> result = new HashMap<>();
        for (Integer id : ids) {
            final List<List<Commodity>> commodities = getCommodityByStoreId(id);
            System.out.println("JSON.toJSONString(commodities) = " + JSON.toJSONString(commodities));
            final Boolean push = push(commodities);
            result.put(id,push);
        }
        return result;
    }

    public static void main(String[] args) throws NoSuchMethodException {
        String param = "[[{\"name\":\"商品A2\",\"specifications\":[{\"name\":\"商品A大2\",\"price\":11},{\"name\":\"商品A大2\",\"price\":11}]},{\"name\":\"商品B2\",\"specifications\":[{\"name\":\"商品B大2\",\"price\":22},{\"name\":\"商品B小2\",\"price\":12}]}]]";
        final Method push = LuckUserServiceImpl.class.getMethod("push", List.class);
        try {
            final Type[] parameterTypes = push.getGenericParameterTypes();
            final Type genericReturnType = push.getGenericReturnType();

            final List<List<Commodity>> list = JSON.parseObject(param, parameterTypes[0]);
            final LuckUserServiceImpl luckUserService = getImplBySpring(LuckUserServiceImpl.class);
            final Object invoke = push.invoke(luckUserService,list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private static <T> T getImplBySpring(Class<T> clazz){
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


}
