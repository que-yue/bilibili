package cn.xiaoke.fanxin.fanshe;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Specification {
    String name;
    Integer price;

    public Specification() {
    }

    public Specification(String name, Integer price) {
        this.name = name;
        this.price = price;
    }
}
