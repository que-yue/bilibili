/*
 * 版权信息 : @ 聚均科技
 */
package cn.xiaoke.fanxin.案例1;


/**
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/3/15 9:42
 */
public class 案例1_2 {
    public static void main(String[] args) {
        final Biology carnivore1 = getCarnivore();
        final Biology spawn = carnivore1.spawn();
        ((Carnivore) spawn).takeFood(null);


        final Carnivore carnivore2 = new Carnivore();

        //相对于1-1 来说  这里就舒服了很多
        final Carnivore carnivore3 = carnivore2.spawn();
        carnivore3.takeFood(null);

    }

    //返回1个食肉动物
    public static Biology getCarnivore() {
        return new Carnivore();
    }


    //生物
    static abstract class Biology {
        //繁衍
        abstract Biology spawn();

    }

    //食肉动物
    static class Carnivore extends Biology {

        //返回值改了
        @Override
        Carnivore spawn() {
            return new Carnivore();
        }


        //进食
        void takeFood(Herbivore food) {

        }
    }

    //食草动物
    static class Herbivore extends Biology {

        //返回值改了
        @Override
        Herbivore spawn() {
            return new Herbivore();
        }

        //进食
        void takeFood(Object obj) {

        }
    }
}


