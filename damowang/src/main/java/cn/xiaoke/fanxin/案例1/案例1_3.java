/*
 * 版权信息 : @ 聚均科技
 */
package cn.xiaoke.fanxin.案例1;


/**
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/3/15 9:42
 */
public class 案例1_3 {
    public static void main(String[] args) {
        final Biology<Carnivore> carnivore1 = getCarnivore();
        final Carnivore spawn = carnivore1.spawn();
        //相对于1-2 这里不需要强转了
        spawn.takeFood(null);


        final Carnivore carnivore2 = new Carnivore();

        //相对于1-1 来说  这里就舒服了很多
        final Carnivore carnivore3 = carnivore2.spawn();
        carnivore3.takeFood(null);

        //但是好像有问题 怎么生物繁殖出了石头
        final Biology<Stone> carnivore4 = getCarnivore();
        final Stone stone = carnivore4.spawn();
    }

    //返回1个食肉动物
    public static <T> Biology<T> getCarnivore() {
        return (Biology<T>) new Carnivore();
    }


    //生物
    static abstract class Biology<T> {
        //繁衍
        abstract T spawn();

    }

    //食肉动物
    static class Carnivore extends Biology<Carnivore> {

        //返回值改了

        @Override
        Carnivore spawn() {
            return null;
        }

        //进食
        void takeFood(Herbivore food) {

        }
    }

    //食草动物
    static class Herbivore extends Biology<Herbivore> {

        //返回值改了
        @Override
        Herbivore spawn() {
            return new Herbivore();
        }

        //进食
        void takeFood(Object obj) {

        }
    }
    //石头
    static class Stone{

    }
}


