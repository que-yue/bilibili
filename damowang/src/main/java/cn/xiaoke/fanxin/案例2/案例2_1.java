package cn.xiaoke.fanxin.案例2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class 案例2_1 {


    public static void main(String[] args) {

        final Botany botany = new Botany();

        final Herbivore herbivore = new Herbivore();
        herbivore.takeFood(botany);


        final Carnivore carnivore = new Carnivore();
        final Carnivore carnivore2 = new Carnivore();
        carnivore.takeFood(herbivore);
        carnivore.takeFood(carnivore2);

        final Animal<Animal<?>> spawn1 = carnivore.spawn();

        final List<Biology<?>> list = Arrays.asList(botany, herbivore, carnivore);
        final List<Biology<?>> list2 = new ArrayList<>();
        final List<? extends Biology<?>> list3 = new ArrayList<>();
        final List<? super Biology<?>> list4 = new ArrayList<>();


        for (Biology<?> biology : list) {
            final Biology<?> spawn = biology.spawn();
            list2.add(spawn);
//            list3.add(spawn);
            list4.add(spawn);
        }

    }


    //生物
    static abstract class Biology<T extends Biology<T>> {
        //繁衍
        abstract T spawn();

    }

    //动物
    static abstract class Animal<S extends Biology<?>> extends Biology<Animal<?>> {

        //进食
        abstract void takeFood(S food);

        @Override
        Animal<S> spawn() {
            return null;
        }
    }


    //食肉动物
    static class Carnivore extends Animal<Animal<?>> {
        @Override
        void takeFood(Animal<?> food) {

        }
    }

    //食草动物
    static class Herbivore extends Animal<Botany> {


        @Override
        void takeFood(Botany food) {

        }
    }


    //石头
    static class Stone {

    }


    //植物
    static class Botany extends Biology<Botany> {

        @Override
        Botany spawn() {
            return null;
        }
    }

}
