package cn.xiaoke.fanxin.桥接方法;

import cn.xiaoke.fanxin.RefUtil;

import java.util.Comparator;

public class 桥接方法_2 {

    public static void main(java.lang.String[] args) {
        final PackSub<B> b = new PackSub<>();
        final Pack<String> packSub2 = new PackSub2();
        packSub2.setData("");
        RefUtil.打印方法信息(b);
        RefUtil.打印方法信息(packSub2);
    }


    static class Pack<T> {
        T data;

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }
    }


    static class PackSub<T> extends Pack<T> {
        @Override
        public T getData() {
            return super.getData();
        }

        @Override
        public void setData(T data) {
            super.setData(data);
        }
    }

    static class PackSub2 extends Pack<String> {
        @Override
        public String getData() {
            return super.getData();
        }


        @Override
        public void setData(String data) {
            super.setData(data);
        }
    }

    static class A {

    }

    static class B extends A {

    }

    static class C extends B implements Comparable<C>, Comparator<C> {

        @Override
        public int compareTo(C o) {
            return 0;
        }

        @Override
        public int compare(C o1, C o2) {
            return 0;
        }
    }
}
