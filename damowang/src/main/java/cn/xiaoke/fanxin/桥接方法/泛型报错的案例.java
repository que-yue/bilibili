package cn.xiaoke.fanxin.桥接方法;

import cn.xiaoke.fanxin.RefUtil;

public class 泛型报错的案例 {

    public static void main(java.lang.String[] args) {
        final Pack<String> pack = new Pack<>();
        RefUtil.打印方法信息(pack);
        final A a = new A();
        RefUtil.打印方法信息(a);

    }



    static class Pack<T> {
        T data;

//        public boolean equals(T obj) {
//            return data.equals(obj);
//        }

//        public boolean equals(Pack<T> obj) {
//            return true;
//        }
    }



    static class A implements Comparable<A>{

        @Override
        public int compareTo(A o) {
            return 0;
        }
    }

//    static class B extends A implements Comparable<B>{
//
//    }

}
