package cn.xiaoke.fanxin.泛型擦除;

import cn.xiaoke.fanxin.RefUtil;
import lombok.Getter;
import lombok.Setter;

public class 泛型擦除_1 {

    public static void main(String[] args) {
        final Pack<String> stringPack = new Pack<>();
        final Pack<Integer> integerPack = new Pack<>();
        final PackString packString = new PackString();
        RefUtil.打印属性信息(stringPack);
        RefUtil.打印属性信息(integerPack);
        RefUtil.打印属性信息(packString);
    }


    @Getter
    @Setter
    static class Pack<T>{
        T data;
    }

    @Getter
    @Setter
    static class PackString{
        String data;
    }
}
