package cn.xiaoke.fanxin.泛型擦除;

import cn.xiaoke.fanxin.RefUtil;
import lombok.Getter;
import lombok.Setter;

public class 泛型擦除_2 {

    public static void main(String[] args) {
        final Pack<String> stringPack = new Pack<>();
        final Pack<Integer> integerPack = new Pack<>();
        final PackString packString = new PackString();
        RefUtil.打印方法信息(stringPack);
        RefUtil.打印方法信息(integerPack);
        RefUtil.打印方法信息(packString);


    }


    @Getter
    @Setter
    static class Pack<T>{
        T data;
        String aaa(Integer index){
            return null;
        }
    }

    @Getter
    @Setter
    static class PackString{
        String data;
        String aaa(Integer index){
            return null;
        }
    }
}
