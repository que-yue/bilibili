package cn.xiaoke.fanxin.泛型擦除;

import cn.xiaoke.fanxin.RefUtil;
import lombok.Getter;
import lombok.Setter;
import org.apache.poi.ss.formula.functions.T;

import java.util.Comparator;

public class 泛型擦除_3 {

    public static void main(String[] args) {
        final Pack<B> b = new Pack<>();
        b.setData(new C());
        final Pack<C> c = new Pack<>();
        final Pack2<C> c2 = new Pack2<>();
        final Pack3<C> c3 = new Pack3<>();
        RefUtil.打印属性信息(b);
        RefUtil.打印属性信息(c);
        RefUtil.打印属性信息(c2);
        RefUtil.打印属性信息(c3);
    }


    @Getter
    @Setter
    static class Pack<T extends B>{
        T data;
    }

    @Getter
    @Setter
    static class PackSub<T extends B> extends Pack<T> {
        T data;
    }

    static class PackSub2  extends Pack<C> {
        T data;
    }

    static class Pack2<T extends  Comparable >{
        T data;
    }

    @Getter
    @Setter
    static class Pack3<T extends Comparable  &  Comparator >{
        T data;
    }

    static class A{

    }
    static class B extends A{

    }
    static class C extends B implements Comparable<C>,Comparator<C>{

        @Override
        public int compareTo(C o) {
            return 0;
        }

        @Override
        public int compare(C o1, C o2) {
            return 0;
        }
    }
}
