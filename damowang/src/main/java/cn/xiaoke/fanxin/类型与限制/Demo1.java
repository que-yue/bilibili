//package cn.xiaoke.泛型.类型与限制;
//
//public class Demo1 {
//
//
//    public static void main(String[] args) {
//        Pack<String> stringPack = new Pack<>();
//        StrPack stringPackSub = new StrPack();
//        stringPack = stringPackSub;
//        stringPackSub = stringPack;
//        Pack<Integer> integerPack = new Pack<>();
//        Pack<? extends Integer> integerPack2 = new Pack<>();
//        Pack<? super Integer> integerPack3 = new Pack<>();
//        integerPack = integerPack2;
//        integerPack = integerPack3;
//        integerPack2 = integerPack;
//        integerPack2 = integerPack3;
//        integerPack3 = integerPack;
//        integerPack3 = integerPack2;
//
//        final Pack<?> pack = new Pack<>();
//        pack.setT(null);
//        Pack.asd(pack);
//
//    }
//
//    static class Pack<T>{
//        T t;
//
//        public T getT() {
//            return t;
//        }
//
//        public void setT(T t) {
//            this.t = t;
//        }
//
//        public static <A> void asd(Pack<A> pack){
//
//        }
//
//    }
//    static class StrPack extends Pack<String>{
//
//    }
//}
