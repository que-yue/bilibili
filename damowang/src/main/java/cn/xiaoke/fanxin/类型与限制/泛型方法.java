package cn.xiaoke.fanxin.类型与限制;

import cn.xiaoke.fanxin.RefUtil;

import java.util.List;

public class 泛型方法 {

    public static void main(String[] args) {
        final Pack<String> pack = new Pack<>();
        final Pack<String> packSub = new PackSub();
        RefUtil.打印全部方法信息(pack);
        RefUtil.打印全部方法信息(packSub);
    }

    static class Pack<T> {
        T t;

        public T getT() {
            return t;
        }

        public <A> A getAndSetObj(List<String> list) {
            return null;
        }



    }

    static class PackSub extends Pack<String> {

//        public String  getAndSetObj(List<Integer> list) {
//            return null;
//        }

    }

}
