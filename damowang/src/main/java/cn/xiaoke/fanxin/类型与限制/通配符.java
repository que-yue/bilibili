package cn.xiaoke.fanxin.类型与限制;


public class 通配符 {

    public static void main(String[] args) {

        final Pack<Object> objectPack = new Pack<>();
        final Pack<Number> numberPack = new Pack<>();
        final Pack<Integer> integerPack = new Pack<>();

//        numberPack.setT2(123);
//        objectPack = numberPack;
//        objectPack.setT2(new Object());

//        final Number t2 = numberPack.getT2();


        soutHashCode(objectPack);
        soutHashCode(numberPack);
        soutHashCode(integerPack);
//        swapHelp(objectPack);
    }
    public static void soutHashCode(Pack<?> packNum){
        System.out.println(packNum.hashCode());
    }

    public static void swap(Pack<?> pack){
        final Object t1 = pack.getT1();
//        pack.setT1(pack.getT2());
//        pack.setT2(t1);
        swapHelp(pack);
    }

    public static <T> void swapHelp(Pack<T> pack){
        final T t1 = pack.getT1();
        pack.setT1(pack.getT2());
        pack.setT2(t1);
    }

    static class Pack<T>{
        T t1;
        T t2;

        public T getT1() {
            return t1;
        }

        public void setT1(T t1) {
            this.t1 = t1;
        }

        public T getT2() {
            return t2;
        }

        public void setT2(T t2) {
            this.t2 = t2;
        }
    }


    static class Node<T>{

    }
}
