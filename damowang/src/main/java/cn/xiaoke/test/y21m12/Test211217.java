package cn.xiaoke.test.y21m12;

import cn.xiaoke.util.ListToTreeUtil;
import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class Test211217 {

    @Data
    @Accessors(chain = true)
    public static class Node {
        Integer id;

        Integer pid;
        List<Node> children;

    }


    public static void main(String[] args) {
        final List<Node> nodeList = createRandomTestTreeInfoList(2, 2, 2);
        System.out.println(JSON.toJSONString(nodeList));


        ListToTreeUtil.listToTree(nodeList, Node::getId, Node ::getPid, Node ::getChildren, Node::setChildren);


    }


    public static List<Node> createRandomTestTreeInfoList(int tierN, int rootN, int childN) {
        final Random random = new Random();

        final int tier = tierN == 0 ? 5 + random.nextInt(5) : tierN;
        final List<Node> testTrees = new ArrayList<>();

        Queue<Node> pre = new LinkedList<>();

        final int rootNum = rootN == 0 ? 1 + random.nextInt(5) : rootN;

        for (int i = 0; i < rootNum; i++) {
            final Node testTree = new Node().setId(testTrees.size());
            testTrees.add(testTree);
            pre.add(testTree);
        }
        for (int i = 0; i < tier; i++) {
            final int childNum = childN == 0 ? 3 + random.nextInt(3) : childN;

            final int size = pre.size();

            for (int j = 0; j < size; j++) {
                final Integer pid = Objects.requireNonNull(pre.poll()).getId();
                for (int k = 0; k < childNum; k++) {
                    final Node child = new Node().setId(testTrees.size()).setPid(pid);
                    testTrees.add(child);
                    pre.offer(child);
                }
            }
        }
        return testTrees;
    }
}

