package cn.xiaoke.test.y21m12;

import cn.xiaoke.util.ListToTreeUtil;
import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test211231 {


    public static void main(String[] args) {
        final Random random = new Random();


        final List<NameNode> nameNodes1 = createRandomTestTreeInfoList(()->{
            return new NameNode().setName(String.valueOf((char)(97 + random.nextInt(3))));
        },NameNode::setId,NameNode ::setPid,2, 2, 2);


        final List<NameNode> nameNodes2 = nameNodes1.stream().map((n) -> {
            return new NameNode().setId(n.getId()).setPid(n.getPid()).setName(n.getName());
        }).collect(Collectors.toList());

        final List<NameNode> nameNodes3= nameNodes1.stream().map((n) -> {
            return new NameNode().setId(n.getId()).setPid(n.getPid()).setName(n.getName());
        }).collect(Collectors.toList());

        final List<NameNode> nameNodes4 = nameNodes1.stream().map((n) -> {
            return new NameNode().setId(n.getId()).setPid(n.getPid()).setName(n.getName());
        }).collect(Collectors.toList());

        final List<NameNode> trees1 = ListToTreeUtil.listToTree(nameNodes1, NameNode::getId, NameNode::getPid, NameNode::getChildren, NameNode::setChildren);
        final List<NameNode> trees2 = ListToTreeUtil.listToTree(nameNodes2, NameNode::getId, NameNode::getPid, NameNode::getChildren, NameNode::setChildren);
        final List<NameNode> trees3 = ListToTreeUtil.listToTree(nameNodes3, NameNode::getId, NameNode::getPid, NameNode::getChildren, NameNode::setChildren);
        final List<NameNode> trees4 = ListToTreeUtil.listToTree(nameNodes4, NameNode::getId, NameNode::getPid, NameNode::getChildren, NameNode::setChildren);



        String searchStr = "b";
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        模糊搜索树保留相关节点(trees1,searchStr);
        System.out.println("========================================");
        System.out.println(JSON.toJSONString(trees1));

        模糊搜索树保留相关节点2(trees2,searchStr);
        System.out.println("========================================");
        System.out.println(JSON.toJSONString(trees2));


        递归_模糊搜索树保留相关节点(trees3,searchStr);
        System.out.println("========================================");
        System.out.println(JSON.toJSONString(trees3));

        递归_模糊搜索树保留相关节点2(trees4,searchStr);
        System.out.println("========================================");
        System.out.println(JSON.toJSONString(trees4));

    }



    public static boolean 递归_模糊搜索树保留相关节点(List<NameNode> trees,String str){
        if (trees == null || trees.isEmpty()) return false;
        boolean flag = false;
        for (int i = trees.size() - 1; i >= 0; i--) {
            final NameNode node = trees.get(i);
            final List<NameNode> children = node.getChildren();
            final boolean childFlag = 递归_模糊搜索树保留相关节点(children, str);
            if (!childFlag){
                final boolean contains = node.getName().contains(str);
                if (contains){
                    flag = true;
                }else{
                    trees.remove(i);
                }
            }else{
                flag = true;
            }
        }
        return flag;
    }


    public static boolean 递归_模糊搜索树保留相关节点2(List<NameNode> trees,String str){
        if (trees == null || trees.isEmpty()) return false;
        final long count = Stream.iterate(trees.size() - 1, (index) -> --index).limit(trees.size()).filter(i -> {
            final NameNode node = trees.get(i);
            return 递归_模糊搜索树保留相关节点2(node.getChildren(), str) || node.getName().contains(str) || trees.remove(i.intValue()) == null;
        }).count();
        return count > 0;
    }





//    public static <T> Boolean treeSearchFlag(List<T> tree, Predicate<T> searchPredicate, Function<T, List<T>> getChildFun) {
//        if (Objects.isNull(tree) || tree.isEmpty()) {
//            return false;
//        }
//        boolean flag;
//        for (int i = tree.size() - 1; i >= 0; i--) {
//            flag = searchPredicate.test(tree.get(i));
//            if (treeSearchFlag(getChildFun.apply(tree.get(i)), searchPredicate, getChildFun)) {
//                flag = true;
//            }
//            if (!flag) {
//                tree.remove(i);
//            }
//        }
//        return !tree.isEmpty();
//    }


    public static <T> Boolean treeSearchFlag(List<T> tree, Predicate<T> searchPredicate, Function<T, List<T>> getChildFun) {
        if (Objects.isNull(tree) || tree.isEmpty()) {
            return false;
        }
        final long count = Stream.iterate(tree.size() - 1, (index) -> --index).limit(tree.size()).filter(i -> {
            final T treeNode = tree.get(i);
            return treeSearchFlag(getChildFun.apply(treeNode), searchPredicate, getChildFun) || searchPredicate.test(treeNode) || tree.remove(i.intValue()) == null;
        }).count();
        return count > 0;
    }




    public static void 模糊搜索树保留相关节点(List<NameNode> trees,String str){
        for (int i = trees.size() - 1; i >= 0; i--) {
            final NameNode nameNode = trees.get(i);
            if (!判断某个节点是否符合条件(nameNode,str)){
                trees.remove(i);
            }

        }
    }

    public static boolean 判断某个节点是否符合条件(NameNode node,String str){
        if (node == null) return false;
        boolean curFlag = node.getName().contains(str);
        final List<NameNode> children = node.getChildren();
        if (children != null){
            for (int i = children.size() - 1; i >= 0; i--) {
                final NameNode childNode = children.get(i);
                final boolean childFlag = 判断某个节点是否符合条件(childNode, str);
                if (childFlag){
                    curFlag = true;
                }else{
                    children.remove(i);
                }
            }
        }
        return curFlag;
    }

    public static void 模糊搜索树保留相关节点2(List<NameNode> trees,String str){
        Stream.iterate(trees.size() - 1,(index) -> --index).limit(trees.size()).forEach(i ->{
            if (!判断某个节点是否符合条件2(trees.get(i), str)){
                trees.remove(i.intValue());
            }
        });
    }
    public static boolean 判断某个节点是否符合条件2(NameNode node,String str){
        if (node == null) return false;
        final Boolean curFlag = Optional.ofNullable(node.getChildren()).map(children -> {
            final long count = Stream.iterate(children.size() - 1, (index) -> --index).limit(children.size()).filter(i -> {
                final NameNode childNode = children.get(i);
                return 判断某个节点是否符合条件2(childNode, str) || children.remove(i.intValue()) == null;
            }).count();
            return count > 0 || node.getName().contains(str);
        }).orElseGet(() ->node.getName().contains(str));

        return curFlag;
    }






















































    public static <T> List<T> createRandomTestTreeInfoList(Supplier<T> supplier, BiConsumer<T,Integer> idFun,BiConsumer<T,Integer> pidFun, int tierN, int rootN, int childN) {
        final Random random = new Random();

        final int tier = tierN == 0 ? 5 + random.nextInt(5) : tierN;
        final List<T> testTrees = new ArrayList<>();

        Queue<Integer> pre = new LinkedList<>();

        final int rootNum = rootN == 0 ? 1 + random.nextInt(5) : rootN;

        for (int i = 0; i < rootNum; i++) {
            final T testTree = supplier.get();
            final int id = testTrees.size();
            idFun.accept(testTree,id);
            testTrees.add(testTree);
            pre.add(id);
        }
        for (int i = 0; i < tier; i++) {
            final int childNum = childN == 0 ? 3 + random.nextInt(3) : childN;
            final int size = pre.size();
            for (int j = 0; j < size; j++) {
                final Integer parentId = pre.poll();

                for (int k = 0; k < childNum; k++) {
                    final T child = supplier.get();
                    final int childId = testTrees.size();
                    idFun.accept(child,childId);
                    pidFun.accept(child,parentId);
                    testTrees.add(child);
                    pre.offer(childId);
                }
            }
        }
        return testTrees;
    }
}
@Data
@Accessors(chain = true)
class NameNode {
    Integer id;
    String name;
    Integer pid;
    List<NameNode> children;
}

