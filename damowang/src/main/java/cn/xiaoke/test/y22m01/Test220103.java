package cn.xiaoke.test.y22m01;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class Test220103 {


    public static void main(String[] args) {
        String jsonStr = "{\"result\":{\"rotated_image_height\":1200,\"image_angle\":0,\"rotated_image_width\":860,\"item_list\":[{\"value\":\"False\",\"position\":[],\"key\":\"BizLicenseIsCopy\",\"description\":\"是否为副本\"},{\"value\":\"False\",\"position\":[],\"key\":\"BizLicenseIsElectronic\",\"description\":\"是否为电子营业执照\"},{\"value\":\"320111000000000000000\",\"position\":[523,351,694,352,694,369,523,368],\"key\":\"BizLicenseSerialNumber\",\"description\":\"证照编号\"},{\"value\":\"320111600060000\",\"position\":[523,375,684,374,684,396,523,396],\"key\":\"BizLicenseRegistrationCode\",\"description\":\"注册号\"},{\"value\":\"\",\"position\":[],\"key\":\"BizLicenseRegCapital\",\"description\":\"注册资本\"},{\"value\":\"\",\"position\":[],\"key\":\"BizLicenseCreditCode\",\"description\":\"统一社会信用代码\"},{\"value\":\"南京市浦口区信小信小吃铺\",\"position\":[319,431,582,430,582,457,319,458],\"key\":\"BizLicenseCompanyName\",\"description\":\"名称\"},{\"value\":\"合小合\",\"position\":[319,597,392,597,393,623,319,623],\"key\":\"BizLicenseOwnerName\",\"description\":\"经营者\"},{\"value\":\"个体工商户\",\"position\":[323,487,433,486,434,513,323,514],\"key\":\"BizLicenseCompanyType\",\"description\":\"类型\"},{\"value\":\"南京市浦口区蒲园村88-88号\",\"position\":[318,542,588,540,589,568,318,569],\"key\":\"BizLicenseAddress\",\"description\":\"经营场所\"},{\"value\":\"面食小吃制售。（依法须经批准的项目，经相关部门批准后方可开展经营活动）\",\"position\":[326,757,731,755,731,800,326,800],\"key\":\"BizLicenseScope\",\"description\":\"经营范围\"},{\"value\":\"2005年09月05日\",\"position\":[324,705,474,706,474,731,324,730],\"key\":\"BizLicenseStartTime\",\"description\":\"注册日期\"},{\"value\":\"\",\"position\":[],\"key\":\"BizLicenseOperatingPeriod\",\"description\":\"营业期限\"},{\"value\":\"个人经营\",\"position\":[326,650,415,651,415,677,326,677],\"key\":\"BizLicenseComposingForm\",\"description\":\"组成形式\"},{\"value\":\"\",\"position\":[],\"key\":\"BizLicensePaidInCapital\",\"description\":\"实收资本\"},{\"value\":\"2010年04月09日\",\"position\":[517,1040,705,1041,704,1065,517,1064],\"key\":\"BizLicenseRegistrationDate\",\"description\":\"登记日期\"}],\"type\":\"business_license\"},\"message\":\"success\",\"code\":200}";

        final JSONObject json = JSON.parseObject(jsonStr);

        final JSONObject result = json.getJSONObject("result");
        final JSONArray itemList = result.getJSONArray("item_list");
        for (int i = 0; i < itemList.size(); i++) {
            final JSONObject item = itemList.getJSONObject(i);
            final String description = item.getString("description");
            if (description != null && (description.equals("证照编号") || description.equals("名称"))){
                System.out.println(item.getString("value"));
            }
        }
    }
}
