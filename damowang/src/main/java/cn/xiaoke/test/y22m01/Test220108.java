package cn.xiaoke.test.y22m01;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.invoke.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test220108 {


    static final Map<Integer, BiConsumer<Student, StudentStatistics>> map;
    static final Map<Integer, AddStudentClass> addMap;

    static {
        map = new HashMap<>();


        final Field[] fields = StudentStatistics.class.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            final String fieldName = field.getName();
            final char c = fieldName.charAt(fieldName.length() - 1);
            @SuppressWarnings("unchecked")
            BiConsumer<Student, StudentStatistics> biConsumer = (student, statistics) -> {
                try {
                    Optional.ofNullable((List<Student>) field.get(statistics)).map((list) -> {
                        list.add(student);
                        return list;
                    }).orElseGet(() -> {
                        final List<Student> list = new ArrayList<>();
                        try {
                            field.set(statistics, list);
                            list.add(student);
                            return list;
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        return null;
                    });
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            };
            final int grade = Integer.parseInt(String.valueOf(c));
            map.put(grade, biConsumer);
        }


        addMap = new HashMap<>();
        final MethodHandles.Lookup lookup = MethodHandles.lookup();
        Arrays.stream(StudentStatistics.class.getDeclaredMethods()).filter((method) -> {
            final String methodName = method.getName();
            return methodName.startsWith("get") || methodName.startsWith("set");
        }).collect(Collectors.groupingBy((method) -> method.getName().substring(3)))
                .forEach((k, v) -> {
                    final AddStudentClass addStudentClass = new AddStudentClass();
                    addMap.put(Integer.parseInt(k.substring(k.length() - 1)), addStudentClass);
                    v.forEach(method -> {
                        final String methodName = method.getName();
                        try {
                            final MethodHandle methodHandle = lookup.unreflect(method);

                            if (methodName.startsWith("get")) {
                                final CallSite callSite = LambdaMetafactory.metafactory(
                                        lookup
                                        , "apply"
                                        , MethodType.methodType(Function.class)
                                        , MethodType.methodType(Object.class, Object.class)
                                        , methodHandle
                                        , MethodType.methodType(List.class, StudentStatistics.class)
                                );
                                final Function<StudentStatistics, List<Student>> getFun = (Function<StudentStatistics, List<Student>>) callSite.getTarget().invokeExact();
                                addStudentClass.setGetFun(getFun);

                            } else {
                                final CallSite callSite = LambdaMetafactory.metafactory(
                                        lookup
                                        , "accept"
                                        , MethodType.methodType(BiConsumer.class)
                                        , MethodType.methodType(void.class, Object.class, Object.class)
                                        , methodHandle
                                        , MethodType.methodType(void.class, StudentStatistics.class, List.class)
                                );
                                final BiConsumer<StudentStatistics, List<Student>> setFun = (BiConsumer<StudentStatistics, List<Student>>) callSite.getTarget().invokeExact();
                                addStudentClass.setSetFun(setFun);
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    });
                });
    }


    public static void addStudentFun(Student stu, StudentStatistics statistics
            , Function<StudentStatistics, List<Student>> getFun, BiConsumer<StudentStatistics, List<Student>> setFun) {
        Optional.ofNullable(getFun.apply(statistics)).map(list -> {
            list.add(stu);
            return list;
        }).orElseGet(() -> {
            final List<Student> list = new ArrayList<>();
            setFun.accept(statistics, list);
            list.add(stu);
            return list;
        });
    }

    public static void main(String[] args) {
        final List<Student> students = randomStudent(100);

        final StudentStatistics statistics = new StudentStatistics();
        for (Student student : students) {
//            map.get(student.grade).accept(student, statistics);
            addMap.get(student.grade).addStudentFun(student, statistics);
        }
        System.out.println(JSON.toJSONString(statistics));
    }

    public static List<Student> addStudent(Student stu, List<Student> students) {
        return Optional.ofNullable(students).map((list) -> {
            list.add(stu);
            return list;
        }).orElseGet(() -> {
            final List<Student> list = new ArrayList<>();
            list.add(stu);
            return list;
        });
    }


    //随机创建个学生
    public static List<Student> randomStudent(int n) {
        Random random = new Random();
        return Stream.generate(() -> new Student().setGrade(1 + random.nextInt(9))
                .setName("学生" + random.nextInt(1000000))).limit(n).collect(Collectors.toList());
    }
}


@Data
@Accessors(chain = true)
class Student {
    // 年级
    Integer grade;
    //姓名
    String name;
}

//统计类
@Data
class StudentStatistics {
    //分别代表一年级到九年级
    List<Student> list1;
    List<Student> list2;
    List<Student> list3;
    List<Student> list4;
    List<Student> list5;
    List<Student> list6;
    List<Student> list7;
    List<Student> list8;
    List<Student> list9;
}

@Data
class AddStudentClass {
    private Function<StudentStatistics, List<Student>> getFun;
    private BiConsumer<StudentStatistics, List<Student>> setFun;

    public void addStudentFun(Student stu, StudentStatistics statistics) {
        Optional.ofNullable(getFun.apply(statistics)).map((list -> {
            list.add(stu);
            return list;
        })).orElseGet(() -> {
            final List<Student> list = new ArrayList<>();
            list.add(stu);
            setFun.accept(statistics, list);
            return list;
        });
    }
}

