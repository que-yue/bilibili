package cn.xiaoke.test.y22m01;

import cn.xiaoke.util.ListToTreeUtil;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.asm.Type;
import org.springframework.cglib.core.ClassInfo;
import org.springframework.cglib.core.MethodInfo;
import org.springframework.cglib.core.ReflectUtils;
import org.springframework.cglib.core.Signature;

import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.function.Function;


public class Test220109 {
    @Data
    @Accessors(chain = true)
    static class Student{
        Integer age;
        String name;
    }

    public static void main(String[] args) throws Exception {
        final Class<Student> aClass = Student.class;
        final Method method = aClass.getMethod("getName");
        System.out.println(method);
        final Class<?> returnType = method.getReturnType();
        System.out.println(returnType);
        final String name = returnType.getName();
        final String simpleName = returnType.getSimpleName();
        final String typeName = returnType.getTypeName();
        final String canonicalName = returnType.getCanonicalName();

        final MethodInfo methodInfo = ReflectUtils.getMethodInfo(method);
        final Signature signature = methodInfo.getSignature();
        final String descriptor = signature.getDescriptor();
        System.out.println("descriptor = " + descriptor);
        System.out.println(aClass);
        test(Student::getName);
//        test(Student::getId);
//        test2(Student::getName);
        System.out.println();
    }


    public static void test(ListToTreeUtil.XKFunction<Student, ?> fun)  {
        final Student Student = new Student().setName("xiaoke");
        System.out.println("fun.apply(Student) = " + fun.apply(Student));
        final Class<? extends ListToTreeUtil.XKFunction> aClass = fun.getClass();
        final Method[] methods = aClass.getMethods();
        final Method[] declaredMethods = aClass.getDeclaredMethods();
        final Method method;
        try {
            method = aClass.getDeclaredMethod("apply",Object.class);
            final MethodInfo methodInfo = ReflectUtils.getMethodInfo(method);
            final ClassInfo classInfo = ReflectUtils.getClassInfo(Student.class);
            final Type type = classInfo.getType();
            final String className = type.getClassName();
            final String s = type.toString();
            System.out.println();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        System.out.println("getLambdaName(fun) = " + getLambdaName(fun));
    }

    public static void test2(Function<Student, ?> fun) {
        final Student Student = new Student().setName("name");
        System.out.println("fun.apply(Student) = " + fun.apply(Student));
        System.out.println("getLambdaName(fun) = " + getLambdaName(fun));
    }

    public static String getLambdaName(Function<Student, ?> fun) {
        final Class<? extends Function> aClass = fun.getClass();
        final Method method;
        try {
            method = aClass.getDeclaredMethod("writeReplace");
            method.setAccessible(true);
            SerializedLambda lambda = (SerializedLambda) method.invoke(fun);
            soutParam(lambda);
            return lambda.getImplMethodName();
        } catch (Exception e) {
        }
        return null;
    }

    public static void soutParam(SerializedLambda lambda){
        final Method readResolve;
        try {
            readResolve = SerializedLambda.class.getDeclaredMethod("readResolve");
            readResolve.setAccessible(true);
            final Object invoke = readResolve.invoke(lambda);
            final ListToTreeUtil.XKFunction xkFunction = (ListToTreeUtil.XKFunction) invoke;

            System.out.println("=====================================");
            final String capturingClass = lambda.getCapturingClass();
            System.out.println("capturingClass = " + capturingClass);//cn/xiaoke/test/y22m01/Test220109
            final String functionalInterfaceClass = lambda.getFunctionalInterfaceClass();
            System.out.println("functionalInterfaceClass = " + functionalInterfaceClass);//cn/xiaoke/util/ListToTreeUtil$XKFunction
            final String functionalInterfaceMethodName = lambda.getFunctionalInterfaceMethodName();
            System.out.println("functionalInterfaceMethodName = " + functionalInterfaceMethodName);//apply
            final String functionalInterfaceMethodSignature = lambda.getFunctionalInterfaceMethodSignature();
            System.out.println("functionalInterfaceMethodSignature = " + functionalInterfaceMethodSignature);//(Ljava/lang/Object;)Ljava/lang/Object;
            final int implMethodKind = lambda.getImplMethodKind();
            System.out.println("implMethodKind = " + implMethodKind);//5
            final String implClass = lambda.getImplClass();
            System.out.println("implClass = " + implClass);//cn/xiaoke/test/y22m01/Test220109$Student
            final String implMethodName = lambda.getImplMethodName();
            System.out.println("implMethodName = " + implMethodName);//getName
            final String implMethodSignature = lambda.getImplMethodSignature();
            System.out.println("implMethodSignature = " + implMethodSignature);//()Ljava/lang/String;
            final String instantiatedMethodType = lambda.getInstantiatedMethodType();
            System.out.println("instantiatedMethodType = " + instantiatedMethodType);//(Lcn/xiaoke/test/y22m01/Test220109$Student;)Ljava/lang/Object;
            final int capturedArgCount = lambda.getCapturedArgCount();
            System.out.println("capturedArgCount = " + capturedArgCount);//0
            final Object[] capturedArgs = new Object[capturedArgCount];
            for (int i = 0; i < capturedArgCount; i++) {
                final Object capturedArg = lambda.getCapturedArg(i);
                capturedArgs[i] = capturedArg;
            }
            System.out.println("capturedArgs = " + Arrays.toString(capturedArgs));//[]

            System.out.println("============================================");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
