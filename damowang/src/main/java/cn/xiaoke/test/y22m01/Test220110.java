package cn.xiaoke.test.y22m01;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.invoke.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Test220110 {


    public static void main(String[] args) throws Throwable {
        final Student stu1 = new Student().setAge(10).setName("张三");
//        stu1.setTeacherNames(new ArrayList<>());
        final Student stu2 = new Student().setAge(12).setName("李四");

//        addTeacherNames(stu1,Student::getTeacherNames,Student::setTeacherNames);

//        soutName(stu2::getName);
        final MethodHandles.Lookup lookup = MethodHandles.lookup();

        //获取方法句柄
        final Method getMethod = Student.class.getDeclaredMethod("getTeacherNames");

        final MethodHandle getMethodHandle = lookup.unreflect(getMethod);
        //动态调用点
        final CallSite getCallSite = LambdaMetafactory.metafactory(
                lookup
                , "apply"
                , MethodType.methodType(Function.class)
                , MethodType.methodType(Object.class, Object.class)
                , getMethodHandle
                , MethodType.methodType(List.class, Student.class)
        );
        final Function<Student, List<String>> getFun = (Function<Student, List<String>>)getCallSite.getTarget().invokeExact();

        final Method setMethod = Student.class.getDeclaredMethod("setTeacherNames",List.class);
        final MethodHandle setMethodHandle = lookup.unreflect(setMethod);

        final CallSite setCallSite = LambdaMetafactory.metafactory(
                lookup
                , "accept"
                , MethodType.methodType(BiConsumer.class)
                , MethodType.methodType(void.class,Object.class, Object.class)
                , setMethodHandle
                , MethodType.methodType(void.class, Student.class,List.class)
        );

        final BiConsumer<Student, List<String>> setFun = (BiConsumer<Student, List<String>>) setCallSite.getTarget().invokeExact();

        addTeacherNames(stu1,getFun,setFun);
        System.out.println(JSON.toJSONString(stu1));



        final Method getNameMethod = Student.class.getDeclaredMethod("getName");
        final MethodHandle getNameMethodHandle = lookup.unreflect(getNameMethod);

        final CallSite getNameCallSite = LambdaMetafactory.metafactory(
                lookup
                , "get"
                , MethodType.methodType(Supplier.class,Student.class)
                , MethodType.methodType(Object.class)
                , getNameMethodHandle
                , MethodType.methodType(String.class)
        );

        final Supplier<String> getNameFun = (Supplier<String>)getNameCallSite.getTarget().invokeExact(stu2);

        soutName(getNameFun);
    }


    @Data
    @Accessors(chain = true)
    static class Student {
        Integer age;
        String name;
        List<String> teacherNames;

    }

    public static void addTeacherNames(Student stu, Function<Student, List<String>> getFun
            , BiConsumer<Student, List<String>> setFun) {
        Random random = new Random();
        String teacher = "老师" + random.nextInt(10000);
        final List<String> apply = getFun.apply(stu);
        Optional.ofNullable(apply).map((list -> {
            list.add(teacher);
            return list;
        })).orElseGet(() -> {
            final List<String> list = new ArrayList<>();
            setFun.accept(stu, list);
            list.add(teacher);
            return list;
        });
    }

    public static void soutName(Supplier<String> nameFun) {
        System.out.println(nameFun.get());
    }
}
