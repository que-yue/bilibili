package cn.xiaoke.test.y22m01;

import java.util.Random;

public class Test220110_2 {


    //    public static void main(String[] args) {
//        final XkList list = new XkList();
//        final Random random = new Random();
//        for (int i = 0; i < 100000; i++) {
//            final int i1 = random.nextInt(3);
//            if (i1 == 0) {
//                list.add(new Object());
//            } else if (i1 == 1){
//                list.remove(random.nextInt(1 + list.size()));
//            }else {
//                list.add(new Object(),random.nextInt(1 + list.size()));
//            }
//        }
//    }
    public static void main(String[] args) {
        XkList arrs = new XkList(2);
        arrs.add(0, 0);
        arrs.add(1, 1);
        arrs.add(2, 2);
        arrs.add(3, 3);
        arrs.add(1, 11);
        for (int i = 0; i < arrs.size(); i++) {
            System.out.print(arrs.get(i) + "\t");
        }

        System.out.println();
        arrs.remove(2);
        for (int i = 0; i < arrs.size(); i++) {
            System.out.print(arrs.get(i) + "\t");
        }

    }

    static class XkList {

        Object get(int i){
            return arr[i];
        }
        int size = 0;
        Object[] arr;

        public XkList(int i){
            arr = new Object[i];
        }

        void add(Object obj) {
            kuorong();
            arr[size++] = obj;
        }

        void kuorong() {
            if (size >= arr.length) {
                Object[] oldArr = arr;
                arr = new Object[arr.length << 1];
                for (int i = 0; i < oldArr.length; i++) {
                    arr[i] = oldArr[i];
                }
            }
        }

        void add(int i,Object obj) {
            kuorong();
            if (i < 0 || i > size) return;
            if (i == size) {
                add(obj);
                return;
            }
            for (int k = size; k > i; k--) {
                arr[k] = arr[k - 1];
            }
            arr[i] = obj;
            size++;
        }

        Object remove(int i) {
            if (i < 0 || size <= 0) return null;
            final Object ret = arr[i];
            for (int j = i; j < size; j++) {
                arr[j] = arr[j + 1];
            }
            arr[size - 1] = null;
            size--;
            return ret;
        }

        int size() {
            return size;
        }
    }
}
