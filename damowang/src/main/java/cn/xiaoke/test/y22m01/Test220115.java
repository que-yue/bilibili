package cn.xiaoke.test.y22m01;


import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.function.Function;

public class Test220115 {


    public static void main(String[] args) {

        final Student student = new Student().setName("张三");

        final Name obj = (Name) Proxy.newProxyInstance(Test220115.class.getClassLoader(), new Class[]{Name.class,Function.class},new A(student));
        obj.sout();

        final String name = obj.getName2();
        System.out.println("name = " + name);
        System.out.println("===");

        final Function function = (Function) obj;
        System.out.println(function.apply("asd"));
    }


    interface Name{
        void sout();
        String getName2();
    }


    static class A implements InvocationHandler{
        Student student;

        public A(Student student){
            this.student = student;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("getName2")) {
                return method.invoke(student, args);
            }else if (method.getName().equals("apply")){
                return "asdasfdsgfdgdf:" + args[0];
            }
            System.out.println("方法名:"+method.getName());
            System.out.println("参数:" + JSON.toJSONString(args));
            final Object invoke = method.invoke(student, args);
            if (invoke != null){
                System.out.println("返回值:" + JSON.toJSONString(invoke));
            }
            return invoke;
        }
    }






    @Data
    @Accessors(chain = true)
    static class Student implements Name{
        String name;

        @Override
        public void sout() {
            System.out.println("我是studen的sout方法");
        }

        @Override
        public String getName2() {
            System.out.println("我是studen的getName2方法");
            return name;
        }

    }



}




