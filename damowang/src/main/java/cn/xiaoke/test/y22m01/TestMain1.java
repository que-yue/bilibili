package cn.xiaoke.test.y22m01;

import cn.xiaoke.util.ListToTreeUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.lang.invoke.*;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class TestMain1 {

    public static void main(String[] args) throws Throwable {
        final Student student = new Student().setAge(11).setName("张三");
        addTeacherNames(student, Student::getTeacherNames, Student::setTeacherNames);
        System.out.println(JSON.toJSONString(student));


        final Student student2 = new Student().setAge(12).setName("李四");


        MethodHandles.Lookup lookup = MethodHandles.lookup();
        //获取方法句柄
        final MethodHandle getMethodHandle = lookup.findVirtual(Student.class, "getTeacherNames", MethodType.methodType(List.class));
        final MethodHandle setMethodHandle = lookup.findVirtual(Student.class, "setTeacherNames", MethodType.methodType(Student.class, List.class));
        //动态调用点
        final CallSite getCallSite = LambdaMetafactory.metafactory(
                lookup
                , "apply"
                , MethodType.methodType(Function.class)
                , MethodType.methodType(Object.class, Object.class)
                , getMethodHandle
                , MethodType.methodType(List.class, Student.class)
        );

        final CallSite setCallSite = LambdaMetafactory.metafactory(
                lookup
                , "accept"
                , MethodType.methodType(BiConsumer.class)
                , MethodType.methodType(void.class, Object.class, Object.class)
                , setMethodHandle
                , MethodType.methodType(void.class, Student.class, List.class)
        );
        final Function<Student, List<String>> getFun = (Function<Student, List<String>>) getCallSite.getTarget().invokeExact();
        final BiConsumer<Student, List<String>> setFun = (BiConsumer<Student, List<String>>) setCallSite.getTarget().invokeExact();

        addTeacherNames(student2, getFun, setFun);
        System.out.println(JSON.toJSONString(student2));
    }


    public static void main3(String[] args) throws Throwable {
        final Student student = new Student().setAge(11).setName("张三");
        addTeacherNames(student, Student::getTeacherNames, Student::setTeacherNames);
        System.out.println(JSON.toJSONString(student));


        final Student student2 = new Student().setAge(12).setName("李四");


        MethodHandles.Lookup lookup = MethodHandles.lookup();

        final Class<Student> studentClass = Student.class;
        final Method getMethod = studentClass.getDeclaredMethod("getTeacherNames");
        final Method setMethod = studentClass.getDeclaredMethod("setTeacherNames", List.class);
        final Method getInterfaceMethod = Function.class.getDeclaredMethod("apply", Object.class);
        final Method setInterfaceMethod = BiConsumer.class.getDeclaredMethod("accept", Object.class, Object.class);

        //获取方法句柄
        final MethodHandle getMethodHandle = lookup.unreflect(getMethod);
        final MethodHandle setMethodHandle = lookup.unreflect(setMethod);
        final MethodHandle getInterfaceHandle = lookup.unreflect(getInterfaceMethod);
        final MethodHandle setInterfaceHandle = lookup.unreflect(setInterfaceMethod);
        //动态调用点
        final CallSite getCallSite = LambdaMetafactory.metafactory(
                lookup
                , getInterfaceMethod.getName()
                , MethodType.methodType(Function.class)
                , getInterfaceHandle.type().dropParameterTypes(0, 1)
                , getMethodHandle
                , getMethodHandle.type()
        );
        final Method getInterfaceMethod2 = Supplier.class.getDeclaredMethod("get");
        final MethodHandle getMethodHandle2 = lookup.unreflect(getMethod);
        final MethodHandle getInterfaceHandle2 = lookup.unreflect(getInterfaceMethod2);
        final MethodType methodType = getMethodHandle.type().dropParameterTypes(0, 1).changeReturnType(Object.class);


        //===================================================

        Student oAAA = Student.class.newInstance();
        oAAA.setTeacherNames(new ArrayList<>());

        final Method getMethodAAA = Student.class.getDeclaredMethod("getTeacherNames");
        MethodHandles.Lookup lookupAAA = MethodHandles.lookup();

        MethodHandle mh = lookupAAA.unreflect(getMethodAAA);

        MethodType type = mh.type();

        MethodType factoryType = MethodType.methodType(Supplier.class, type.parameterType(0));
        type = type.dropParameterTypes(0, 1).changeReturnType(Object.class);

        final CallSite get = LambdaMetafactory
                .metafactory(
                        lookupAAA
//                , getInterfaceMethod2.getName()
                        , "get"
//                , MethodType.methodType(Supplier.class, Student.class)
                        , factoryType
//                , getMethodHandle.type().dropParameterTypes(0, 1).changeReturnType(Object.class)
                        , type
                        , mh
//                , getMethodHandle.type().dropParameterTypes(0, 1).changeReturnType(Object.class)
                        , type
                );
        final Supplier o1 = (Supplier) get.getTarget().invokeExact(oAAA);
        final Object o = o1.get();
        System.out.println(o);
        //===================================================
        System.out.println("===================");
        final CallSite setCallSite = LambdaMetafactory.metafactory(
                lookup
                , setInterfaceMethod.getName()
                , MethodType.methodType(BiConsumer.class)
                , setInterfaceHandle.type().dropParameterTypes(0, 1)
                , setMethodHandle
                , setMethodHandle.type().changeReturnType(void.class)
        );
        final Function<Student, List<String>> getFun = (Function<Student, List<String>>) getCallSite.getTarget().invokeExact();
        final BiConsumer<Student, List<String>> setFun = (BiConsumer<Student, List<String>>) setCallSite.getTarget().invokeExact();

        addTeacherNames(student2, getFun, setFun);
        System.out.println(JSON.toJSONString(student2));
    }


    public static void main2(String[] args) {
        String methodName = "getName";
        try {
            //通过全类名，获取类的实例
            //获取到类的对象，要求该类必须有无参构造
            Student o = Student.class.newInstance();
            o.setTeacherNames(new ArrayList<>()).setName("撒佛挡杀佛");
            //获取方法对象
            Method method = Student.class.getDeclaredMethod(methodName);
            MethodHandles.Lookup lookup = MethodHandles.lookup();
            //指定方法不以反射运行
            MethodHandle mh = lookup.unreflect(method);
            //获取代理对象，注意，第二个参数的字符串必须为函数式接口里的方法名
            Supplier operator = (Supplier) LambdaMetafactory
                    .metafactory(
                            lookup
                            , "get"
                            , MethodType.methodType(Supplier.class, Student.class)
                            , MethodType.methodType(Object.class)
                            , mh
                            , MethodType.methodType(Object.class)
                    ).getTarget().invokeExact(o);
            final Object o1 = operator.get();
            System.out.println(o1);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Data
    @Accessors(chain = true)
    static class Student {
        Integer age;
        String name;
        List<String> teacherNames;

    }

    public static void addTeacherNames(Student stu, Function<Student, List<String>> getFun
            , BiConsumer<Student, List<String>> setFun) {
        Random random = new Random();
        String teacher = "老师" + random.nextInt(10000);
        final List<String> apply = getFun.apply(stu);
        Optional.ofNullable(apply).map((list -> {
            list.add(teacher);
            return list;
        })).orElseGet(() -> {
            final List<String> list = new ArrayList<>();
            setFun.accept(stu, list);
            list.add(teacher);
            return list;
        });
    }

    public static void soutName(Supplier<String> nameFun) {
        System.out.println(nameFun.get());
    }
}
