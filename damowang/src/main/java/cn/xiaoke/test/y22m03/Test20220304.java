/*
 * 版权信息 : @ 聚均科技
 */
package cn.xiaoke.test.y22m03;


import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.*;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/3/4 11:11
 */
@Slf4j
public class Test20220304 {

    static String url = "http://image-linshi.xiaokedamowang.cn/%E6%B0%B4%E5%8D%B0.png";

    private static final String SPACE = " ";
    private static final String LIST = "#list";

    private static final BiConsumer<HasAnnotationTagResult, Element> tableConsumer = (info, tbl) -> {
        if (!info.has) return;
        StringBuilder xmlStrBuilder = new StringBuilder(LIST);
        final Map<String, String> map = new HashMap<>();
        final String[] kvs = info.value.split(",");
        for (String kvStr : kvs) {
            final String[] kv = kvStr.split("=");
            map.put(kv[0].trim(), kv[1].trim());
        }
        xmlStrBuilder.append(SPACE).append(map.get("source")).append(SPACE).append("as").append(SPACE).append(map.get("var")).append(SPACE);
        final QName aQname = DocumentHelper.createQName("aQname");
        try {
            final Field name = QName.class.getDeclaredField("name");
            final Field qualifiedName = QName.class.getDeclaredField("qualifiedName");
            name.setAccessible(true);
            qualifiedName.setAccessible(true);
            name.set(aQname, xmlStrBuilder.toString());
            qualifiedName.set(aQname, LIST);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        final Element element = DocumentHelper.createElement(aQname);
        final Element tr = tbl.elements("tr").get(1);
        tbl.remove(tr);
        element.add(tr);
        tbl.add(element);
    };

    public static void main(String[] args) throws Exception {
        final BufferedImage read1 = ImgUtil.read(new URL(url));
        final byte[] bytes = ImgUtil.toBytes(read1, "jpg");
        FileUtil.writeBytes(bytes, new File("D:\\javaproject\\bilibili\\a.jpg"));

        SAXReader saxReader = new SAXReader();
        final Document read = saxReader.read(new File("C:\\Users\\xiaokedamowang\\Desktop\\测试模板XML-5.xml"));
        final Element rootElement = read.getRootElement();
        System.out.println("===========");
        System.out.println(rootElement.getNamespace());
        System.out.println(rootElement.getNamespacePrefix());
        System.out.println(rootElement.getQualifiedName());
        System.out.println("===========");
        final List<Element> elements = rootElement.elements();
        for (Element element : elements) {

            System.out.println("--------------------------");
            System.out.println("element.getData() = " + element.getData());
            System.out.println("element.getText() = " + element.getText());
            System.out.println("element.getName() = " + element.getName());
            System.out.println("element.getQName() = " + element.getQName());
            System.out.println("element.getQualifiedName() = " + element.getQualifiedName());
            System.out.println("--------------------------");
        }
        System.out.println("=====================");

        final List<Element> body = bfsFindElementsByCondition(rootElement, getNameCondition("body"));
        System.out.println(body);

        Predicate<Element> addCondition = getNameCondition("tbl");
        final List<Element> tbls = bfsFindElementsByCondition(rootElement, addCondition);
        System.out.println(tbls);

        for (Element tbl : tbls) {
            final HasAnnotationTagResult result = dfsHasAnnotationTagResult(tbl, matchAnnotationTagAndRemove());
            tableConsumer.accept(result, tbl);
        }

        Predicate<Element> addCondition2 = getNameCondition("pict");
        final List<Element> picts = bfsFindElementsByCondition(rootElement, addCondition2);
        for (Element pict : picts) {
            System.out.println(pict);
        }


        final Map<String, Object> data = new HashMap<>();
        final List<Map<String, String>> students = new ArrayList<>();
        data.put("studentList", students);
        students.add(new HashMap<String, String>() {
            {
                put("name", "name1");
                put("age", "11");
                put("gender", "男fdsghsd冬瓜豆腐回家");
                put("address", "上海1");
            }
        });
        students.add(new HashMap<String, String>() {
            {
                put("name", "name2");
                put("age", "12");
                put("gender", "男");
                put("address", "上海2");
            }
        });
        students.add(new HashMap<String, String>() {
            {
                put("name", "name3");
                put("age", "13");
                put("gender", "男");
                put("address", "上海3");
            }
        });

        OutputFormat format = OutputFormat.createPrettyPrint();//一种美观的文本内容打印格式
        XMLWriter writer = new MyXMLWriter(new FileWriter("xun.ftl"));

        writer.write(read);
        writer.flush();
//        printXML(rootElement,0);

        Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
        configuration.setDefaultEncoding("utf-8");
        configuration.setDirectoryForTemplateLoading(new File("D:\\javaproject\\bilibili"));
//        configuration.setClassLoaderForTemplateLoading(this.getClass().getClassLoader(),"static");
        final Template template = configuration.getTemplate("xun.ftl");
//        Template template = configuration.getTemplate("newWordtemplateFTL.ftl", "utf-8");
        template.process(data, new FileWriter("aaaaa.docx"));

//        XWPFDocument document = new XWPFDocument(new FileInputStream("aaaaa.docx"));
//        PdfConverter.getInstance().convert(document, new FileOutputStream("aaaaa.pdf"), null);

        long old = System.currentTimeMillis();
//        FontSettings
//                .setFontsFolder(
//                        fileConfig.getFileBasePath() + fileConfig.getFontsPath() + File.separator, true);
        InputStream is = Test20220304.class.getClassLoader().getResourceAsStream("license.xml");
        License aposeLic = new License();
        aposeLic.setLicense(is);
        com.aspose.words.Document doc = new com.aspose.words.Document("aaaaa.docx");
        doc.save("aaaaa.pdf", SaveFormat.PDF);
        long now = System.currentTimeMillis();
        // 转化用时
        log.info("共耗时：" + ((now - old) / 1000.0) + "秒");

        System.out.println(111);
    }


    public static class MyXMLWriter extends XMLWriter implements AutoCloseable{

        public MyXMLWriter(Writer writer) {
            super(writer);
        }

        @Override
        protected void writeElement(Element element) throws IOException {
            int size = element.nodeCount();
            final String name = element.getName();
            String qualifiedName = element.getQualifiedName();
            if (name != null && name.startsWith(LIST)) {
                writer.write("<");
                writer.write(name);
                if (size <= 0) {
                    writeEmptyElementClose(qualifiedName);
                } else {
                    writer.write(">");
                    writeElementContent(element);
                    writer.write("</");
                    writer.write(qualifiedName);
                    writer.write(">");
                }
            } else {
                super.writeElement(element);
            }
        }
    }

//    private static Function<Element,String> findAnnotationTagAndRemoveCondition(){
//
//    }

    private static Consumer<Element> removeByCondition(Predicate<Element> predicate) {
        return node -> {
            if (predicate.test(node)) {
                node.getParent().remove(node);
            }
        };
    }


    private static Predicate<Element> getNameCondition(String name) {
        return elem -> name.equals(elem.getName());
    }

    private static Function<Element, HasAnnotationTagResult> matchAnnotationTagAndRemove() {
        final Predicate<Element> nameCondition = getNameCondition("annotation");
        Function<Element, HasAnnotationTagResult> function = element -> {
            final HasAnnotationTagResult result = new HasAnnotationTagResult(false);
            if (nameCondition.test(element)) {
                final String value = element.attribute("type").getValue();
                final Element parent = element.getParent();
                parent.remove(element);
                result.has = true;
                if ("Word.Comment".equals(value)) {
                    final Element valAnnoTag = element.element("content").element("p").element("r").element("t");
                    result.value = valAnnoTag.getText();
                    parent.getParent().remove(parent);
                }
            }
            return result;
        };
        return elem -> dfsHasAnnotationTagResult(elem, function);
    }


    private static HasAnnotationTagResult dfsHasAnnotationTagResult(
            Element root, Function<Element, HasAnnotationTagResult> function) {
        return dfsHasAnnotationTagResult(root, function, Element::elements);
    }

    private static HasAnnotationTagResult dfsHasAnnotationTagResult(
            Element root, Function<Element, HasAnnotationTagResult> function, Function<Element, List<Element>> childFunction) {
        if (root == null) return new HasAnnotationTagResult(false);
        final HasAnnotationTagResult result = function.apply(root);
        if (result.has) return result;
        final List<Element> apply = childFunction.apply(root);
        return apply.stream().map(ele -> dfsHasAnnotationTagResult(ele, function)).reduce(result, (as, bs) -> {
            as.has |= bs.has;
            as.value = bs.value == null ? as.value : bs.value;
            return as;
        });
    }

    private static List<Element> bfsFindElementsByCondition(
            Element root, Predicate<Element> addCondition) {
        return bfsFindElementsByCondition(root, addCondition, addCondition, Element::elements);
    }

    private static List<Element> bfsFindElementsByCondition(
            Element root, Predicate<Element> addCondition
            , Predicate<Element> ignoreCondition, Function<Element, List<Element>> childFunction) {
        final List<Element> list = new ArrayList<>();
        if (root == null) return list;
        final Queue<Element> queue = new LinkedList<>(root.elements());
        while (!queue.isEmpty()) {
            final Element poll = queue.poll();
            if (addCondition.test(poll)) {
                list.add(poll);
            }
            if (!ignoreCondition.test(poll)) {
                queue.addAll(childFunction.apply(poll));
            }
        }
        return list;
    }

    static class HasAnnotationTagResult {
        boolean has;
        String value;

        public HasAnnotationTagResult(boolean has) {
            this.has = has;
        }

        public HasAnnotationTagResult(boolean has, String value) {
            this.has = has;
            this.value = value;
        }

        @Override
        public String toString() {
            return "HasAnnotationTagResult{" +
                    "has=" + has +
                    ", value='" + value + '\'' +
                    '}';
        }
    }

    /**
     * <p>Title:printXML</p>
     * <p>Description:打印xml</p>
     *
     * @param elm    需要打印的xml
     * @param indent 首次缩进倍数
     */
    public static void printXML(Element elm, int indent) {
        //缩进字符串
        String indentStr = "    ";
        //打印缩进
        for (int i = 0; i < indent; i++) {
            System.out.print(indentStr);
        }
        //得到该节点子节点的集合
        List nodes = elm.elements();
        //获得其所有属性
        List attrs = elm.attributes();
        StringBuffer attrStr = new StringBuffer("");
        if (attrs.size() > 0) {
            for (Iterator it = attrs.iterator(); it.hasNext(); ) {
                Attribute attr = (Attribute) it.next();
                attrStr.append(" " + attr.getName() + "=\"" + attr.getText() + "\"");
            }
        }
        //如果有子节点则换行,没有子节点不换行
        String enterStr = (nodes.size() <= 0) ? "" : "\n";
        //得到标签的内容
        String elmStr = elm.getText();
        //打印前标签
        System.out.print("<" + elm.getName() + attrStr + ">" + elmStr + enterStr);
        //遍历子节点集合 并递归这个子节点
        for (Iterator it = nodes.iterator(); it.hasNext(); ) {
            printXML((Element) it.next(), indent + 1);
        }
        //如果有孩子节点 后标签不缩进
        if (nodes.size() > 0) {
            for (int i = 0; i < indent; i++) {
                System.out.print(indentStr);
            }
        }
        //打印后标签
        System.out.println("</" + elm.getName() + ">");
    }
}
