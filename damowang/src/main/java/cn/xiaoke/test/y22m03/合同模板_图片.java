package cn.xiaoke.test.y22m03;

import cn.hutool.core.img.ImgUtil;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.awt.image.BufferedImage;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class 合同模板_图片 {

    static SAXReader saxReader = new SAXReader();

    public static void main(String[] args) {
        final InputStream resourceAsStream = ClassLoader.getSystemClassLoader().getResourceAsStream("test_1.xml");
        final Document document = loadDocument(resourceAsStream);
        final List<Element> pList = findElementByName(document.getRootElement(), "p");
        final List<Element> followUpPList = pList.stream().filter(getChildHaveCondition("annotation"))
                .collect(Collectors.toList());
        followUpPList.forEach(element -> {
            final List<Element> children = element.elements();
            final Predicate<Element> rCondition = getNameCondition("r");
            final Optional<Element> binData = children.stream().filter(rCondition.and(getChildHaveCondition("pict")))
                    .map(child -> child.element("pict").element("binData")).findAny();
            final Predicate<Element> anCondition = getNameCondition("annotation");
            children.stream().filter(anCondition).forEach(child -> child.getParent().remove(child));

            final Predicate<Element> childOtherCondition = rCondition.and(getChildOtherCondition(anCondition
                    .and(getAttributeTypeCondition("Word.Comment"))));
            final Optional<Element> haveAnR = children.stream().filter(childOtherCondition).findAny();
            haveAnR.ifPresent(ele -> {
                final Map<String, String> map = textToMap(ele.element("annotation").element("content").element("p").element("r").element("t").getText());
                if (map.get("type").equals("url")) {
                    final BufferedImage bufferedImage;
                    try {
                        final String path = map.get("path");
                        bufferedImage = ImgUtil.read(new URL(path));
                        final String base64 = ImgUtil.toBase64(bufferedImage, path.substring(path.lastIndexOf(".") + 1));
                        binData.ifPresent(pictEle -> {
                            pictEle.setText(base64);
                        });
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                }
                ele.getParent().remove(ele);
            });
        });
        try (
                FileWriter fileWriter = new FileWriter("test_2.docx");
                Test20220304.MyXMLWriter writer = new Test20220304.MyXMLWriter(fileWriter);
        ) {
            writer.write(document);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static Map<String, String> textToMap(String text) {
        final Map<String, String> map = new HashMap<>();
        final String[] kvs = text.split(",");
        for (String kvStr : kvs) {
            final String[] kv = kvStr.split("=");
            map.put(kv[0].trim(), kv[1].trim());
        }
        return map;
    }

    public static Document loadDocument(InputStream inputStream) {
        Document read = null;
        try {
            read = saxReader.read(inputStream);
        } catch (DocumentException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return read;
    }

    private static Predicate<Element> getNameCondition(String name) {
        return elem -> name.equals(elem.getName());
    }

    private static Predicate<Element> getAttributeTypeCondition(String attributeName) {
        return elem -> attributeName.equals(elem.attribute("type").getValue());
    }

    private static Predicate<Element> getChildHaveCondition(String name) {
        final Predicate<Element> childNameCondition = getNameCondition(name);
        return elem -> elem.elements().stream().anyMatch(childNameCondition);
    }

    private static Predicate<Element> getChildOtherCondition(Predicate<Element> predicate) {
        return elem -> elem.elements().stream().anyMatch(predicate);
    }

    public static List<Element> findElementByName(Element root, String name) {
        final Predicate<Element> nameCondition = getNameCondition(name);
        return bfsFindElementsByCondition(root, nameCondition, nameCondition, Element::elements);
    }

    private static List<Element> bfsFindElementsByCondition(
            Element root, Predicate<Element> addCondition
            , Predicate<Element> ignoreCondition, Function<Element, List<Element>> childFunction) {
        final List<Element> list = new ArrayList<>();
        if (root == null) return list;
        final Queue<Element> queue = new LinkedList<>(root.elements());
        while (!queue.isEmpty()) {
            final Element poll = queue.poll();
            if (addCondition.test(poll)) {
                list.add(poll);
            }
            if (!ignoreCondition.test(poll)) {
                queue.addAll(childFunction.apply(poll));
            }
        }
        return list;
    }
}
