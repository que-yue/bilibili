/*
 * 版权信息: © 聚均科技
 */
package cn.xiaoke.util;

import java.lang.annotation.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = Excel.List.class)

/**
 *
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/1/6 15:53
 */
public @interface Excel {

    String DEFAULT_GROUP = "default";

    /**
     * 导出中文名
     */
    String value() default "";

    /**
     * 顺序
     */
    int ord() default 0;

    /**
     * 分组
     */
    String group() default DEFAULT_GROUP;

    /**
     * 转换类
     */
    Class<? extends ExcelConvert> convert() default DefualtExcelConvert.class;

    @Target({ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @interface List {
        Excel[] value();
    }

    interface ExcelConvert<T, R> {
        /**
         * 输出出去的时候转换方法
         *
         * @param obj 需要转换的对象
         * @return java.lang.String
         * @author xiaokedamowang
         * @date 2022-01-07 09:54
         */
        String output(T obj);

        /**
         * 输入的时候转换方法
         *
         * @param obj 需要转换的对象
         * @return R
         * @author xiaokedamowang
         * @date 2022-01-07 09:54
         */
        R input(Object obj);
    }

    class DefualtExcelConvert implements ExcelConvert<Object, Object> {

        @Override
        public String output(Object obj) {
            if (obj == null) {
                return null;
            }
            return obj.toString();
        }

        @Override
        public Object input(Object obj) {
            return null;
        }
    }

    public class LocalDateTimeExcelConvert implements ExcelConvert<LocalDateTime, String> {
        private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        @Override
        public String output(LocalDateTime obj) {
            if (obj == null) {
                return null;
            }
            return formatter.format(obj);
        }

        @Override
        public String input(Object obj) {
            return null;
        }
    }

    public class OldDateExcelConvert implements ExcelConvert<Date, String> {
        @Override
        public String output(Date obj) {
            if (obj == null) {
                return null;
            }
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj);
        }

        @Override
        public String input(Object obj) {
            return null;
        }
    }
}
