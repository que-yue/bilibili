/*
 * 版权信息 : @ 聚均科技
 */
package cn.xiaoke.util.interc;

/**
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/1/18 16:30
 */
public interface Converter<T,D>{
    D converter(T t);
}
