package cn.xiaoke.damowang_boot_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DamowangBootTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DamowangBootTestApplication.class, args);
    }

}
