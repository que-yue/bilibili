/*
 * 版权信息 : @ 聚均科技
 */
package cn.xiaoke.damowang_boot_test.controller;


import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import cn.xiaoke.damowang_boot_test.service.ILuckUserService;
import cn.xiaoke.damowang_boot_test.util.auto_wrapper.AutoWhereUtil;
import cn.xiaoke.damowang_boot_test.util.auto_wrapper.Where;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/2/16 10:39
 */
@RestController
@RequestMapping("/lucky-user")
@RequiredArgsConstructor
public class LuckUserController {

    private final ILuckUserService luckUserService;

    @GetMapping("selectList")
    public Object list() throws Throwable {
        final TestDto testDto = new TestDto();

        testDto.setPhone("1767311");
        testDto.setNum(1).setIndex(123).setCreateEndTime(LocalDateTime.now())
                .setCreateStartTime(LocalDateTime.now().minusDays(1)).setValue("val")
                .setVips(Arrays.asList(1, 5, 6));


//        final LambdaQueryWrapper<LuckUser> wrapper = AutoWhereUtil.lambdaQueryByAnnotation(LuckUser.class, testDto);
        final QueryWrapper<LuckUser> wrapper = AutoWhereUtil.queryByField(LuckUser.class, testDto);
        final List<LuckUser> list1 = luckUserService.list(wrapper);
        return null;
    }


    @EqualsAndHashCode(callSuper = true)
    @Data
    @Accessors(chain = true)
    public static class TestDto extends TestDto2 {

        private String phone;
        private Integer current;
        private Integer pageSize;
    }

    @Data
    @Accessors(chain = true)
    public static class TestDto2 {

        @Where(column = "card",value = "like")
        private String value;

        private Integer index;

        private Integer num;

        private List<Integer> vips;

        private LocalDateTime createStartTime;

        private LocalDateTime createEndTime;

    }


}
