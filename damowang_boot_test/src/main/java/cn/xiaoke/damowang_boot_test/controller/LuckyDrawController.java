package cn.xiaoke.damowang_boot_test.controller;


import cn.xiaoke.damowang_boot_test.service.ILuckyDrawService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
@RestController
@RequestMapping("/lucky-draw")
@RequiredArgsConstructor
public class LuckyDrawController {

    private final ILuckyDrawService luckyDrawService;

    @GetMapping("/lucky")
    public Object lucky(Integer userId,Integer actId){
        return luckyDrawService.lucky(userId,actId);
    }

    @GetMapping("/lucky2")
    public Object lucky2(Integer userId,Integer actId){
        return luckyDrawService.lucky2(userId,actId);
    }
    @GetMapping("/lucky3")
    public Object lucky3(Integer userId,Integer actId){
        return luckyDrawService.lucky3(userId,actId);
    }


    public static void main(String[] args) {
        final List[] lists = new List[1];
        final List<A> list1 = new ArrayList<>();
        lists[0] = list1;
        final A a = new A();
        list1.add(a);
        final List<Map<String, B>> list2 = new ArrayList<>();
        final B b = new B();
        final Map<String, B> map = new HashMap<>();
        list2.add(map);
        map.put("B",b);
        a.setBs(list2);
        final List<C> list3 = new ArrayList<>();
        b.setCs(list3);
        final C c = new C();
        list3.add(c);
        c.setName("w s c");

        final String jsonStr = JSON.toJSONString(lists);
        System.out.println(jsonStr);
        final List list = JSON.parseObject(jsonStr, List.class);
        final List<List> as = JSON.parseArray(jsonStr, List.class);
        final List<List<A>> parseObject = JSON.parseObject(jsonStr, new TypeReference<List<List<A>>>() {
        });
//        List<List<A>> newList = list;
//        List<List<A>> newList = (List<List<A>>)(Object)as;
        List<List<A>> newList = parseObject;
        final List<A> as1 = newList.get(0);
        final A a1 = as1.get(0);
        System.out.println();
    }
    @Data
    static class A{
        List<Map<String,B>> bs;
    }
    @Data
    static class B{
        List<C> cs;
    }
    @Data
    static class C{
        String name;
    }
}

