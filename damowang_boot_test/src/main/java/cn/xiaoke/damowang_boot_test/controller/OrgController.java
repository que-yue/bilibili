package cn.xiaoke.damowang_boot_test.controller;


import cn.xiaoke.damowang_boot_test.entity.Org;
import cn.xiaoke.damowang_boot_test.service.IOrgService;
import cn.xiaoke.damowang_boot_test.service.impl.OrgServiceImpl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-25
 */
@RestController
@RequestMapping("/org")
@RequiredArgsConstructor
public class OrgController {

    private final IOrgService orgService;


    @GetMapping("test")
    public List<Org> test() throws JsonProcessingException {
        return orgService.listToTree();
    }


    @GetMapping("OptionalMap")
    public IPage<Bank> pageBank(Integer current, Integer pageSize) {
        final IPage<Bank> pageBank = getPageBank(1, 10);
        final List<OrgServiceImpl.Area> areas = orgService.listArea();
        final Map<Integer, OrgServiceImpl.Area> map = areas.stream().collect(Collectors.toMap(OrgServiceImpl.Area::getId, Function.identity()));

        final List<Bank> records = pageBank.getRecords();

        records.forEach(bank ->{
            Optional.ofNullable(map.get(bank.getAreaId())).map(area ->{
                bank.setAreaName(area.getName()).setCityId(area.getPid());
                return map.get(area.getPid());
            }).map(city ->{
                bank.setCityName(city.getName()).setProvincesId(city.getPid());
                return map.get(city.getPid());
            }).ifPresent(prov ->{
                bank.setProvincesName(prov.getName());
            });
        });
        return pageBank;
    }


    public static IPage<Bank> getPageBank(Integer current, Integer pageSize) {
        final Random random = new Random();
        return new Page<Bank>(current, pageSize).setRecords(new ArrayList<Bank>() {{
            add(new Bank().setId(random.nextInt(10000))
                    .setName("XX银行" + random.nextInt(100000)).setAreaId(6));
            add(new Bank().setId(random.nextInt(10000))
                    .setName("XX银行" + random.nextInt(100000)).setAreaId(8));
            add(new Bank().setId(random.nextInt(10000))
                    .setName("XX银行" + random.nextInt(100000)).setAreaId(5));
        }});
    }

    @Data
    @Accessors(chain = true)
    static class Tuple<A, B> {
        A a;
        B b;
    }

    @Data
    @Accessors(chain = true)
    public static class Bank {
        Integer id;
        String name;
        //区 ID
        Integer areaId;
        //区 名称
        String areaName;
        //市 Id
        Integer cityId;
        //市 名称
        String cityName;
        //省 名称
        Integer provincesId;
        //省 名称
        String provincesName;


    }


//    @GetMapping("OptionalMap2")
//    public IPage<Bank> pageBank2(Integer current, Integer pageSize) {
//        return null;
//    }
}

