package cn.xiaoke.damowang_boot_test.controller;


import cn.xiaoke.damowang_boot_test.fun.XKFunction;
import cn.xiaoke.damowang_boot_test.testInterface.PageInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.util.function.Function;

@RestController
@RequestMapping("/testContoller")
public class TestContoller {


    @GetMapping("test1")
    public Object test1( ){
        Function<Integer,Integer> f = (a)->{
            return a+1;
        };
        return f;
    }


    @GetMapping("test2")
    public Object test2( ){
        XKFunction<Integer,Integer> f = (a)->{
            return a+1;
        };
        return f;
    }


    @PostMapping("test3")
    public Object test3(@RequestBody XKFunction<Integer,Integer> f){
        System.out.println(f.apply(1));
        return f;
    }

    @PostMapping("test4")
    public Object test4(@RequestBody PageInfo pageInfo){
        System.out.println(JSON.toJSONString(pageInfo));
        return pageInfo;
    }

    @PostMapping("test5")
    public Object test5(@RequestBody MyJSONObject pageInfo){
        System.out.println(JSON.toJSONString(pageInfo));
        return pageInfo;
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    @JSONType(serialzeFeatures = {SerializerFeature.WriteClassName})
    public static class MyJSONObject extends PageInfo{
        private Integer aaa;
        private String bbb;
    }
}
