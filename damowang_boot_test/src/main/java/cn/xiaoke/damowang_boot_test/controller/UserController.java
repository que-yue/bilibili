package cn.xiaoke.damowang_boot_test.controller;


import cn.xiaoke.damowang_boot_test.entity.TestUser;
import cn.xiaoke.damowang_boot_test.mapper.TestUserMapper;
import cn.xiaoke.damowang_boot_test.service.ITestUserService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("user")
@Api(tags = "测试用户控制器")
@RequiredArgsConstructor
public class UserController {
    private final Random random = new Random();
    private final ITestUserService testUserService;



    @PostMapping("save")
    public Boolean save(@RequestBody Integer id){
        final TestUser testUser = new TestUser().setUserName("测试用户" + random.nextInt(1000000));
        if (id == 1){
            testUserService.save(testUser);
        }else{
            final TestUserMapper baseMapper = (TestUserMapper) testUserService.getBaseMapper();
            baseMapper.myInsert(testUser.setId(id),"asd");
        }
        return true;
    }

    @PostMapping("save2")
    public Boolean save2(@RequestBody Integer id){
        final TestUser testUser = new TestUser().setUserName("测试用户" + random.nextInt(1000000));
        if (id == 1){
            testUserService.save(testUser);
        }else{
            final TestUserMapper baseMapper = (TestUserMapper) testUserService.getBaseMapper();
            baseMapper.myInsert2();
        }
        return true;
    }
}
