package cn.xiaoke.damowang_boot_test.domain;

import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import cn.xiaoke.damowang_boot_test.entity.LuckyDraw;
import cn.xiaoke.damowang_boot_test.service.RedisService;

import java.time.LocalDate;

/**
 * 普通会员
 */
public class CommonVip extends LuckUser implements User {

    public static Integer getTag(){
        return 0;
    }


    @Override
    public ResultInfo action(Lucky luckyInstance) {
        return luckyInstance.goTo(this);
    }
}
