package cn.xiaoke.damowang_boot_test.domain;

import cn.xiaoke.damowang_boot_test.domain.anno.MyAnnotation;
import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import cn.xiaoke.damowang_boot_test.entity.LuckyDraw;
import org.reflections.Reflections;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;

public class Factory2 {


    public final static Map<Integer, Function<LuckUser,User>> userMap;
    public final static Map<Integer, Function<LuckyDraw,Lucky>> luckyMap;

    static {
        userMap = new ConcurrentHashMap<>();
        luckyMap = new ConcurrentHashMap<>();


        Reflections reflections = new Reflections("cn.xiaoke");


        final Set<Class<? extends User>> subTypesOf = reflections.getSubTypesOf(User.class);
        for (Class<? extends User> aClass : subTypesOf) {
            System.out.println(aClass);
            try {
                final Method getTag = aClass.getMethod("getTag");
                final Object invoke = getTag.invoke(null);
                System.out.println(invoke);
                userMap.put((Integer) invoke,(user ->{
                    try {
                        final User vip = aClass.newInstance();
                        BeanUtils.copyProperties(user,vip);
                        return vip;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(MyAnnotation.class);
        for (Class<?> aClass : typesAnnotatedWith) {
            System.out.println(aClass.getName());
            final MyAnnotation annotation = aClass.getAnnotation(MyAnnotation.class);
            System.out.println("annotation.value() = " + annotation.value());


            luckyMap.put(Integer.parseInt(annotation.value()),(lucky ->{
                try {
                    final Object o = aClass.newInstance();
                    BeanUtils.copyProperties(lucky,o);
                    return (Lucky)o;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }));
        }
        System.out.println("===========================");
        luckyMap.forEach((k,v)->{
            System.out.println("v.apply(new LuckyDraw()).getClass() = " + v.apply(new LuckyDraw()).getClass());
        });

    }

    public static void main(String[] args) {
        System.out.println(111);
    }


    public static User getUserInstance(LuckUser user){
        final User apply = userMap.get(user.getVipLevel()).apply(user);
        return apply;
    }

    public static Lucky getLuckyInstance(LuckyDraw luckyDraw){
        final Lucky apply = luckyMap.get(luckyDraw.getType()).apply(luckyDraw);
        return apply;
    }

}
