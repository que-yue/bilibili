package cn.xiaoke.damowang_boot_test.domain;

import cn.xiaoke.damowang_boot_test.domain.anno.MyAnnotation;
import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import cn.xiaoke.damowang_boot_test.entity.LuckyDraw;
import cn.xiaoke.damowang_boot_test.service.RedisService;

import java.time.LocalDate;

@MyAnnotation("2")
/**
 * 免费抽奖
 */
public class FreeLucky extends LuckyDraw implements Lucky {


    @Override
    public ResultInfo goTo(CommonVip commonVip) {
        final String key = commonVip.getId() + LocalDate.now().toString();
        final Integer num = RedisService.get(key);
        if (num != null && num >= 1){
            throw new RuntimeException("今天抽奖次数用光了");
        }
        RedisService.put(key,num == null ? 1 : num + 1);
        final String gift = gifts.get(random.nextInt(100));
        return new ResultInfo(gift,0);
    }

    @Override
    public ResultInfo goTo(GrodVip grodVip) {
        return null;
    }
}
