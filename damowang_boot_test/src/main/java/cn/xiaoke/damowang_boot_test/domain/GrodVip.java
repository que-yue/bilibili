package cn.xiaoke.damowang_boot_test.domain;

import cn.xiaoke.damowang_boot_test.domain.anno.MyAnnotation;
import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import cn.xiaoke.damowang_boot_test.entity.LuckUser;

/**
 * 黄金会员
 */
public class GrodVip extends LuckUser implements User {

    public static Integer getTag(){
        return 1;
    }

    @Override
    public ResultInfo action(Lucky luckyInstance) {
        return luckyInstance.goTo(this);
    }
}
