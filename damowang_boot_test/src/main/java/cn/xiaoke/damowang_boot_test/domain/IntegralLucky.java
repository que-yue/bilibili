package cn.xiaoke.damowang_boot_test.domain;

import cn.xiaoke.damowang_boot_test.domain.anno.MyAnnotation;
import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.entity.LuckyDraw;
@MyAnnotation("1")
/**
 * 积分抽奖
 */
public class IntegralLucky extends LuckyDraw implements Lucky {

    @Override
    public ResultInfo goTo(CommonVip commonVip) {
        if (commonVip.getIntegral() < 10){
            throw new RuntimeException("积分不够");
        }
        final String gift = gifts.get(random.nextInt(100));
        return new ResultInfo(gift,10);
    }

    @Override
    public ResultInfo goTo(GrodVip grodVip) {
        return null;
    }
}
