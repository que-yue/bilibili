package cn.xiaoke.damowang_boot_test.domain;

import cn.xiaoke.damowang_boot_test.domain.anno.MyAnnotation;
import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.entity.LuckyDraw;

import java.util.Optional;

@MyAnnotation("0")
/**
 * 订单抽奖
 */
public class OrderLucky extends LuckyDraw implements Lucky {

    @Override
    public ResultInfo goTo(CommonVip commonVip) {
        final Integer orderMoney = commonVip.getOrderMoney();
        if (orderMoney == null || orderMoney < 100) {
            return null;
        }
        final String gift = gifts.get(random.nextInt(100));
        return new ResultInfo(gift,0);
    }

    @Override
    public ResultInfo goTo(GrodVip grodVip) {
        return null;
    }
}
