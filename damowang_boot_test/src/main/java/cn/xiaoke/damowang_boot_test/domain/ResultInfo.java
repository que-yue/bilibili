package cn.xiaoke.damowang_boot_test.domain;

import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import lombok.Data;

@Data
public class ResultInfo {
    String prize;
    Integer deductIntegral;

    public ResultInfo(String prize, Integer deductIntegral) {
        this.prize = prize;
        this.deductIntegral = deductIntegral;
    }
}
