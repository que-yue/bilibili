package cn.xiaoke.damowang_boot_test.domain.interfac;

import cn.xiaoke.damowang_boot_test.domain.CommonVip;
import cn.xiaoke.damowang_boot_test.domain.DiamondVip;
import cn.xiaoke.damowang_boot_test.domain.GrodVip;
import cn.xiaoke.damowang_boot_test.domain.ResultInfo;

import java.util.*;

public interface Lucky {

    Random random = new Random();
    List<String> gifts = new ArrayList<String>(){
        {
            for (int i = 0; i < 30; i++) {
                add("奖品"+ i);
            }
            for (int i = 0; i < 70; i++) {
                add(null);
            }
        }
    };

    ResultInfo goTo(CommonVip commonVip);

    ResultInfo goTo(GrodVip grodVip);
}
