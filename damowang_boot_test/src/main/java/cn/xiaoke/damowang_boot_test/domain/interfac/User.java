package cn.xiaoke.damowang_boot_test.domain.interfac;

import cn.xiaoke.damowang_boot_test.domain.FreeLucky;
import cn.xiaoke.damowang_boot_test.domain.IntegralLucky;
import cn.xiaoke.damowang_boot_test.domain.OrderLucky;
import cn.xiaoke.damowang_boot_test.domain.ResultInfo;

import java.util.Random;

public interface User {


    ResultInfo action(Lucky luckyInstance);
}
