package cn.xiaoke.damowang_boot_test.domain.org;

import cn.xiaoke.damowang_boot_test.domain.interfac.OrgNode;
import cn.xiaoke.damowang_boot_test.entity.Org;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.annotation.JSONType;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

//@JSONType(typeKey = "__class",typeName = "dpartment",serialzeFeatures = {
//        SerializerFeature.WriteClassName
//})
@JsonTypeName("department")
public class Department extends Org implements OrgNode {


    @Override
    @SuppressWarnings("all")
    public List<OrgNode> obtainChilds() {
        return (List)super.getChilds();
    }

    @Override
    @SuppressWarnings("all")
    public void installChilds(List<OrgNode> childs) {
        super.setChilds((List)childs);
    }

    @Override
//    @JSONField(serialize = false)
    @JsonIgnore
    public List<Integer> getSchoolIds() {
        final List<Integer> idList = new ArrayList<>();
        final List<OrgNode> childs = obtainChilds();
        if (childs != null) {
            for (OrgNode child : childs) {
                idList.addAll(child.getSchoolIds());
            }
        }
        return idList;
    }
}
