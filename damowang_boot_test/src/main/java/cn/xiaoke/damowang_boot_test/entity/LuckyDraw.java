package cn.xiaoke.damowang_boot_test.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="LuckyDraw对象", description="")
public class LuckyDraw implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "类型 0:下单抽奖 1: 积分抽奖 2: 免费抽奖")
    private Integer type;

    @ApiModelProperty(value = "活动名称")
    private String name;

    @ApiModelProperty(value = "活动图片")
    private String img;

    @ApiModelProperty(value = "活动描述")
    private String descr;

    @ApiModelProperty(value = "每次抽奖扣多少积分")
    private Integer integralNum;

    @ApiModelProperty(value = "每天最多几次")
    private Integer everydayTimes;

    @ApiModelProperty(value = "最少满足多少钱订单才能中奖")
    private Integer money;



}
