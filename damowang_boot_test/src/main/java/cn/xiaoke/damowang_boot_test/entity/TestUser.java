package cn.xiaoke.damowang_boot_test.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TestUser {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String userName;
}
