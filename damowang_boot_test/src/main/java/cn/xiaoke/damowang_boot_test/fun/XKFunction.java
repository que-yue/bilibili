package cn.xiaoke.damowang_boot_test.fun;

import com.baomidou.mybatisplus.extension.api.R;

import java.io.Serializable;
import java.util.function.Function;

public interface XKFunction<T, R> extends Function<T, R> , Serializable {
}
