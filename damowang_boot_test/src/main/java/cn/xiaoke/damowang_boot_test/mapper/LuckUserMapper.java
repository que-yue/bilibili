package cn.xiaoke.damowang_boot_test.mapper;


import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
public interface LuckUserMapper extends BaseMapper<LuckUser> {

    User getVip(Integer id);
}
