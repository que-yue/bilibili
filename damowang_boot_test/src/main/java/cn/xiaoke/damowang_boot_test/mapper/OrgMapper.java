package cn.xiaoke.damowang_boot_test.mapper;


import cn.xiaoke.damowang_boot_test.domain.interfac.OrgNode;
import cn.xiaoke.damowang_boot_test.entity.Org;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-25
 */
public interface OrgMapper extends BaseMapper<Org> {

    List<OrgNode> getAll();
}
