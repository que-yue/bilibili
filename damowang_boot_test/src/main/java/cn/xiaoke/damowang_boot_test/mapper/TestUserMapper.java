package cn.xiaoke.damowang_boot_test.mapper;


import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import cn.xiaoke.damowang_boot_test.entity.TestUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
public interface TestUserMapper extends BaseMapper<TestUser> {

    void myInsert(@Param("user") TestUser user,@Param("str")String str);
    void myInsert2();
}
