package cn.xiaoke.damowang_boot_test.service;


import cn.xiaoke.damowang_boot_test.entity.LuckyDraw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
public interface ILuckyDrawService extends IService<LuckyDraw> {

    Object lucky(Integer userId,Integer actId);
    Object lucky2(Integer userId,Integer actId);
    Object lucky3(Integer userId,Integer actId);
}
