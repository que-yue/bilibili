package cn.xiaoke.damowang_boot_test.service;


import cn.xiaoke.damowang_boot_test.domain.interfac.OrgNode;
import cn.xiaoke.damowang_boot_test.entity.Org;
import cn.xiaoke.damowang_boot_test.service.impl.OrgServiceImpl;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-25
 */
public interface IOrgService extends IService<Org> {

    List<Org> listToTree() throws JsonProcessingException;

    List<OrgNode> getAllByMysql();

    List<OrgServiceImpl.Area> listArea();
    List<OrgServiceImpl.Area> listAreaByIds(List<Integer> ids);
}
