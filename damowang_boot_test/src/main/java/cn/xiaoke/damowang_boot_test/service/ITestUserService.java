package cn.xiaoke.damowang_boot_test.service;


import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import cn.xiaoke.damowang_boot_test.entity.TestUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
public interface ITestUserService extends IService<TestUser> {

}
