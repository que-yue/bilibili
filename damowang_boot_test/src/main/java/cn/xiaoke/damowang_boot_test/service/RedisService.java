package cn.xiaoke.damowang_boot_test.service;

import cn.xiaoke.damowang_boot_test.domain.interfac.OrgNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class RedisService {
    private static Map<String,String> strmap = new ConcurrentHashMap<>();
    private static Map<String,Integer> map = new ConcurrentHashMap<>();
    private static Map<String,Object> objmap = new ConcurrentHashMap<>();

    private IOrgService orgService;

    @Autowired
    public void setOrgService(IOrgService orgService) {
        this.orgService = orgService;
        final List<OrgNode> allByMysql = orgService.getAllByMysql();

        objmap.put("tree",allByMysql);
    }

    public List<OrgNode> getTree(){
        return (List<OrgNode>)objmap.get("tree");
    }

    public static Integer get(String key){
        return map.get(key);
    }
    public static void put(String key,Integer value){
        map.put(key, value);
    }

    public static void put(String key,String value){
        strmap.put(key, value);
    }

    public static String getStr(String key){
        return strmap.get(key);
    }




}
