package cn.xiaoke.damowang_boot_test.service.impl;


import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import cn.xiaoke.damowang_boot_test.mapper.LuckUserMapper;
import cn.xiaoke.damowang_boot_test.service.ILuckUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
@Service
public class LuckUserServiceImpl extends ServiceImpl<LuckUserMapper, LuckUser> implements ILuckUserService {


}
