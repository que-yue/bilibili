package cn.xiaoke.damowang_boot_test.service.impl;


import cn.xiaoke.damowang_boot_test.domain.*;
import cn.xiaoke.damowang_boot_test.domain.interfac.Lucky;
import cn.xiaoke.damowang_boot_test.domain.interfac.User;
import cn.xiaoke.damowang_boot_test.entity.LuckUser;
import cn.xiaoke.damowang_boot_test.entity.LuckyDraw;
import cn.xiaoke.damowang_boot_test.mapper.LuckyDrawMapper;
import cn.xiaoke.damowang_boot_test.service.ILuckyDrawService;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
@Service
@RequiredArgsConstructor
public class LuckyDrawServiceImpl extends ServiceImpl<LuckyDrawMapper, LuckyDraw> implements ILuckyDrawService {

    private final LuckUserServiceImpl luckUserService;

    @Override
    public Object lucky(Integer userId,Integer actId) {
        final LuckUser user = luckUserService.getById(userId);
        final LuckyDraw luckyDraw = this.getById(actId);

        final User userInstance = Factory.getUserInstance(user);
        final Lucky luckyInstance = Factory2.getLuckyInstance(luckyDraw);
        userInstance.action(luckyInstance);
        return null;
    }

    @Override
    public Object lucky2(Integer userId,Integer actId) {
        final User vip = luckUserService.getBaseMapper().getVip(userId);
        System.out.println("vip.getClass() = " + vip.getClass());
        final Lucky lucky = this.getBaseMapper().getLucky(actId);
        System.out.println("lucky.getClass() = " + lucky.getClass());

        ResultInfo result = vip.action(lucky);


        return null;
    }


    @Override
    public Object lucky3(Integer userId,Integer actId) {

        return null;
    }
}
