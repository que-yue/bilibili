package cn.xiaoke.damowang_boot_test.service.impl;


import cn.xiaoke.damowang_boot_test.domain.interfac.OrgNode;
import cn.xiaoke.damowang_boot_test.entity.Org;
import cn.xiaoke.damowang_boot_test.mapper.OrgMapper;
import cn.xiaoke.damowang_boot_test.service.IOrgService;
import cn.xiaoke.damowang_boot_test.service.RedisService;
import cn.xiaoke.util.ListToTreeUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-25
 */
@Service
public class OrgServiceImpl extends ServiceImpl<OrgMapper, Org> implements IOrgService {

    @Autowired
    private RedisService redisService;

    @Override
    public List<Org> listToTree() throws JsonProcessingException {
        final List<OrgNode> orgNodes = this.getBaseMapper().getAll();
        final List<OrgNode> orgNodeTree = ListToTreeUtil.listToTree(orgNodes, OrgNode::getId, OrgNode::getPid, OrgNode::obtainChilds, OrgNode::installChilds);

        final OrgNode root = orgNodeTree.get(0);

        List<Integer> ids = root.getAllChildIds();
        System.out.println(ids);
        List<Integer> schoolIds = root.getSchoolIds();
        System.out.println(schoolIds);


        System.out.println("==================");

        final String one = new ObjectMapper().writeValueAsString(root);

        final String str = new ObjectMapper().writerFor(new TypeReference<List<OrgNode>>() {
        }).writeValueAsString(orgNodeTree);
        System.out.println(str);


        final List<OrgNode> list = new ObjectMapper().readValue(str, new TypeReference<List<OrgNode>>() {
        });

        final OrgNode oneNode = new ObjectMapper().readValue(one, OrgNode.class);

//        final String treeJson = JSON.toJSONString(orgNodeTree);
//        System.out.println(treeJson);
//
//
//        final LuckUser loginUser = UserUtil.getLoginUser();
//        final Integer orgId = loginUser.getOrgId();
//        final OrgNode node = getOrgNodeByOrgId(orgId);
//
//        final List<Integer> list = node.getSchoolIds();
//
//        final List<Org> orgList = this.listByIds(list);
//        System.out.println("=====================");
//        System.out.println(JSON.toJSONString(orgList));
//        return orgList;

        return null;
    }


    public OrgNode getOrgNodeByOrgId(Integer orgId){
        final List<OrgNode> tree = redisService.getTree();
        return OrgNode.dfsFindByIds(orgId,tree);
    }



    public List<OrgNode> getAllByMysql(){
        final List<OrgNode> orgNodes = this.getBaseMapper().getAll();
        final List<OrgNode> orgNodeTree = ListToTreeUtil.listToTree(orgNodes, OrgNode::getId, OrgNode::getPid, OrgNode::obtainChilds, OrgNode::installChilds);
        return orgNodeTree;
    }

    public List<Area> areaList = new ArrayList<Area>(){{
        add(new Area().setId(1).setName("湖北省"));
            add(new Area().setId(2).setPid(1).setName("武汉市"));
                add(new Area().setId(5).setPid(2).setName("江岸区"));
                add(new Area().setId(6).setPid(2).setName("江汉区"));
                add(new Area().setId(7).setPid(2).setName("青山区"));
            add(new Area().setId(3).setPid(1).setName("十堰市"));
            add(new Area().setId(4).setPid(11).setName("荆州市"));
                add(new Area().setId(8).setPid(4).setName("沙市区"));
                add(new Area().setId(9).setPid(4).setName("荆州区"));
    }};

    @Override
    public List<Area> listArea() {
        return areaList;
    }

    @Override
    public List<Area> listAreaByIds(List<Integer> ids) {
        return areaList;
    }
    @Data
    @Accessors(chain = true)
    public static class Area{
        Integer id;
        Integer pid;
        String name;
    }
}
