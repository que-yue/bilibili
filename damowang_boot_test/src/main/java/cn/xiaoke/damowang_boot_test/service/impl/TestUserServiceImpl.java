package cn.xiaoke.damowang_boot_test.service.impl;


import cn.xiaoke.damowang_boot_test.entity.TestUser;
import cn.xiaoke.damowang_boot_test.mapper.TestUserMapper;
import cn.xiaoke.damowang_boot_test.service.ILuckUserService;
import cn.xiaoke.damowang_boot_test.service.ITestUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xiaokedamowang
 * @since 2021-12-24
 */
@Service
public class TestUserServiceImpl extends ServiceImpl<TestUserMapper, TestUser> implements ITestUserService {


}
