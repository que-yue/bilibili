package cn.xiaoke.damowang_boot_test.test;


import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.cglib.proxy.Callback;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.function.Function;

public class Test220115 {

    public static void main(String[] args) {

        final Student student = new Student();

        final Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(Student.class);
        enhancer.setCallback(new A(student));


        final Student obj = (Student) enhancer.create();
        obj.sout();
        final String name = obj.getName2();
        System.out.println("name = " + name);
        System.out.println("===");

    }


    static class A implements MethodInterceptor{
        private Student student;
        public A(Student student){
            this.student = student;
        }

        @Override
        public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
            if (method.getName().equals("getName2")) {
                return method.invoke(student, objects);
            }else if (method.getName().equals("apply")){
                return "asdasfdsgfdgdf:" + objects[0];
            }
            System.out.println("方法名:"+method.getName());
            System.out.println("参数:" + JSON.toJSONString(objects));
            final Object invoke = method.invoke(student, objects);
            if (invoke != null){
                System.out.println("返回值:" + JSON.toJSONString(invoke));
            }
            return invoke;
        }
    }



    @Data
    @Accessors(chain = true)
    static class Student {
        String name;

        public void sout() {
            System.out.println("我是studen的sout方法");
        }

        public String getName2() {
            System.out.println("我是studen的getName2方法");
            return name;
        }

    }

}
