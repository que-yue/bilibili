package cn.xiaoke.damowang_boot_test.util.auto_wrapper;

import java.lang.annotation.*;

/**
 * @author xiaokedamowang
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Wheres {
    String group() default "";
    Where[] value() default {};
    boolean outerJoin() default false;
    boolean innerJoin() default false;
}
