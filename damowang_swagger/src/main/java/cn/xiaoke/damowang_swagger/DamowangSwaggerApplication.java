package cn.xiaoke.damowang_swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DamowangSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DamowangSwaggerApplication.class, args);
    }

}
