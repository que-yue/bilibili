package cn.xiaoke.damowang_swagger.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
@MapperScan("cn.xiaoke.damowang_swagger.dao")
public class DataSourceAndMybatisConfig {
    @Bean
    public DataSource dataSource(Environment env) {
        DruidDataSource ds = new DruidDataSource();
        ds.setUrl(env.getProperty("spring.datasource.url"));
        ds.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        ds.setPassword(env.getProperty("spring.datasource.password"));
        ds.setUsername(env.getProperty("spring.datasource.username"));
        ds.setInitialSize(10);//初始化时建立物理连接的个数
        ds.setMaxActive(300);//最大连接池数量
        ds.setMinIdle(10);//最小连接池数量
        ds.setMaxWait(60000);//获取连接时最大等待时间，单位毫秒。
        ds.setValidationQuery("SELECT 1");//用来检测连接是否有效的sql
        ds.setTestOnBorrow(false);//申请连接时执行validationQuery检测连接是否有效
        ds.setTestWhileIdle(true);//建议配置为true，不影响性能，并且保证安全性。
        ds.setPoolPreparedStatements(false);//是否缓存preparedStatement，也就是PSCache
        return ds;
    }

    //    @Bean
//    public PaginationInterceptor paginationInterceptor() {
//        return new PaginationInterceptor();
//    }
// 最新版
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.H2));
        return interceptor;
    }
}
