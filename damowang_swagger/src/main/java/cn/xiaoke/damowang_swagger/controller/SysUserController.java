package cn.xiaoke.damowang_swagger.controller;


import cn.xiaoke.damowang_swagger.dto.SysUserDTO;
import cn.xiaoke.damowang_swagger.other.ResponseData;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.sun.istack.internal.NotNull;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;
import javax.validation.groups.Default;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xiaokedamowang
 * @since 2020-11-08
 */
@RestController
@RequestMapping("/sysUser")
@Api(tags = "后台管理员账号")
@RequiredArgsConstructor
@Slf4j
public class SysUserController {

    @ApiOperation(value = "获取用户的所有角色",notes = "获取用户的所有角色2")
    @GetMapping("/getUserRoles/{userId}")
    public ResponseData<List<SysUserDTO>> getUserRoles( @ApiParam(value = "用户ID",example = "123321" )@PathVariable Integer userId){
        return null;
    }

    @ApiOperation(value = "禁用账号")
    @GetMapping("/enable")
    public ResponseData enable(@RequestParam @ApiParam("id") String id) {
        return ResponseData.getSuccess();
    }

    @ApiOperation(value = "获取单个账号信息")
    @GetMapping("/{id}")
    @ApiImplicitParam(name = "id",value = "唯一ID")
    public ResponseData<SysUserDTO> getById(@PathVariable String id) {
        return null;
    }

    @ApiOperation(value = "通过ID和Name查找")
    @GetMapping("/getByIdAndName")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "唯一ID",example = "123321" ),
            @ApiImplicitParam(name = "name",value = "名字")
    })
    public ResponseData<List<SysUserDTO>> getByIdAndName(@Validated @Size(min = 1,groups = Default.class) @RequestParam String id, @RequestParam String name) {
        return null;
    }


    @ApiOperation(value = "新建管理员")
    @PostMapping("/save")
    public ResponseData save(@RequestBody SysUserDTO sysUserDTO) {
        return ResponseData.getSuccess();
    }

    @ApiOperation(value = "获取账号列表")
    @PostMapping("/list/{pageNo}/{pageSize}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo2",value = "第几页"),
            @ApiImplicitParam(name = "pageSize2",value = "每页大小")
    })
    public ResponseData<IPage<SysUserDTO>> getList(
            @ApiParam(value = "第几页2") @PathVariable Integer pageNo
            ,@ApiParam(value = "每页大小2") @PathVariable Integer pageSize
    ) {
        return null;
    }


    @ApiOperation(value = "修改管理员")
    @PutMapping("/update")
    public ResponseData<?> update(@ApiParam(value = "管理员对象") @RequestBody SysUserDTO sysUserDTO) {
        return ResponseData.getSuccess();
    }



    @ApiOperation(value = "删除账号")
    @DeleteMapping("/{id}")
    public ResponseData delete(@PathVariable String id) {
        return ResponseData.getSuccess();
    }


    @ApiOperation(value = "给用户添加角色")
    @PostMapping("addRole")
    public ResponseData addRole(@ApiParam("角色ID数组")@RequestBody List<SysUserDTO> roles){
        return ResponseData.getSuccess();
    }
    @ApiOperation(value = "上传文件")
    @PostMapping("/restUpload")
    public ResponseData<?> singleFileUpload(@RequestParam("file") MultipartFile file) throws Exception {
        return null;
    }


//    String pretty = JSON.toJSONString(object, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
//            SerializerFeature.WriteDateUseDateFormat)


}

