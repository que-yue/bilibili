package cn.xiaoke.damowang_swagger.dto;


import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@ApiModel(value="SysUserDTO对象", description="撒地方单身狗")
public class SysUserDTO {
    private static final long serialVersionUID = 1L;

    private Integer id;

    @ApiModelProperty(value = "用户类型 0:管理员 1:公文学校人员 2:公文办公室人员 \r\n3:公文办公室主任 4:办公室领导",example ="asda")
    private Integer userType;

    @ApiModelProperty(value = "用户名",example = "asd啊啊啊啊")
    private String username;

    @ApiModelProperty(value = "职位 position",example ="asda2")
    @NotNull
    private Integer position;

    @JSONField(serialize = false)
    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "组织架构ID")
    private Integer organizationId;

    @ApiModelProperty(value = "单位名称")
    private String organizationName;

    @ApiModelProperty(value = "身份证")
    private String card;

    @ApiModelProperty(value = "邮箱")
    private String emall;

    @ApiModelProperty(value = "描述")
    private String descr;

    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否启用 0:否 1:是")
    private Boolean enable;

    private int[] authoritys = new int[32];
}
