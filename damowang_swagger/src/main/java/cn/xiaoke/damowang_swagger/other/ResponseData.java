package cn.xiaoke.damowang_swagger.other;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ResponseData<T> {
    private Integer code;
    private T data;
    private String msg;

    public static<T> ResponseData<T> getError(){
        return new ResponseData<T>().setCode(500);
    }
    public static<T> ResponseData<T> getError(String msg){
        return ResponseData.<T>getError().setMsg(msg);
    }
    public static<T> ResponseData<T> getError(Exception ex){
        return ResponseData.<T>getError().setMsg(ex.getMessage());
    }
    public static ResponseData<String> getSuccess(){
        return new ResponseData<String>().setCode(200).setMsg("请求成功");
    }
    public static<T> ResponseData<T> getSuccess(T obj){
        return new ResponseData<T>().setCode(200).setMsg("请求成功").setData(obj);
    }
    public static ResponseDataFactory getFactory(){
        return ResponseDataFactory.factory;
    }


    public static class ResponseDataFactory{
        private static final  ResponseDataFactory factory = new ResponseDataFactory();
        private static final String MSG400 = "账号或密码错误";
        private static final String MSG401 = "请登录";
        private static final String MSG402 = "登录失效";
        private static final String MSG403 = "未授权";
        private final  ResponseData<?> res400;
        private final  ResponseData<?> res401;
        private final  ResponseData<?> res402;
        private final  ResponseData<?> res403;
        private ResponseDataFactory(){
            res400 = new ResponseData<String>().setCode(400).setMsg(MSG400);
            res401 = new ResponseData<String>().setCode(401).setMsg(MSG401);
            res402 = new ResponseData<String>().setCode(402).setMsg(MSG402);
            res403 = new ResponseData<String>().setCode(403).setMsg(MSG403);
        }
        public ResponseData<?> get400(){
            return res400;
        }
        public ResponseData<?> get401(){
            return res401;
        }
        public ResponseData<?> get402(){
            return res402;
        }
        public ResponseData<?> get403(){
            return res403;
        }
    }
}
