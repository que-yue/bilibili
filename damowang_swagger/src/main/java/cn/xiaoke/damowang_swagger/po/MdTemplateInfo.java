package cn.xiaoke.damowang_swagger.po;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class MdTemplateInfo {
    String controllerTitle;
    String controllerDesc;
    String path;
    String method;
    String functionName;
    String functionDesc;
    String contentType;
    List<MdTemplateParamBlock> paramBlocks;
    List<MdTemplateResultBlock> resultBlocks;
    String requestTemplate;
    String responseTemplate;

    String desc;
    String joinTable;
    String joinModule;

    @Data
    @Accessors(chain = true)
    public static class MdTemplateParamBlock{
        String title;
        List<MdTemplateParam> params;
    }

    @Data
    @Accessors(chain = true)
    public static class MdTemplateResultBlock{
        String title;
        List<MdTemplateResult> params;
    }


    @Data
    @Accessors(chain = true)
    public static class MdTemplateParam {
        String cnName;
        String enName;
        String paramType;
        String dataType;
        String required;
        String remarks;
        String example;
    }

    @Data
    @Accessors(chain = true)
    public static class MdTemplateResult {
        String cnName;
        String enName;
        String dataType;
        String remarks;
        String example;
    }
}
