package cn.xiaoke.damowang_swagger.po;


import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author xiaokedamowang
 * @since 2020-11-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SysUser对象", description="")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;
    @ApiModelProperty(value = "用户类型 0:管理员 1:公文学校人员 2:公文办公室人员 3:公文办公室主任 4:办公室领导")
    private Integer userType;

    @ApiModelProperty(value = "职位 position")
    private Integer position;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "组织架构ID")
    private Integer organizationId;

    @ApiModelProperty(value = "身份证")
    private String card;

    @ApiModelProperty(value = "邮箱")
    private String emall;

    @ApiModelProperty(value = "描述")
    private String descr;

    @ApiModelProperty(value = "手机")
    private String phone;

    @ApiModelProperty(value = "是否删除 0:否 1:是")
    @TableLogic
    private Boolean delFlag;

    @ApiModelProperty(value = "是否启用 0:否 1:是")
    private Boolean enable;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;


}
