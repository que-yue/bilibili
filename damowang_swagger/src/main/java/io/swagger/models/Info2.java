package io.swagger.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.Data;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
public class Info2 {
    /**
     * 描述
     */
    private String description;
    /**
     * 版本号
     */
    private String version;
    /**
     * 标题
     */
    private String title;
    private String termsOfService;
    /**
     * 联系
     */
    private Contact contact;
    private License license;
    private Map<String, Object> vendorExtensions = new LinkedHashMap();

    public Info2() {
    }

    public Info2 version(String version) {
        this.setVersion(version);
        return this;
    }

    public Info2 title(String title) {
        this.setTitle(title);
        return this;
    }

    public Info2 description(String description) {
        this.setDescription(description);
        return this;
    }

    public Info2 termsOfService(String termsOfService) {
        this.setTermsOfService(termsOfService);
        return this;
    }

    public Info2 contact(Contact contact) {
        this.setContact(contact);
        return this;
    }

    public Info2 license(License license) {
        this.setLicense(license);
        return this;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTermsOfService() {
        return this.termsOfService;
    }

    public void setTermsOfService(String termsOfService) {
        this.termsOfService = termsOfService;
    }

    public Contact getContact() {
        return this.contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public License getLicense() {
        return this.license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    public Info2 mergeWith(Info2 info) {
        if (info != null) {
            if (this.description == null) {
                this.description = info.description;
            }

            if (this.version == null) {
                this.version = info.version;
            }

            if (this.title == null) {
                this.title = info.title;
            }

            if (this.termsOfService == null) {
                this.termsOfService = info.termsOfService;
            }

            if (this.contact == null) {
                this.contact = info.contact;
            }

            if (this.license == null) {
                this.license = info.license;
            }

            if (this.vendorExtensions == null) {
                this.vendorExtensions = info.vendorExtensions;
            }
        }

        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getVendorExtensions() {
        return this.vendorExtensions;
    }

    @JsonAnySetter
    public void setVendorExtension(String name, Object value) {
        if (name.startsWith("x-")) {
            this.vendorExtensions.put(name, value);
        }

    }


}