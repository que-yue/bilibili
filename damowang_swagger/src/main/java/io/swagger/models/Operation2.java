package io.swagger.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import io.swagger.models.parameters.Parameter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Operation2 {
    private Map<String, Object> vendorExtensions = new LinkedHashMap();

    private List<String> tags;
    /**
     * 接口名称(摘要)
     */
    private String summary;
    /**
     * 详细描述
     */
    private String description;
    /**
     * 唯一ID
     */
    private String OperationId;

    private List<Scheme> schemes;
    /**
     * contentType
     */
    private List<String> consumes;

    private List<String> produces;
    /**
     * 参数
     * [
     *                     {
     *                         "name":"authorization", 参数名
     *                         "in":"header", 什么参数 path 、 query 、 body 、 header或form
     *                         "description":"令牌", 描述
     *                         "required":false, 是否必填
     *                         "type":"string"  参数类型
     *                     },
     *                     {
     *                         "in":"body",
     *                         "name":"roles",
     *                         "description":"角色ID数组",
     *                         "required":false,
     *                         "schema":{
     *                             "type":"array", 数组
     *                             "items":{ 引用
     *                                 "$ref":"#/definitions/SysUserDTO对象"
     *                             }
     *                         }
     *                     }
     * ]
     */
    private List<Parameter> parameters = new ArrayList();
    /**
     * 返回
     * {
     *                     "200":{
     *                         "description":"OK",
     *                         "schema":{
     *                             "$ref":"#/definitions/ResponseData"
     *                         }
     *                     }
     *  }
     */
    private Map<String, Response> responses;
    private List<Map<String, List<String>>> security;
    private ExternalDocs externalDocs;
    private Boolean deprecated;

    public Operation2() {
    }

    public Operation2 summary(String summary) {
        this.setSummary(summary);
        return this;
    }

    public Operation2 description(String description) {
        this.setDescription(description);
        return this;
    }

    public Operation2 Operation2Id(String OperationId) {
        this.setOperation2Id(OperationId);
        return this;
    }

    public Operation2 schemes(List<Scheme> schemes) {
        this.setSchemes(schemes);
        return this;
    }

    public Operation2 scheme(Scheme scheme) {
        this.addScheme(scheme);
        return this;
    }

    public Operation2 consumes(List<String> consumes) {
        this.setConsumes(consumes);
        return this;
    }

    public Operation2 consumes(String consumes) {
        this.addConsumes(consumes);
        return this;
    }

    public Operation2 produces(List<String> produces) {
        this.setProduces(produces);
        return this;
    }

    public Operation2 produces(String produces) {
        this.addProduces(produces);
        return this;
    }

    public Operation2 security(SecurityRequirement security) {
        this.addSecurity(security.getName(), security.getScopes());
        return this;
    }

    public Operation2 parameter(Parameter parameter) {
        this.addParameter(parameter);
        return this;
    }

    public Operation2 response(int key, Response response) {
        this.addResponse(String.valueOf(key), response);
        return this;
    }

    public Operation2 defaultResponse(Response response) {
        this.addResponse("default", response);
        return this;
    }

    public Operation2 tags(List<String> tags) {
        this.setTags(tags);
        return this;
    }

    public Operation2 tag(String tag) {
        this.addTag(tag);
        return this;
    }

    public Operation2 externalDocs(ExternalDocs externalDocs) {
        this.setExternalDocs(externalDocs);
        return this;
    }

    public Operation2 deprecated(Boolean deprecated) {
        this.setDeprecated(deprecated);
        return this;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void addTag(String tag) {
        if (this.tags == null) {
            this.tags = new ArrayList();
        }

        this.tags.add(tag);
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperationId() {
        return this.OperationId;
    }

    public void setOperation2Id(String OperationId) {
        this.OperationId = OperationId;
    }

    public List<Scheme> getSchemes() {
        return this.schemes;
    }

    public void setSchemes(List<Scheme> schemes) {
        this.schemes = schemes;
    }

    public void addScheme(Scheme scheme) {
        if (this.schemes == null) {
            this.schemes = new ArrayList();
        }

        if (!this.schemes.contains(scheme)) {
            this.schemes.add(scheme);
        }

    }

    public List<String> getConsumes() {
        return this.consumes;
    }

    public void setConsumes(List<String> consumes) {
        this.consumes = consumes;
    }

    public void addConsumes(String consumes) {
        if (this.consumes == null) {
            this.consumes = new ArrayList();
        }

        this.consumes.add(consumes);
    }

    public List<String> getProduces() {
        return this.produces;
    }

    public void setProduces(List<String> produces) {
        this.produces = produces;
    }

    public void addProduces(String produces) {
        if (this.produces == null) {
            this.produces = new ArrayList();
        }

        this.produces.add(produces);
    }

    public List<Parameter> getParameters() {
        return this.parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(Parameter parameter) {
        if (this.parameters == null) {
            this.parameters = new ArrayList();
        }

        this.parameters.add(parameter);
    }

    public Map<String, Response> getResponses() {
        return this.responses;
    }

    public void setResponses(Map<String, Response> responses) {
        this.responses = responses;
    }

    public void addResponse(String key, Response response) {
        if (this.responses == null) {
            this.responses = new LinkedHashMap();
        }

        this.responses.put(key, response);
    }

    public List<Map<String, List<String>>> getSecurity() {
        return this.security;
    }

    public void setSecurity(List<Map<String, List<String>>> security) {
        this.security = security;
    }

    public void addSecurity(String name, List<String> scopes) {
        if (this.security == null) {
            this.security = new ArrayList();
        }

        Map<String, List<String>> req = new LinkedHashMap();
        if (scopes == null) {
            scopes = new ArrayList();
        }

        req.put(name, scopes);
        this.security.add(req);
    }

    public ExternalDocs getExternalDocs() {
        return this.externalDocs;
    }

    public void setExternalDocs(ExternalDocs value) {
        this.externalDocs = value;
    }

    public Boolean isDeprecated() {
        return this.deprecated;
    }

    public void setDeprecated(Boolean value) {
        if (value != null && !value.equals(Boolean.FALSE)) {
            this.deprecated = value;
        } else {
            this.deprecated = null;
        }

    }

    @JsonAnyGetter
    public Map<String, Object> getVendorExtensions() {
        return this.vendorExtensions;
    }

    @JsonAnySetter
    public void setVendorExtension(String name, Object value) {
        if (name.startsWith("x-")) {
            this.vendorExtensions.put(name, value);
        }

    }

    public void setVendorExtensions(Map<String, Object> vendorExtensions) {
        this.vendorExtensions = vendorExtensions;
    }


    public Operation2 vendorExtensions(Map<String, Object> vendorExtensions) {
        this.vendorExtensions.putAll(vendorExtensions);
        return this;
    }

    public String toString() {
        return super.toString() + "[" + this.OperationId + "]";
    }
}
