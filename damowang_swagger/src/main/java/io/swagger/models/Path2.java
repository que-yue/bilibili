package io.swagger.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.models.parameters.Parameter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Path2 {
    private Map<String, Object> vendorExtensions = new LinkedHashMap();
    private Operation get;
    private Operation put;
    private Operation post;
    private Operation head;
    private Operation delete;
    private Operation patch;
    private Operation options;
    private List<Parameter> parameters;

    public Path2() {
    }

    public Path2 set(String method, Operation op) {
        if ("get".equals(method)) {
            return this.get(op);
        } else if ("put".equals(method)) {
            return this.put(op);
        } else if ("head".equals(method)) {
            return this.head(op);
        } else if ("post".equals(method)) {
            return this.post(op);
        } else if ("delete".equals(method)) {
            return this.delete(op);
        } else if ("patch".equals(method)) {
            return this.patch(op);
        } else {
            return "options".equals(method) ? this.options(op) : null;
        }
    }

    public Path2 get(Operation get) {
        this.get = get;
        return this;
    }

    public Path2 head(Operation head) {
        this.head = head;
        return this;
    }

    public Path2 put(Operation put) {
        this.put = put;
        return this;
    }

    public Path2 post(Operation post) {
        this.post = post;
        return this;
    }

    public Path2 delete(Operation delete) {
        this.delete = delete;
        return this;
    }

    public Path2 patch(Operation patch) {
        this.patch = patch;
        return this;
    }

    public Path2 options(Operation options) {
        this.options = options;
        return this;
    }

    public Operation getGet() {
        return this.get;
    }

    public void setGet(Operation get) {
        this.get = get;
    }

    public Operation getHead() {
        return this.head;
    }

    public void setHead(Operation head) {
        this.head = head;
    }

    public Operation getPut() {
        return this.put;
    }

    public void setPut(Operation put) {
        this.put = put;
    }

    public Operation getPost() {
        return this.post;
    }

    public void setPost(Operation post) {
        this.post = post;
    }

    public Operation getDelete() {
        return this.delete;
    }

    public void setDelete(Operation delete) {
        this.delete = delete;
    }

    public Operation getPatch() {
        return this.patch;
    }

    public void setPatch(Operation patch) {
        this.patch = patch;
    }

    public Operation getOptions() {
        return this.options;
    }

    public void setOptions(Operation options) {
        this.options = options;
    }

    @JsonIgnore
    public List<Operation> getOperations() {
        List<Operation> allOperations = new ArrayList();
        if (this.get != null) {
            allOperations.add(this.get);
        }

        if (this.put != null) {
            allOperations.add(this.put);
        }

        if (this.head != null) {
            allOperations.add(this.head);
        }

        if (this.post != null) {
            allOperations.add(this.post);
        }

        if (this.delete != null) {
            allOperations.add(this.delete);
        }

        if (this.patch != null) {
            allOperations.add(this.patch);
        }

        if (this.options != null) {
            allOperations.add(this.options);
        }

        return allOperations;
    }

    @JsonIgnore
    public Map<HttpMethod, Operation> getOperationMap() {
        Map<HttpMethod, Operation> result = new LinkedHashMap();
        if (this.get != null) {
            result.put(HttpMethod.GET, this.get);
        }

        if (this.put != null) {
            result.put(HttpMethod.PUT, this.put);
        }

        if (this.post != null) {
            result.put(HttpMethod.POST, this.post);
        }

        if (this.delete != null) {
            result.put(HttpMethod.DELETE, this.delete);
        }

        if (this.patch != null) {
            result.put(HttpMethod.PATCH, this.patch);
        }

        if (this.head != null) {
            result.put(HttpMethod.HEAD, this.head);
        }

        if (this.options != null) {
            result.put(HttpMethod.OPTIONS, this.options);
        }

        return result;
    }

    public List<Parameter> getParameters() {
        return this.parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public void addParameter(Parameter parameter) {
        if (this.parameters == null) {
            this.parameters = new ArrayList();
        }

        this.parameters.add(parameter);
    }

    @JsonIgnore
    public boolean isEmpty() {
        return this.get == null && this.put == null && this.head == null && this.post == null && this.delete == null && this.patch == null && this.options == null;
    }

    @JsonAnyGetter
    public Map<String, Object> getVendorExtensions() {
        return this.vendorExtensions;
    }

    @JsonAnySetter
    public void setVendorExtension(String name, Object value) {
        if (name.startsWith("x-")) {
            this.vendorExtensions.put(name, value);
        }

    }

    public void setVendorExtensions(Map<String, Object> vendorExtensions) {
        this.vendorExtensions = vendorExtensions;
    }


    }
