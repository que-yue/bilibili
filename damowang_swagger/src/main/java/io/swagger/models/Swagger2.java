//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package io.swagger.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.models.auth.SecuritySchemeDefinition;
import io.swagger.models.parameters.Parameter;
import lombok.Data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@Data
public class Swagger2 {
    /**
     * 版本号
     */
    protected String swagger = "2.0";
    /**
     * 项目信息
     *
     * {
     *         "description":"swaggerJSON分析",
     *         "version":"1.0",
     *         "title":"damowang_swagger文档",
     *         "contact":{
     *             "name":"xiaokedamowang",
     *             "url":"xiaokedamowang.cn",
     *             "email":"191853541@qq.com"
     *         }
     *     }
     *
     *
     */
    protected Info info;
    /**
     * 地址
     * localhost:8080
     */
    protected String host;
    /**
     * 项目前缀
     * /damowang_swagger
     */
    protected String basePath;

    /**
     * controller 描述
     *
     * [
     *         {
     *             "name":"swagger-api-json-controller",
     *             "description":"Swagger Api JSON Controller"
     *         },
     *         {
     *             "name":"后台管理员账号",
     *             "description":"Sys User Controller"
     *         }
     *     ],
     *
     *
     */
    protected List<Tag> tags;

    protected List<Scheme> schemes;
    protected List<String> consumes;
    protected List<String> produces;
    protected List<SecurityRequirement> security;

    /**
     * 接口信息
     *{
     *         "/sysUser/addRole":Object{...},
     *         "/sysUser/enable":Object{...},
     *         "/sysUser/getByIdAndName":Object{...},
     *         "/sysUser/getUserRoles/{userId}":Object{...},
     *         "/sysUser/list/{pageNo}/{pageSize}":Object{...},
     *         "/sysUser/save":Object{...},
     *         "/sysUser/update":Object{...},
     *         "/sysUser/{id}":Object{...}
     *     }
     *
     */
    protected Map<String, Path> paths;
    /**
     * 登录凭证
     * {
     *         "登录凭证":{
     *             "type":"apiKey",
     *             "name":"authorization",
     *             "in":"header"
     *         }
     *     }
     */
    protected Map<String, SecuritySchemeDefinition> securityDefinitions;
    /**
     * 对象定义
     * {
     *         "IPage«SysUserDTO对象»":Object{...},
     *         "ResponseData":Object{...},
     *         "ResponseData«IPage«SysUserDTO对象»»":Object{...},
     *         "ResponseData«List«SysUserDTO对象»»":Object{...},
     *         "ResponseData«SysUserDTO对象»":Object{...},
     *         "SysUserDTO对象":Object{...}
     *     }
     *
     *
     *       ----------↓
     *
     *   {
     *             "type":"object", 类型
     *             "description":"的说法都是个", 描述
     *             "properties":Object{...}, 属性
     *             "title":"ResponseData" 标题
     *         }
     *
     *      ----------↓
     *
     *      {
     *          "type" : "array", 类型
     *          "items": {
     *              "$ref":"#/definitions/SysUserDTO对象"
     *          },
     *          "description":"描述",
     *          "example": "示例"
     *          "format":"int32" 格式
     *      }
     */
    protected Map<String, Model> definitions;
    protected Map<String, Parameter> parameters;
    protected Map<String, Response> responses;
    protected ExternalDocs externalDocs;
    protected Map<String, Object> vendorExtensions;

    public Swagger2() {
    }

    public Swagger2 info(Info info) {
        this.setInfo(info);
        return this;
    }

    public Swagger2 host(String host) {
        this.setHost(host);
        return this;
    }

    public Swagger2 basePath(String basePath) {
        this.setBasePath(basePath);
        return this;
    }

    public Swagger2 externalDocs(ExternalDocs value) {
        this.setExternalDocs(value);
        return this;
    }

    public Swagger2 tags(List<Tag> tags) {
        this.setTags(tags);
        return this;
    }

    public Swagger2 tag(Tag tag) {
        this.addTag(tag);
        return this;
    }

    public Swagger2 schemes(List<Scheme> schemes) {
        this.setSchemes(schemes);
        return this;
    }

    public Swagger2 scheme(Scheme scheme) {
        this.addScheme(scheme);
        return this;
    }

    public Swagger2 consumes(List<String> consumes) {
        this.setConsumes(consumes);
        return this;
    }

    public Swagger2 consumes(String consumes) {
        this.addConsumes(consumes);
        return this;
    }

    public Swagger2 produces(List<String> produces) {
        this.setProduces(produces);
        return this;
    }

    public Swagger2 produces(String produces) {
        this.addProduces(produces);
        return this;
    }

    public Swagger2 paths(Map<String, Path> paths) {
        this.setPaths(paths);
        return this;
    }

    public Swagger2 path(String key, Path path) {
        if (this.paths == null) {
            this.paths = new LinkedHashMap();
        }

        this.paths.put(key, path);
        return this;
    }

    public Swagger2 responses(Map<String, Response> responses) {
        this.responses = responses;
        return this;
    }

    public Swagger2 response(String key, Response response) {
        if (this.responses == null) {
            this.responses = new LinkedHashMap();
        }

        this.responses.put(key, response);
        return this;
    }

    public Swagger2 parameter(String key, Parameter parameter) {
        this.addParameter(key, parameter);
        return this;
    }

    public Swagger2 securityDefinition(String name, SecuritySchemeDefinition securityDefinition) {
        this.addSecurityDefinition(name, securityDefinition);
        return this;
    }

    public Swagger2 model(String name, Model model) {
        this.addDefinition(name, model);
        return this;
    }

    public Swagger2 security(SecurityRequirement securityRequirement) {
        this.addSecurity(securityRequirement);
        return this;
    }

    public Swagger2 vendorExtension(String key, Object extension) {
        if (this.vendorExtensions == null) {
            this.vendorExtensions = new LinkedHashMap();
        }

        this.vendorExtensions.put(key, extension);
        return this;
    }

    public String getSwagger() {
        return this.swagger;
    }

    public void setSwagger(String swagger) {
        this.swagger = swagger;
    }

    public Info getInfo() {
        return this.info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBasePath() {
        return this.basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public List<Scheme> getSchemes() {
        return this.schemes;
    }

    public void setSchemes(List<Scheme> schemes) {
        this.schemes = schemes;
    }

    public void addScheme(Scheme scheme) {
        if (this.schemes == null) {
            this.schemes = new ArrayList();
        }

        if (!this.schemes.contains(scheme)) {
            this.schemes.add(scheme);
        }

    }

    public List<Tag> getTags() {
        return this.tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Tag getTag(String tagName) {
        Tag tag = null;
        if (this.tags != null && tagName != null) {
            Iterator i$ = this.tags.iterator();

            while(i$.hasNext()) {
                Tag existing = (Tag)i$.next();
                if (existing.getName().equals(tagName)) {
                    tag = existing;
                    break;
                }
            }
        }

        return tag;
    }

    public void addTag(Tag tag) {
        if (this.tags == null) {
            this.tags = new ArrayList();
        }

        if (tag != null && tag.getName() != null && this.getTag(tag.getName()) == null) {
            this.tags.add(tag);
        }

    }

    public List<String> getConsumes() {
        return this.consumes;
    }

    public void setConsumes(List<String> consumes) {
        this.consumes = consumes;
    }

    public void addConsumes(String consumes) {
        if (this.consumes == null) {
            this.consumes = new ArrayList();
        }

        if (!this.consumes.contains(consumes)) {
            this.consumes.add(consumes);
        }

    }

    public List<String> getProduces() {
        return this.produces;
    }

    public void setProduces(List<String> produces) {
        this.produces = produces;
    }

    public void addProduces(String produces) {
        if (this.produces == null) {
            this.produces = new ArrayList();
        }

        if (!this.produces.contains(produces)) {
            this.produces.add(produces);
        }

    }

    public Map<String, Path> getPaths() {
        return this.paths;
    }

    public void setPaths(Map<String, Path> paths) {
        this.paths = paths;
    }

    public Path getPath(String path) {
        return this.paths == null ? null : (Path)this.paths.get(path);
    }

    public Map<String, SecuritySchemeDefinition> getSecurityDefinitions() {
        return this.securityDefinitions;
    }

    public void setSecurityDefinitions(Map<String, SecuritySchemeDefinition> securityDefinitions) {
        this.securityDefinitions = securityDefinitions;
    }

    public void addSecurityDefinition(String name, SecuritySchemeDefinition securityDefinition) {
        if (this.securityDefinitions == null) {
            this.securityDefinitions = new LinkedHashMap();
        }

        this.securityDefinitions.put(name, securityDefinition);
    }

    /** @deprecated */
    @JsonIgnore
    @Deprecated
    public List<SecurityRequirement> getSecurityRequirement() {
        return this.security;
    }

    /** @deprecated */
    @JsonIgnore
    @Deprecated
    public void setSecurityRequirement(List<SecurityRequirement> securityRequirements) {
        this.security = securityRequirements;
    }

    /** @deprecated */
    @JsonIgnore
    @Deprecated
    public void addSecurityDefinition(SecurityRequirement securityRequirement) {
        this.addSecurity(securityRequirement);
    }

    public List<SecurityRequirement> getSecurity() {
        return this.security;
    }

    public void setSecurity(List<SecurityRequirement> securityRequirements) {
        this.security = securityRequirements;
    }

    public void addSecurity(SecurityRequirement securityRequirement) {
        if (this.security == null) {
            this.security = new ArrayList();
        }

        this.security.add(securityRequirement);
    }

    public Map<String, Model> getDefinitions() {
        return this.definitions;
    }

    public void setDefinitions(Map<String, Model> definitions) {
        this.definitions = definitions;
    }

    public void addDefinition(String key, Model model) {
        if (this.definitions == null) {
            this.definitions = new LinkedHashMap();
        }

        this.definitions.put(key, model);
    }

    public Map<String, Parameter> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, Parameter> parameters) {
        this.parameters = parameters;
    }

    public Parameter getParameter(String parameter) {
        return this.parameters == null ? null : (Parameter)this.parameters.get(parameter);
    }

    public void addParameter(String key, Parameter parameter) {
        if (this.parameters == null) {
            this.parameters = new LinkedHashMap();
        }

        this.parameters.put(key, parameter);
    }

    public Map<String, Response> getResponses() {
        return this.responses;
    }

    public void setResponses(Map<String, Response> responses) {
        this.responses = responses;
    }

    public ExternalDocs getExternalDocs() {
        return this.externalDocs;
    }

    public void setExternalDocs(ExternalDocs value) {
        this.externalDocs = value;
    }

    @JsonAnyGetter
    public Map<String, Object> getVendorExtensions() {
        return this.vendorExtensions;
    }

    @JsonAnySetter
    public void setVendorExtension(String name, Object value) {
        if (name.startsWith("x-")) {
            this.vendorExtension(name, value);
        }

    }


    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (this.getClass() != obj.getClass()) {
            return false;
        } else {
            Swagger2 other = (Swagger2)obj;
            if (this.basePath == null) {
                if (other.basePath != null) {
                    return false;
                }
            } else if (!this.basePath.equals(other.basePath)) {
                return false;
            }

            if (this.consumes == null) {
                if (other.consumes != null) {
                    return false;
                }
            } else if (!this.consumes.equals(other.consumes)) {
                return false;
            }

            if (this.definitions == null) {
                if (other.definitions != null) {
                    return false;
                }
            } else if (!this.definitions.equals(other.definitions)) {
                return false;
            }

            if (this.externalDocs == null) {
                if (other.externalDocs != null) {
                    return false;
                }
            } else if (!this.externalDocs.equals(other.externalDocs)) {
                return false;
            }

            if (this.host == null) {
                if (other.host != null) {
                    return false;
                }
            } else if (!this.host.equals(other.host)) {
                return false;
            }

            if (this.info == null) {
                if (other.info != null) {
                    return false;
                }
            } else if (!this.info.equals(other.info)) {
                return false;
            }

            if (this.parameters == null) {
                if (other.parameters != null) {
                    return false;
                }
            } else if (!this.parameters.equals(other.parameters)) {
                return false;
            }

            if (this.paths == null) {
                if (other.paths != null) {
                    return false;
                }
            } else if (!this.paths.equals(other.paths)) {
                return false;
            }

            if (this.produces == null) {
                if (other.produces != null) {
                    return false;
                }
            } else if (!this.produces.equals(other.produces)) {
                return false;
            }

            if (this.responses == null) {
                if (other.responses != null) {
                    return false;
                }
            } else if (!this.responses.equals(other.responses)) {
                return false;
            }

            if (this.schemes == null) {
                if (other.schemes != null) {
                    return false;
                }
            } else if (!this.schemes.equals(other.schemes)) {
                return false;
            }

            if (this.security == null) {
                if (other.security != null) {
                    return false;
                }
            } else if (!this.security.equals(other.security)) {
                return false;
            }

            if (this.securityDefinitions == null) {
                if (other.securityDefinitions != null) {
                    return false;
                }
            } else if (!this.securityDefinitions.equals(other.securityDefinitions)) {
                return false;
            }

            if (this.swagger == null) {
                if (other.swagger != null) {
                    return false;
                }
            } else if (!this.swagger.equals(other.swagger)) {
                return false;
            }

            if (this.tags == null) {
                if (other.tags != null) {
                    return false;
                }
            } else if (!this.tags.equals(other.tags)) {
                return false;
            }

            if (this.vendorExtensions == null) {
                if (other.vendorExtensions != null) {
                    return false;
                }
            } else if (!this.vendorExtensions.equals(other.vendorExtensions)) {
                return false;
            }

            return true;
        }
    }

    public Swagger2 vendorExtensions(Map<String, Object> vendorExtensions) {
        if (vendorExtensions == null) {
            return this;
        } else {
            if (this.vendorExtensions == null) {
                this.vendorExtensions = new LinkedHashMap();
            }

            this.vendorExtensions.putAll(vendorExtensions);
            return this;
        }
    }

    public void setVendorExtensions(Map<String, Object> vendorExtensions) {
        this.vendorExtensions = vendorExtensions;
    }
}
