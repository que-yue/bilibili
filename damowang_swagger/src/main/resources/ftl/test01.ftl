### **请求地址**
<#-- 请求path为/common/product/param, 请求方式为post -->
请求path为${info.path}, 请求方式为${info.method}

<#--

-   **请求**

| **中文名称** | **英文名称** | **字段类型** | **输出属性** | **备注** |
|----------|----------|----------|----------|--------|
| 产品code     |   productCode       | String   | 必输       |        |
| 参数key     |   paramCode       | String   | 必输       |   中银保信传:"ZYBX_AUTH"     |


-->
- **请求**

<#if info.paramBlocks??>
<#list info.paramBlocks as block>

<#if block.title??>
- **${block.title!}**
</#if>

|**中文名称**|**英文名称**|**传参方式**|**字段类型**|**输出属性**|**示例值**|
|----------|----------|----------|----------|--------|--------|
<#list block.params as info>
| ${info.cnName!} | ${info.enName!} | ${info.paramType!} | ${info.dataType!} | <#if>${info.required??}</#if> | ${info.example!} |
</#list>
</#list>
</#if>

<#if info.resultBlocks??>

#### **响应**
---

<#list info.resultBlocks as block>
<#if block.title??>

-   **${block.title}**

</#if>
|**中文名称**|**英文名称**|**字段类型**|**示例值**|
|----------|----------|--------------|----------|
<#list block.params as info>
| ${info.cnName!} | ${info.enName!} | ${info.dataType!} | ${info.example!} |
</#list>
</#list>
</#if>


-   **报文样例**

-   **处理流程**
<#--校验产品是否需要中银保信授权-->

-   **关联表**
<#--渠道产品校验配置表-->
<#--comm_product_param-->

-   **关联模块**
<#--platform_comm-service-->



