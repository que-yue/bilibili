package cn.xiaoke.damowang.lucky_audience;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableConfigurationProperties
public class LuckyAudienceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LuckyAudienceApplication.class, args);
    }

}
