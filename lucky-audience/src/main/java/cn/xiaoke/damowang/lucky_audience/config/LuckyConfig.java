package cn.xiaoke.damowang.lucky_audience.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "lucky-config")
@Component
@Data
public class LuckyConfig {

    @Value("${SESSDATA}")
    private String SESSDATA;

    private Boolean cache;

    private String memberPre;

    private String apiPre;

    private String archiveList;

    private String likeList;

    private String likeDetail;

    private String fansList;

    private String fansRanking;


}
