package cn.xiaoke.damowang.lucky_audience.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * swagger配置
 */
@Configuration
@EnableSwagger2
//@EnableKnife4j
//@Profile({"dev", "uat"})
public class SwaggerConfig {

    @Bean
    public Docket createRestApi(Environment environment) {
        // 添加请求参数，我们这里把token作为请求头部参数传入后端
        ParameterBuilder parameterBuilder = new ParameterBuilder();
        List<Parameter> parameters = new ArrayList<>();
        parameterBuilder.name("authorization").description("令牌")
                .modelRef(new ModelRef("string")).parameterType("header")
                .required(false).build();//header中的authorization参数非必填，传空也可以
        boolean flag = !environment.acceptsProfiles(Profiles.of("prod"));
//        boolean flag = true;
        parameters.add(parameterBuilder.build());
        return new Docket(DocumentationType.SWAGGER_2).pathMapping("/")
                .enable(flag)//如果是pro就不开启
                .apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.basePackage("cn.xiaoke.damowang.lucky_audience.controller"))
                .paths(PathSelectors.any()).build().globalOperationParameters(parameters)
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }

    private List<SecurityScheme> securitySchemes() {
        ArrayList list = new ArrayList();
        list.add(new ApiKey("登录凭证", "authorization", "header"));
        return list;
    }

    private List<SecurityContext> securityContexts() {
        List list = new ArrayList();
        list.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
//                        .forPaths(PathSelectors.regex("^(?!auth).*$"))
                        .build()
        );
        return list;
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List list = new ArrayList();
        list.add(new SecurityReference("登录凭证", authorizationScopes));
        return list;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("lucky-audience文档")
                .description("lucky-audience")
                .contact(new Contact("xiaokedamowang", "xiaokedamowang.cn", "191853541@qq.com"))
                .version("1.0")
                .build();
    }
}
