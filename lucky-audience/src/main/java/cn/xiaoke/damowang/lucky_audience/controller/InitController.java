package cn.xiaoke.damowang.lucky_audience.controller;

import cn.xiaoke.damowang.lucky_audience.config.LuckyConfig;
import cn.xiaoke.damowang.lucky_audience.entity.User;
import cn.xiaoke.damowang.lucky_audience.service.InitService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("init")
@RequiredArgsConstructor
public class InitController {

    private final LuckyConfig luckyConfig;
    private final InitService initService;


    @GetMapping("test")
    public String test(){
        return luckyConfig.getSESSDATA();
    }

    @GetMapping("getArchiveList")
    public Object getArchiveList(){
        return initService.getArchiveList();
    }

    @GetMapping("getLikeUserList")
    public List<User> getLikeUserList(){
        return initService.getLikeUserList();
    }

    @GetMapping("getFansList")
    public List<User> getFansList(){
        return initService.getFansList();
    }

    @GetMapping("getFansRankingList")
    public List<User> getFansRankingList(){
        return initService.getfansRankingList();
    }

    @GetMapping("init")
    public String init(){
        initService.init();
        return "OK";
    }
}
