package cn.xiaoke.damowang.lucky_audience.controller;

import cn.xiaoke.damowang.lucky_audience.service.LuckyService;
import cn.xiaoke.damowang.lucky_audience.service.domain.BiliBiliLuckyReservoir;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("lucky")
@RequiredArgsConstructor
public class LuckyController {

    private final LuckyService luckyService;


    @GetMapping("likeLucky")
    @ApiOperation("点赞抽奖")
    public List<BiliBiliLuckyReservoir.WinningUserInfo> likeLucky(){
        return luckyService.likeLucky();
    }

    @GetMapping("fansLucky")
    @ApiOperation("粉丝抽奖")
    public List<BiliBiliLuckyReservoir.WinningUserInfo> fansLucky(){
        return luckyService.fansLucky();
    }

}
