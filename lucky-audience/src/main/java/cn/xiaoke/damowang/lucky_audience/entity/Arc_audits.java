/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;
import java.util.List;

/**
 * Auto-generated: 2022-01-28 14:5:51
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Arc_audits {

    private Archive Archive;
    private int state_panel;
    private String parent_tname;
    private String typename;
    private int open_appeal;
    private String fast_pub;
    private int captions_count;
    private String activity;
    private String app_rules;
    private String hl;
    private String honors;
    private List<String> bs_editor;
    public void setArchive(Archive Archive) {
         this.Archive = Archive;
     }
     public Archive getArchive() {
         return Archive;
     }

    public void setState_panel(int state_panel) {
         this.state_panel = state_panel;
     }
     public int getState_panel() {
         return state_panel;
     }

    public void setParent_tname(String parent_tname) {
         this.parent_tname = parent_tname;
     }
     public String getParent_tname() {
         return parent_tname;
     }

    public void setTypename(String typename) {
         this.typename = typename;
     }
     public String getTypename() {
         return typename;
     }

    public void setOpen_appeal(int open_appeal) {
         this.open_appeal = open_appeal;
     }
     public int getOpen_appeal() {
         return open_appeal;
     }

    public void setFast_pub(String fast_pub) {
         this.fast_pub = fast_pub;
     }
     public String getFast_pub() {
         return fast_pub;
     }

    public void setCaptions_count(int captions_count) {
         this.captions_count = captions_count;
     }
     public int getCaptions_count() {
         return captions_count;
     }

    public void setActivity(String activity) {
         this.activity = activity;
     }
     public String getActivity() {
         return activity;
     }

    public void setApp_rules(String app_rules) {
         this.app_rules = app_rules;
     }
     public String getApp_rules() {
         return app_rules;
     }

    public void setHl(String hl) {
         this.hl = hl;
     }
     public String getHl() {
         return hl;
     }

    public void setHonors(String honors) {
         this.honors = honors;
     }
     public String getHonors() {
         return honors;
     }

    public void setBs_editor(List<String> bs_editor) {
         this.bs_editor = bs_editor;
     }
     public List<String> getBs_editor() {
         return bs_editor;
     }

}