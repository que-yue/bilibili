/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;

/**
 * Auto-generated: 2022-01-28 14:5:51
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Archive {

    private long aid;
    private String bvid;
    private long mid;
    private int tid;
    private String tp_info;
    private String title;
    private String author;
    private String cover;
    private String reject_reason;
    private String reject_reason_url;
    private String tag;
    private int duration;
    private int copyright;
    private int no_reprint;
    private int ugcpay;
    private int order_id;
    private String order_name;
    private int adorder_id;
    private String adorder_name;
    private String adorder_no;
    private int online_time;
    private String desc;
    private long mission_id;
    private String mission_name;
    private int forward;
    private int attribute;
    private int state;
    private String state_desc;
    private int state_panel;
    private String source;
    private int desc_format_id;
    private String porder;
    private String dynamic;
    private String poi_object;
    private int dtime;
    private long ptime;
    private long ctime;
    private String ugcpay_info;
    private String staffs;
    private String vote;
    private String activity;
    private int interactive;
    private String hl;
    private int no_background;
    private int dynamic_video;
    private int no_public;
    private int is_360;
    private int bs_editor;
    private int up_from;
    private String desc_v2;
    private String dynamic_v2;
    private boolean limit_state;
    private String appeal;
    public void setAid(long aid) {
         this.aid = aid;
     }
     public long getAid() {
         return aid;
     }

    public void setBvid(String bvid) {
         this.bvid = bvid;
     }
     public String getBvid() {
         return bvid;
     }

    public void setMid(long mid) {
         this.mid = mid;
     }
     public long getMid() {
         return mid;
     }

    public void setTid(int tid) {
         this.tid = tid;
     }
     public int getTid() {
         return tid;
     }

    public void setTp_info(String tp_info) {
         this.tp_info = tp_info;
     }
     public String getTp_info() {
         return tp_info;
     }

    public void setTitle(String title) {
         this.title = title;
     }
     public String getTitle() {
         return title;
     }

    public void setAuthor(String author) {
         this.author = author;
     }
     public String getAuthor() {
         return author;
     }

    public void setCover(String cover) {
         this.cover = cover;
     }
     public String getCover() {
         return cover;
     }

    public void setReject_reason(String reject_reason) {
         this.reject_reason = reject_reason;
     }
     public String getReject_reason() {
         return reject_reason;
     }

    public void setReject_reason_url(String reject_reason_url) {
         this.reject_reason_url = reject_reason_url;
     }
     public String getReject_reason_url() {
         return reject_reason_url;
     }

    public void setTag(String tag) {
         this.tag = tag;
     }
     public String getTag() {
         return tag;
     }

    public void setDuration(int duration) {
         this.duration = duration;
     }
     public int getDuration() {
         return duration;
     }

    public void setCopyright(int copyright) {
         this.copyright = copyright;
     }
     public int getCopyright() {
         return copyright;
     }

    public void setNo_reprint(int no_reprint) {
         this.no_reprint = no_reprint;
     }
     public int getNo_reprint() {
         return no_reprint;
     }

    public void setUgcpay(int ugcpay) {
         this.ugcpay = ugcpay;
     }
     public int getUgcpay() {
         return ugcpay;
     }

    public void setOrder_id(int order_id) {
         this.order_id = order_id;
     }
     public int getOrder_id() {
         return order_id;
     }

    public void setOrder_name(String order_name) {
         this.order_name = order_name;
     }
     public String getOrder_name() {
         return order_name;
     }

    public void setAdorder_id(int adorder_id) {
         this.adorder_id = adorder_id;
     }
     public int getAdorder_id() {
         return adorder_id;
     }

    public void setAdorder_name(String adorder_name) {
         this.adorder_name = adorder_name;
     }
     public String getAdorder_name() {
         return adorder_name;
     }

    public void setAdorder_no(String adorder_no) {
         this.adorder_no = adorder_no;
     }
     public String getAdorder_no() {
         return adorder_no;
     }

    public void setOnline_time(int online_time) {
         this.online_time = online_time;
     }
     public int getOnline_time() {
         return online_time;
     }

    public void setDesc(String desc) {
         this.desc = desc;
     }
     public String getDesc() {
         return desc;
     }

    public void setMission_id(long mission_id) {
         this.mission_id = mission_id;
     }
     public long getMission_id() {
         return mission_id;
     }

    public void setMission_name(String mission_name) {
         this.mission_name = mission_name;
     }
     public String getMission_name() {
         return mission_name;
     }

    public void setForward(int forward) {
         this.forward = forward;
     }
     public int getForward() {
         return forward;
     }

    public void setAttribute(int attribute) {
         this.attribute = attribute;
     }
     public int getAttribute() {
         return attribute;
     }

    public void setState(int state) {
         this.state = state;
     }
     public int getState() {
         return state;
     }

    public void setState_desc(String state_desc) {
         this.state_desc = state_desc;
     }
     public String getState_desc() {
         return state_desc;
     }

    public void setState_panel(int state_panel) {
         this.state_panel = state_panel;
     }
     public int getState_panel() {
         return state_panel;
     }

    public void setSource(String source) {
         this.source = source;
     }
     public String getSource() {
         return source;
     }

    public void setDesc_format_id(int desc_format_id) {
         this.desc_format_id = desc_format_id;
     }
     public int getDesc_format_id() {
         return desc_format_id;
     }

    public void setPorder(String porder) {
         this.porder = porder;
     }
     public String getPorder() {
         return porder;
     }

    public void setDynamic(String dynamic) {
         this.dynamic = dynamic;
     }
     public String getDynamic() {
         return dynamic;
     }

    public void setPoi_object(String poi_object) {
         this.poi_object = poi_object;
     }
     public String getPoi_object() {
         return poi_object;
     }

    public void setDtime(int dtime) {
         this.dtime = dtime;
     }
     public int getDtime() {
         return dtime;
     }

    public void setPtime(long ptime) {
         this.ptime = ptime;
     }
     public long getPtime() {
         return ptime;
     }

    public void setCtime(long ctime) {
         this.ctime = ctime;
     }
     public long getCtime() {
         return ctime;
     }

    public void setUgcpay_info(String ugcpay_info) {
         this.ugcpay_info = ugcpay_info;
     }
     public String getUgcpay_info() {
         return ugcpay_info;
     }

    public void setStaffs(String staffs) {
         this.staffs = staffs;
     }
     public String getStaffs() {
         return staffs;
     }

    public void setVote(String vote) {
         this.vote = vote;
     }
     public String getVote() {
         return vote;
     }

    public void setActivity(String activity) {
         this.activity = activity;
     }
     public String getActivity() {
         return activity;
     }

    public void setInteractive(int interactive) {
         this.interactive = interactive;
     }
     public int getInteractive() {
         return interactive;
     }

    public void setHl(String hl) {
         this.hl = hl;
     }
     public String getHl() {
         return hl;
     }

    public void setNo_background(int no_background) {
         this.no_background = no_background;
     }
     public int getNo_background() {
         return no_background;
     }

    public void setDynamic_video(int dynamic_video) {
         this.dynamic_video = dynamic_video;
     }
     public int getDynamic_video() {
         return dynamic_video;
     }

    public void setNo_public(int no_public) {
         this.no_public = no_public;
     }
     public int getNo_public() {
         return no_public;
     }

    public void setIs_360(int is_360) {
         this.is_360 = is_360;
     }
     public int getIs_360() {
         return is_360;
     }

    public void setBs_editor(int bs_editor) {
         this.bs_editor = bs_editor;
     }
     public int getBs_editor() {
         return bs_editor;
     }

    public void setUp_from(int up_from) {
         this.up_from = up_from;
     }
     public int getUp_from() {
         return up_from;
     }

    public void setDesc_v2(String desc_v2) {
         this.desc_v2 = desc_v2;
     }
     public String getDesc_v2() {
         return desc_v2;
     }

    public void setDynamic_v2(String dynamic_v2) {
         this.dynamic_v2 = dynamic_v2;
     }
     public String getDynamic_v2() {
         return dynamic_v2;
     }

    public void setLimit_state(boolean limit_state) {
         this.limit_state = limit_state;
     }
     public boolean getLimit_state() {
         return limit_state;
     }

    public void setAppeal(String appeal) {
         this.appeal = appeal;
     }
     public String getAppeal() {
         return appeal;
     }

}