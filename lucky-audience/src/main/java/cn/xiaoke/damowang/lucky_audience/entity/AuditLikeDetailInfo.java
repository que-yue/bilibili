/*
 * 版权信息 : @ 聚均科技
 */
package cn.xiaoke.damowang.lucky_audience.entity;


/**
 * @author xiaokedamowang
 * @version 1.0
 * @date 2022/1/28 17:30
 */

public class AuditLikeDetailInfo {

    private Long aid;

    private Long likeDetailId;

    public AuditLikeDetailInfo(Long aid, Long likeDetailId) {
        this.aid = aid;
        this.likeDetailId = likeDetailId;
    }

    public Long getAid() {
        return aid;
    }

    public void setAid(Long aid) {
        this.aid = aid;
    }

    public Long getLikeDetailId() {
        return likeDetailId;
    }

    public void setLikeDetailId(Long likeDetailId) {
        this.likeDetailId = likeDetailId;
    }
}
