/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;
import java.util.List;

/**
 * Auto-generated: 2022-01-28 14:5:51
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class BiliBiliArchiveData {

    private List<Arc_audits> arc_audits;
    private Page page;
    public void setArc_audits(List<Arc_audits> arc_audits) {
         this.arc_audits = arc_audits;
     }
     public List<Arc_audits> getArc_audits() {
         return arc_audits;
     }

    public void setPage(Page page) {
         this.page = page;
     }
     public Page getPage() {
         return page;
     }

}