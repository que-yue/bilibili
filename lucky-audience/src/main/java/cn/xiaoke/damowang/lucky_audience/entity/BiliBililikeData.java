/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;
import java.util.List;

/**
 * Auto-generated: 2022-01-28 15:42:59
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class BiliBililikeData {

    private Cursor cursor;
    private List<Items> items;
    public void setCursor(Cursor cursor) {
         this.cursor = cursor;
     }
     public Cursor getCursor() {
         return cursor;
     }

    public void setItems(List<Items> items) {
         this.items = items;
     }
     public List<Items> getItems() {
         return items;
     }

}