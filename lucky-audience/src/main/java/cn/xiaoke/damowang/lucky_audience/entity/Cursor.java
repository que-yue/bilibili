/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;

/**
 * Auto-generated: 2022-01-28 15:42:59
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Cursor {

    private boolean is_end;
    private long id;
    private long time;
    public void setIs_end(boolean is_end) {
         this.is_end = is_end;
     }
     public boolean getIs_end() {
         return is_end;
     }

    public void setId(long id) {
         this.id = id;
     }
     public long getId() {
         return id;
     }

    public void setTime(long time) {
         this.time = time;
     }
     public long getTime() {
         return time;
     }

}