/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;

/**
 * Auto-generated: 2022-01-28 15:42:59
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Item {

    private long item_id;
    private int pid;
    private String type;
    private String business;
    private int business_id;
    private int reply_business_id;
    private int like_business_id;
    private String title;
    private String desc;
    private String image;
    private String uri;
    private String detail_name;
    private String native_uri;
    private long ctime;
    public void setItem_id(long item_id) {
         this.item_id = item_id;
     }
     public long getItem_id() {
         return item_id;
     }

    public void setPid(int pid) {
         this.pid = pid;
     }
     public int getPid() {
         return pid;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setBusiness(String business) {
         this.business = business;
     }
     public String getBusiness() {
         return business;
     }

    public void setBusiness_id(int business_id) {
         this.business_id = business_id;
     }
     public int getBusiness_id() {
         return business_id;
     }

    public void setReply_business_id(int reply_business_id) {
         this.reply_business_id = reply_business_id;
     }
     public int getReply_business_id() {
         return reply_business_id;
     }

    public void setLike_business_id(int like_business_id) {
         this.like_business_id = like_business_id;
     }
     public int getLike_business_id() {
         return like_business_id;
     }

    public void setTitle(String title) {
         this.title = title;
     }
     public String getTitle() {
         return title;
     }

    public void setDesc(String desc) {
         this.desc = desc;
     }
     public String getDesc() {
         return desc;
     }

    public void setImage(String image) {
         this.image = image;
     }
     public String getImage() {
         return image;
     }

    public void setUri(String uri) {
         this.uri = uri;
     }
     public String getUri() {
         return uri;
     }

    public void setDetail_name(String detail_name) {
         this.detail_name = detail_name;
     }
     public String getDetail_name() {
         return detail_name;
     }

    public void setNative_uri(String native_uri) {
         this.native_uri = native_uri;
     }
     public String getNative_uri() {
         return native_uri;
     }

    public void setCtime(long ctime) {
         this.ctime = ctime;
     }
     public long getCtime() {
         return ctime;
     }

}