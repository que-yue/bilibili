/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;
import java.util.List;

/**
 * Auto-generated: 2022-01-28 15:42:59
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Items {

    private long id;
    private List<User> users;
    private Item item;
    private int counts;
    private long like_time;
    private int notice_state;
    public void setId(long id) {
         this.id = id;
     }
     public long getId() {
         return id;
     }

    public void setUsers(List<User> users) {
         this.users = users;
     }
     public List<User> getUsers() {
         return users;
     }

    public void setItem(Item item) {
         this.item = item;
     }
     public Item getItem() {
         return item;
     }

    public void setCounts(int counts) {
         this.counts = counts;
     }
     public int getCounts() {
         return counts;
     }

    public void setLike_time(long like_time) {
         this.like_time = like_time;
     }
     public long getLike_time() {
         return like_time;
     }

    public void setNotice_state(int notice_state) {
         this.notice_state = notice_state;
     }
     public int getNotice_state() {
         return notice_state;
     }

}