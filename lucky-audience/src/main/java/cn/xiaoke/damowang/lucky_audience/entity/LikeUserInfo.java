/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * Auto-generated: 2022-01-28 15:42:59
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class LikeUserInfo extends User{

    private Integer ArcId;
}