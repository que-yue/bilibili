/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;

/**
 * Auto-generated: 2022-01-28 14:5:51
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
public class Page {

    private int pn;
    private int ps;
    private int count;
    public void setPn(int pn) {
         this.pn = pn;
     }
     public int getPn() {
         return pn;
     }

    public void setPs(int ps) {
         this.ps = ps;
     }
     public int getPs() {
         return ps;
     }

    public void setCount(int count) {
         this.count = count;
     }
     public int getCount() {
         return count;
     }

}