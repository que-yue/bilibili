/**
  * Copyright 2022 json.cn 
  */
package cn.xiaoke.damowang.lucky_audience.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * Auto-generated: 2022-01-28 15:42:59
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
@Data
@Accessors(chain = true)
public class User {

    private long mid;
    private int fans;
    private String nickname;
    private String avatar;
    private String mid_link;
    private boolean follow;

}