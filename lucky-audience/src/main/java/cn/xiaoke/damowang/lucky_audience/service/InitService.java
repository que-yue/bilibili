package cn.xiaoke.damowang.lucky_audience.service;

import cn.xiaoke.damowang.lucky_audience.config.LuckyConfig;
import cn.xiaoke.damowang.lucky_audience.entity.Archive;
import cn.xiaoke.damowang.lucky_audience.entity.AuditLikeDetailInfo;
import cn.xiaoke.damowang.lucky_audience.entity.User;
import cn.xiaoke.damowang.lucky_audience.service.domain.ArchivesDomainService;
import cn.xiaoke.damowang.lucky_audience.service.domain.FansDomainService;
import cn.xiaoke.damowang.lucky_audience.service.domain.LikeDomainService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class InitService {

    public static List<Archive> archiveList;

    public static List<AuditLikeDetailInfo> likeInfoList;
    public static List<User> likeUserList;

    public static List<User> fansList;
    public static List<User> fansRankingList;

    private final ArchivesDomainService archivesDomainService;
    private final LikeDomainService likeDomainService;
    private final FansDomainService fansDomainService;
    private final LuckyConfig config;


    public List<Archive> initArchiveList(){
        if (archiveList == null || !config.getCache()){
            archiveList = archivesDomainService.send();
        }
        return archiveList;
    }

    public List<Archive> getArchiveList(){
        return initArchiveList();
    }
    @SuppressWarnings("unchecked")
    public List<User> initLikeInfo(){
        if (likeInfoList == null ||likeUserList == null || !config.getCache()){
            final Object result = likeDomainService.send();
            likeInfoList = (List<AuditLikeDetailInfo>) ((Object[])result)[0];
            likeUserList = (List<User>) ((Object[])result)[1];
        }
        return likeUserList;
    }

    public List<User> getLikeUserList(){
        return initLikeInfo();
    }
    public List<AuditLikeDetailInfo> getLikeInfoList(){
        initLikeInfo();
        return likeInfoList;
    }
    @SuppressWarnings("unchecked")
    public List<User> initFansInfo(){
        if (fansList == null ||fansRankingList == null || !config.getCache()){
            final Object result = fansDomainService.send();
            fansList = (List<User>) ((Object[])result)[0];
            fansRankingList = (List<User>) ((Object[])result)[1];
        }
        return fansList;
    }

    public List<User> getFansList(){
        return initFansInfo();
    }

    public List<User> getfansRankingList(){
        initFansInfo();
        return fansRankingList;
    }

    public void init(){
        initArchiveList();
        initLikeInfo();
        initFansInfo();
    }
}
