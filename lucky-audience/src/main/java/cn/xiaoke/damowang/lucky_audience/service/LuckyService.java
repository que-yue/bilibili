package cn.xiaoke.damowang.lucky_audience.service;

import cn.xiaoke.damowang.lucky_audience.entity.Archive;
import cn.xiaoke.damowang.lucky_audience.entity.LikeUserInfo;
import cn.xiaoke.damowang.lucky_audience.entity.User;
import cn.xiaoke.damowang.lucky_audience.service.domain.BiliBiliLuckyReservoir;
import com.alibaba.fastjson.JSON;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Slf4j
public class LuckyService {

    private final InitService initService;


    public List<BiliBiliLuckyReservoir.WinningUserInfo> likeLucky() {
        initService.init();
        final List<Archive> archiveList = initService.getArchiveList();
        final List<User> likeUserList = initService.getLikeUserList();
        final Map<Long, List<User>> map = likeUserList.stream().collect(Collectors.groupingBy(User::getMid));
        final List<LikeUserInfo> likeAwardUserList = map.entrySet().stream().filter(entry -> entry.getValue().size() == archiveList.size())
                .peek(entry -> {
                    log.info("满足点赞特殊任务-> 用户ID:{},用户昵称:{}", entry.getKey(), entry.getValue().get(0).getNickname());
                }).flatMap(entry -> {
                    return Stream.generate(() -> ((LikeUserInfo) entry.getValue().get(0)).setArcId(-1)).limit(archiveList.size() / 2);
                }).collect(Collectors.toList());
        log.info("满足特殊点赞任务的观众{}个", likeAwardUserList.size() / (archiveList.size() / 2));

        final BiliBiliLuckyReservoir<Object> biliLuckyReservoir = new BiliBiliLuckyReservoir<>(
                new BiliBiliLuckyReservoir.PrizeInfo("B站大会员1个月", 10, true, false, 0)
        );

        log.info("开始点赞抽奖");
        likeUserList.forEach(biliLuckyReservoir::add);
        likeAwardUserList.forEach(biliLuckyReservoir::add);
        final List<BiliBiliLuckyReservoir.WinningUserInfo> winningUserInfoList = biliLuckyReservoir.getWinningUserInfoList();
        log.info("点赞抽奖中奖名单:{}", JSON.toJSONString(winningUserInfoList));
        for (BiliBiliLuckyReservoir.WinningUserInfo winningUserInfo : winningUserInfoList) {
            System.out.println(winningUserInfo.toString());
        }
        return winningUserInfoList;
    }

    public List<BiliBiliLuckyReservoir.WinningUserInfo> fansLucky() {
        initService.init();
        final List<User> fansList = initService.getFansList();
        final List<User> fansRankingList = initService.getfansRankingList();

        final BiliBiliLuckyReservoir<Object> biliLuckyReservoir = new BiliBiliLuckyReservoir<>(
                new BiliBiliLuckyReservoir.PrizeInfo("B站大会员1个月", 10, true, false, 0)
        );

        log.info("开始粉丝抽奖");
        fansList.forEach(biliLuckyReservoir::add);
        fansRankingList.forEach(biliLuckyReservoir::add);
        final List<BiliBiliLuckyReservoir.WinningUserInfo> winningUserInfoList = biliLuckyReservoir.getWinningUserInfoList();
        log.info("点赞抽奖中奖名单:{}", JSON.toJSONString(winningUserInfoList));
        for (BiliBiliLuckyReservoir.WinningUserInfo winningUserInfo : winningUserInfoList) {
            System.out.println(winningUserInfo.toString());
        }
        return winningUserInfoList;
    }

    public static void main(String[] args) {
        final int i = new Random().nextInt(29);
        System.out.println(i);

        final int j = new Random().nextInt(544) + 1;

        System.out.println(j);

    }
}
