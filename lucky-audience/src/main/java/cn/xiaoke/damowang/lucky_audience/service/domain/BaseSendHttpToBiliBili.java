package cn.xiaoke.damowang.lucky_audience.service.domain;

import cn.xiaoke.damowang.lucky_audience.config.LuckyConfig;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class BaseSendHttpToBiliBili implements SendHttpToBiliBili{

    @Getter(AccessLevel.PROTECTED)
    @Autowired
    protected LuckyConfig config;

    protected Map<String, List<String>> getHeader(){
        final LuckyConfig config = getConfig();
        final Map<String, List<String>> header = new HashMap<>();
        header.put("cookie" , Collections.singletonList("SESSDATA=" + config.getSESSDATA()));
        header.put("accept" , Collections.singletonList("application/json"));
        return header;
    }

}
