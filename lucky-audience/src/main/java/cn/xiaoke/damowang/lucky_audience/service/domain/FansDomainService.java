package cn.xiaoke.damowang.lucky_audience.service.domain;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.http.HttpRequest;
import cn.xiaoke.damowang.lucky_audience.entity.User;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Slf4j
public class FansDomainService extends BaseSendHttpToBiliBili {

    @Override
    public Object send() {
        List<User> fansList = new ArrayList<>();
        int current = 1;
        int pageSize = 50;
        int total = Integer.MAX_VALUE;
        int errorCount = 0;
        do {
            final String s = HttpRequest.get(config.getApiPre() + String.format(config.getFansList(), current, pageSize)).header(getHeader()).execute().body();
            final JSONObject body = JSON.parseObject(s);
            if (Objects.equals(0, body.getInteger("code"))) {
                final JSONObject data = body.getJSONObject("data");
                total = data.getInteger("total");
                current++;
                final JSONArray jsonArray = data.getJSONArray("list");
                jsonArray.stream().map(arrItem -> (JSONObject) arrItem).forEach(arrItem -> {
                    final User user = new User().setMid(arrItem.getLong("mid")).setNickname(arrItem.getString("uname"));
                    fansList.add(user);
                });
            } else {
                if (errorCount++ > 10) {
                    log.error("重试10次失败");
                    break;
                }
                ThreadUtil.sleep(1000);
            }
        } while ((current - 1) * pageSize < total);

        log.info("粉丝列表: {}",JSON.toJSONString(fansList));
        log.info("粉丝数量: {}",fansList.size());

        //----------------------------------------------------------------------------

        List<User> fansRankingList = new ArrayList<>();

        final String s = HttpRequest.get(config.getMemberPre() + config.getFansRanking()).header(getHeader()).execute().body();
        final JSONObject body = JSON.parseObject(s);
        if (Objects.equals(0, body.getInteger("code"))) {
            final JSONObject data = body.getJSONObject("data");
            final JSONObject rankList = data.getJSONObject("rank_list");
//            动态互动指标排行
            final JSONArray dynamicAct = rankList.getJSONArray("dynamic_act");
//            视频互动指标排行
            final JSONArray videoAct = rankList.getJSONArray("video_act");
//            累计视频播放时长排行
            final JSONArray videoPlay = rankList.getJSONArray("video_play");
            Stream.of(dynamicAct, videoAct, videoPlay).flatMap(JSONArray::stream).map(arrItem -> (JSONObject) arrItem)
                    .forEach(arrItem -> {
                        final User user = new User().setMid(arrItem.getLong("mid")).setNickname(arrItem.getString("uname"));
                        fansRankingList.add(user);
                    });
            log.info("粉丝排行列表:{}",JSON.toJSONString(fansRankingList));
            log.info("粉丝排行数量:{}",fansRankingList.size());
        }
        return new Object[]{fansList,fansRankingList};
    }
}
