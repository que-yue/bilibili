package cn.xiaoke.damowang.lucky_audience.service.domain;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.http.HttpRequest;
import cn.xiaoke.damowang.lucky_audience.entity.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Slf4j
public class LikeDomainService extends BaseSendHttpToBiliBili{

    @Override
    public Object send() {
        List<AuditLikeDetailInfo> infos = new ArrayList<>();
        List<User> userList = new ArrayList<>();
        String id = "";
        boolean flag = false;
        int errorCount = 0;
        do {
            final String s = HttpRequest.get(config.getApiPre() + String.format(config.getLikeList(), id)).header(getHeader()).execute().body();
            final JSONObject body = JSON.parseObject(s);
            if (Objects.equals(0, body.getInteger("code"))) {
                final JSONObject data = body.getJSONObject("data");
                final JSONObject total = data.getJSONObject("total");
                final Cursor cursor = total.getObject("cursor", Cursor.class);
                flag = cursor.getIs_end();
                id = String.valueOf(cursor.getId());
                final List<Items> items = total.getObject("items", new TypeReference<List<Items>>() {
                });
                items.forEach(li -> {
                    final Item item = li.getItem();
                    if (item.getType().equals("video")) {
                        final AuditLikeDetailInfo info = new AuditLikeDetailInfo(item.getItem_id(), li.getId());
                        infos.add(info);
                    }
                });
                System.out.println();
            } else {
                if (errorCount++ > 10) {
                    log.error("重试10次失败");
                    break;
                }
                ThreadUtil.sleep(1000);
            }
        } while (!flag);
        log.info("点赞信息列表: {}",JSON.toJSONString(infos));

        infos.forEach(info -> {
            int innerErrorCount = 0;
            boolean itemFlag = false;
            int pn = 1;
            do {
                final String s = HttpRequest.get(config.getApiPre() + String.format(config.getLikeDetail(), info.getLikeDetailId(), pn)).header(getHeader()).execute().body();
                final JSONObject body = JSON.parseObject(s);
                if (Objects.equals(0, body.getInteger("code"))) {
                    final JSONObject data = body.getJSONObject("data");
                    itemFlag = data.getJSONObject("page").getBoolean("is_end");
                    final Integer arcId = data.getJSONObject("card").getInteger("item_id");
                    pn++;
                    final JSONArray jsonArray = data.getJSONArray("items");
                    jsonArray.stream().map(arrItem -> (JSONObject) arrItem).forEach(arrItem -> {
                        final LikeUserInfo user = arrItem.getObject("user", LikeUserInfo.class);
                        user.setArcId(arcId);
                        userList.add(user);
                    });
                } else {
                    if (innerErrorCount++ > 10) {
                        log.error("重试10次失败");
                        break;
                    }
                    ThreadUtil.sleep(1000);
                }
            } while (!itemFlag);

        });
        log.info("点赞用户列表: {}",JSON.toJSONString(userList));
        log.info("总点赞数量: {}",userList.size());
        return new Object[]{infos,userList};
    }
}
