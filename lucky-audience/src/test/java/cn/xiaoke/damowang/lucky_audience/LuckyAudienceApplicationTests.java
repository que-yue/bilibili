package cn.xiaoke.damowang.lucky_audience;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.http.HttpRequest;
import cn.xiaoke.damowang.lucky_audience.entity.*;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootTest
class LuckyAudienceApplicationTests {

    @Value("${SESSDATA}")
    String SESSDATA;

    private final int pageSize = 50;


    final Map<String, List<String>> header = new HashMap<>();

    @BeforeEach
    void before() {
        header.put("cookie"
                , new ArrayList<>() {
                    {
                        add("SESSDATA=" + SESSDATA);
                    }
                }
        );
        header.put("accept", new ArrayList<>() {
            {
                add("application/json");
            }
        });
    }

    private final String pre = "https://member.bilibili.com";
    private final String archiveListUrl = "/x/web/archives?pn=%d&ps=%d";


    @DisplayName("获取视频列表")
    @Test
    void getList() {
        List<Archive> archiveList = new ArrayList<>();
        int current = 1;
        int pageSize = 10;
        int count = Integer.MAX_VALUE;
        int errorCount = 0;
        do {
            final String s = HttpRequest.get(pre + String.format(archiveListUrl, current, pageSize)).header(header).execute().body();
            final JSONObject body = JSON.parseObject(s);
            if (Objects.equals(0, body.getInteger("code"))) {
                final JSONObject data = body.getJSONObject("data");
                final Page page = data.getObject("page", Page.class);
                count = page.getCount();
                List<Arc_audits> arc_audits = data.getObject("arc_audits", new TypeReference<List<Arc_audits>>() {
                });
                arc_audits.forEach(arc -> {
                    archiveList.add(arc.getArchive());
                });
                System.out.println();
                count++;
            } else {
                if (errorCount++ > 10) {
                    System.out.println("重试10次失败");
                    break;
                }
                ThreadUtil.sleep(1000);
            }
        } while ((current - 1) * pageSize < count);
        System.out.println(JSON.toJSONString(archiveList));
        System.out.println(archiveList.stream().map(Archive::getAid).collect(Collectors.toList()));
    }

    private final String apiPre = "https://api.bilibili.com";
    private final String likeListUrl = "/x/msgfeed/like?id=%s";
    private final String likeDetailUrl = "/x/msgfeed/like_detail?card_id=%s&pn=%d";

    @DisplayName("获取点赞列表")
    @Test
    void getLikeList() {
        List<AuditLikeDetailInfo> infos = new ArrayList<>();
        List<User> userList = new ArrayList<>();
        String id = "";
        final String format = String.format(likeListUrl, id);
        System.out.println(format);
        boolean flag = false;
        int errorCount = 0;

        do {
            final String s = HttpRequest.get(apiPre + String.format(likeListUrl, id)).header(header).execute().body();
            final JSONObject body = JSON.parseObject(s);
            if (Objects.equals(0, body.getInteger("code"))) {
                final JSONObject data = body.getJSONObject("data");
                final JSONObject total = data.getJSONObject("total");
                final Cursor cursor = total.getObject("cursor", Cursor.class);
                flag = cursor.getIs_end();
                id = String.valueOf(cursor.getId());
                final List<Items> items = total.getObject("items", new TypeReference<List<Items>>() {
                });
                items.forEach(li -> {
                    final Item item = li.getItem();
                    if (item.getType().equals("video")) {
                        final AuditLikeDetailInfo info = new AuditLikeDetailInfo(item.getItem_id(), li.getId());
                        infos.add(info);
                    }
                });
                System.out.println();
            } else {
                if (errorCount++ > 10) {
                    System.out.println("重试10次失败");
                    break;
                }
                ThreadUtil.sleep(1000);
            }
        } while (!flag);
        System.out.println(JSON.toJSONString(infos));


        infos.forEach(info -> {
            int innerErrorCount = 0;
            boolean itemFlag = false;
            int pn = 1;
            do {
                final String s = HttpRequest.get(apiPre + String.format(likeDetailUrl, info.getLikeDetailId(), pn)).header(header).execute().body();
                final JSONObject body = JSON.parseObject(s);
                if (Objects.equals(0, body.getInteger("code"))) {
                    final JSONObject data = body.getJSONObject("data");
                    itemFlag = data.getJSONObject("page").getBoolean("is_end");
                    final Integer arcId = data.getJSONObject("card").getInteger("item_id");
                    pn++;
                    final JSONArray jsonArray = data.getJSONArray("items");
                    jsonArray.stream().map(arrItem -> (JSONObject) arrItem).forEach(arrItem -> {
                        final LikeUserInfo user = arrItem.getObject("user", LikeUserInfo.class);
                        user.setArcId(arcId);
                        userList.add(user);
                    });
                } else {
                    if (innerErrorCount++ > 10) {
                        System.out.println("重试10次失败");
                        break;
                    }
                    ThreadUtil.sleep(1000);
                }
            } while (!itemFlag);

        });
        System.out.println(JSON.toJSONString(userList));
        System.out.println(JSON.toJSONString(userList.size()));
    }

    private final String fansListUrl = "/x/relation/followers?vmid=416168709&pn=%s&ps=%s";


    @DisplayName("获取粉丝列表")
    @Test
    void getfansList() {
        List<User> fansList = new ArrayList<>();
        int current = 1;
        int total = Integer.MAX_VALUE;
        int errorCount = 0;
        do {
            final String s = HttpRequest.get(apiPre + String.format(fansListUrl, current, pageSize)).header(header).execute().body();
            final JSONObject body = JSON.parseObject(s);
            if (Objects.equals(0, body.getInteger("code"))) {
                final JSONObject data = body.getJSONObject("data");
                total = data.getInteger("total");
                current++;
                final JSONArray jsonArray = data.getJSONArray("list");
                jsonArray.stream().map(arrItem -> (JSONObject) arrItem).forEach(arrItem -> {
                    final User user = new User().setMid(arrItem.getLong("mid")).setNickname(arrItem.getString("uname"));
                    fansList.add(user);
                });
            } else {
                if (errorCount++ > 10) {
                    System.out.println("重试10次失败");
                    break;
                }
                ThreadUtil.sleep(1000);
            }
        } while ((current - 1) * pageSize < total);

        System.out.println(fansList);
        System.out.println(fansList.size());
    }

    private final String fansRanking = "/x/web/data/fan";

    @DisplayName("获取粉丝排行")
    @Test
    void getfansRanking() {
        List<User> fansRankingList = new ArrayList<>();

        final String s = HttpRequest.get(pre + String.format(fansRanking)).header(header).execute().body();
        final JSONObject body = JSON.parseObject(s);
        if (Objects.equals(0, body.getInteger("code"))) {
            final JSONObject data = body.getJSONObject("data");
            final JSONObject rankList = data.getJSONObject("rank_list");
//            动态互动指标排行
            final JSONArray dynamicAct = rankList.getJSONArray("dynamic_act");
//            视频互动指标排行
            final JSONArray videoAct = rankList.getJSONArray("video_act");
//            累计视频播放时长排行
            final JSONArray videoPlay = rankList.getJSONArray("video_play");
            Stream.of(dynamicAct, videoAct, videoPlay).flatMap(JSONArray::stream).map(arrItem -> (JSONObject) arrItem)
                    .forEach(arrItem -> {
                        final User user = new User().setMid(arrItem.getLong("mid")).setNickname(arrItem.getString("uname"));
                        fansRankingList.add(user);
                    });
            System.out.println(fansRankingList);
            System.out.println(fansRankingList.size());
        }
    }

}
